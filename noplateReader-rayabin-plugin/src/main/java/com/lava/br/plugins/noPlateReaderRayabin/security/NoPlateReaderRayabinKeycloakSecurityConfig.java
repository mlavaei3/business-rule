package com.lava.br.plugins.noPlateReaderRayabin.security;


import com.lava.br.core.security.KeycloakSecurityConfig;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class NoPlateReaderRayabinKeycloakSecurityConfig extends KeycloakSecurityConfig {
    @Override
    protected  void configure(final HttpSecurity http) throws Exception {
     super.configure(http);
        http
                .authorizeRequests()
                .antMatchers("/v1/callbackFromNoPlateReaderRayabin/**").permitAll()
                .antMatchers("/v1/**").hasRole("USER");
                //.anyRequest().permitAll();
                //.authenticated();
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v1/callbackFromNoPlateReaderRayabin/**");
    }
}