package com.lava.br.plugins.noPlateReaderRayabin.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.noPlateReaderRayabin.impl.NoPlateReaderRayabin;
import com.lava.br.plugins.noPlateReaderRayabin.restclients.NoPlateReaderRayabinRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoPlateReaderRayabinService extends PluginService {


    @Autowired
    private NoPlateReaderRayabin noPlateReaderRayabin;

    @Autowired
    private NoPlateReaderRayabinRestTemplateClient noPlateReaderRayabinRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return noPlateReaderRayabin;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return noPlateReaderRayabinRestTemplateClient;
    }



}
