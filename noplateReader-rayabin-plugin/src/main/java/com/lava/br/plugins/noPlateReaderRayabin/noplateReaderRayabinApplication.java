package com.lava.br.plugins.noPlateReaderRayabin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan(value = "com.lava.br")
@EnableMongoRepositories(basePackageClasses = com.lava.br.core.beans.IBusinessRuleRepository.class)
@EnableEurekaClient
@EnableCircuitBreaker
@EnableCaching

public class noplateReaderRayabinApplication {
    public static void main(String[] args) {
        SpringApplication.run(noplateReaderRayabinApplication.class, args);

    }
}
