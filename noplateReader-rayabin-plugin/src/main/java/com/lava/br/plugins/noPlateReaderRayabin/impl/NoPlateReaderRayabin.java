package com.lava.br.plugins.noPlateReaderRayabin.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.noPlateReaderRayabin.services.NoPlateReaderRayabinService;
import com.lava.br.plugins.noPlateReaderRayabin.services.PluginRegisteredRepositoryService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("noPlateReaderRayabin")
@Pluggable(nameOfPlugin = "noPlateReaderRayabin")
public class NoPlateReaderRayabin extends BasePlugin {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);

        //config
        /**
         * computerID
         */
        Field field=new Field();
        field.setName("computerID");
        field.setLabel("computerID");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(1);
        this.getInfo().getConfigVariables().add(field);

        /**
         * customerID
         */
        field=new Field();
        field.setName("customerID");
        field.setLabel("customerID");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(2);
        this.getInfo().getConfigVariables().add(field);


        // inVar
        /**
         * cameraID
         */
        field=new Field();
        field.setName("cameraID");
        field.setLabel("cameraID");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(3);
        this.getInfo().getInVariables().add(field);

        /**
         * plateNo
         */
        field=new Field();
        field.setName("plateNo");
        field.setLabel("plateNo");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(4);
        this.getInfo().getInVariables().add(field);

        /**
         * registeration
         */
        SimpleSelectField registerationField=SimpleSelectField.builder()
                .name("registeration")
                .label("registeration")
                .visibility(true)
                .type(TypeField.SimpleSelectList)
                .fieldClass(SimpleSelectField.class)
                .order(5)
                .build();
        Map<Object,String> selectFields=new LinkedHashMap<>();
        selectFields.put(0,"NO Registered");
        selectFields.put(1,"Registered");
        registerationField.setSelectFields(selectFields);
        this.getInfo().getInVariables().add(registerationField);

        /**
         * entranceExit
         */
        SimpleSelectField entranceExit=SimpleSelectField.builder()
                .name("entranceExit")
                .label("entranceExit")
                .visibility(true)
                .type(TypeField.SimpleSelectList)
                .fieldClass(SimpleSelectField.class)
                .order(6)
                .build();
        selectFields=new LinkedHashMap<>();
        selectFields.put("Enter","Enter");
        selectFields.put("Exit","Exit");
        selectFields.put("?","NO Detect");
        entranceExit.setSelectFields(selectFields);
        this.getInfo().getInVariables().add(entranceExit);


        /**
         * sureName
         */
        field=new Field();
        field.setName("sureName");
        field.setLabel("sureName");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(7);
        this.getInfo().getInVariables().add(field);

        /**
         * firstName
         */
        field=new Field();
        field.setName("firstName");
        field.setLabel("firstName");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(8);
        this.getInfo().getInVariables().add(field);

        /**
         * personID
         */
        field=new Field();
        field.setName("personID");
        field.setLabel("personID");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(9);
        this.getInfo().getInVariables().add(field);

        /**
         * nationalID
         */
        field=new Field();
        field.setName("nationalID");
        field.setLabel("nationalID");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(10);
        this.getInfo().getInVariables().add(field);

        /**
         * comment
         */
        field=new Field();
        field.setName("comment");
        field.setLabel("comment");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(11);
        this.getInfo().getInVariables().add(field);

        /**
         * mobileNo
         */
        field=new Field();
        field.setName("mobileNo");
        field.setLabel("mobileNo");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(12);
        this.getInfo().getInVariables().add(field);

        /**
         * passDate
         */
        field=new Field();
        field.setName("passDate");
        field.setLabel("passDate");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(13);
        this.getInfo().getInVariables().add(field);

        /**
         * passTime
         */
        field=new Field();
        field.setName("passTime");
        field.setLabel("passTime");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(14);
        this.getInfo().getInVariables().add(field);

        /**
         * dataCRC
         */
        field=new Field();
        field.setName("dataCRC");
        field.setLabel("dataCRC");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(15);
        this.getInfo().getInVariables().add(field);

    }

    @Override
    public void consume(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            NoPlateReaderRayabinService  noPlateReaderRayabinService= (NoPlateReaderRayabinService) applicationContext.getBean("noPlateReaderRayabinService");
            String customerID= noPlateReaderRayabinService.getValueForKey(
                    "noPlateReaderRayabin",
                    DbStoreType.PLUGIN,
                    "computerID",
                    plugin.getInfo().getValue(PluginInfo.CONFIG,"computerID").toString(),
                    "customerID");
            if (customerID!=null){

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void unConsume(IPlugin plugin) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void produce(IPlugin plugin) {

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "computerID").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "customerID").toString().trim();

   }
}