package com.lava.br.plugins.noPlateReaderRayabin.restclients;

import com.lava.br.core.restclients.PluginRestTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

//import org.springframework.security.oauth2.client.OAuth2RestTemplate;

@Component
public class NoPlateReaderRayabinRestTemplateClient extends PluginRestTemplateClient {
    private static final Logger logger = LoggerFactory.getLogger(NoPlateReaderRayabinRestTemplateClient.class);
}
