package com.lava.br.plugins.noPlateReaderRayabin.controllers;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.NiligoTriggerType;
import com.lava.br.core.plugins.utils.niligo.Endpoints;
import com.lava.br.core.plugins.utils.niligo.M2MPayload;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.PluginRegistered;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.noPlateReaderRayabin.impl.NoPlate;
import com.lava.br.plugins.noPlateReaderRayabin.impl.NoPlateReaderRayabin;
import com.lava.br.plugins.noPlateReaderRayabin.services.NoPlateReaderRayabinService;
import com.lava.br.plugins.noPlateReaderRayabin.services.PluginRegisteredRepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class NoPlateReaderRayabinController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(NoPlateReaderRayabinController.class);

    @Autowired
    private NoPlateReaderRayabinService noPlateReaderRayabinService;

    @Override
    public PluginService getPluginService(){
        return noPlateReaderRayabinService;
    }

    @RequestMapping(value="testDb/{brId}",method = RequestMethod.GET)
    public String testDb(@PathVariable("brId") String brId) {
        //noPlateReaderRayabinService.addKeyWithValue("mehdi",DbStoreType.ACCOUNTING,"account","acc1","token","ttt22");
        return "ok";
    }


    @Autowired
    PluginRegisteredRepositoryService pluginRegisteredRepositoryService;

    @RequestMapping(value="/callbackFromNoPlateReaderRayabin",method = RequestMethod.POST)
    public ResponseEntity callbackFromNoPlateReaderRayabin(@RequestBody NoPlate object) {
        try {
            if(object.getComputerID()==null || object.getCustomerID()==null){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Customer_Id Or Computer_Id Is Null");
            }
            String customerID= noPlateReaderRayabinService.getValueForKey(
                    "noPlateReaderRayabin",
                    DbStoreType.PLUGIN,
                    "computerID",
                    object.getComputerID(),
                    "customerID");
            if (customerID==null || customerID.isEmpty() || !customerID.equalsIgnoreCase(object.getCustomerID())){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Customer_Id Or Computer_Id Is Not Defined");
            }

            NoPlateReaderRayabin noPlateReaderRayabin=(NoPlateReaderRayabin)  getPluginService().getNewInstancePlugin();
            noPlateReaderRayabin.getInfo().setValue(PluginInfo.CONFIG,"computerID",object.getComputerID());
            noPlateReaderRayabin.getInfo().setValue(PluginInfo.CONFIG,"customerID",object.getCustomerID());

            String unique=getPluginService().getPlugin().getUniqueIdentity(noPlateReaderRayabin);
            List<PluginRegistered> lstPluginsRegistered=pluginRegisteredRepositoryService.getBasicRepository()
                    .findByConsumerTypeAndPluginUniqueIdentity(getPluginService().getPlugin().getInfo().getName(),unique);

            if(lstPluginsRegistered.size()>0){
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"cameraID",object.getCameraID());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"plateNo",object.getPlateNo());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"registeration",object.getRegisteration());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"entranceExit",object.getEntranceExit());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"sureName",object.getSureName());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"firstName",object.getFirstName());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"personID",object.getPersonID());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"nationalID",object.getNationalID());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"comment",object.getComment());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"mobileNo",object.getMobileNo());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"passDate",object.getPassDate());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"passTime",object.getPassTime());
                noPlateReaderRayabin.getInfo().setValue(PluginInfo.IN,"dataCRC",object.getDataCRC());


                getPluginService().sendMessageToServer(noPlateReaderRayabin);
            }


        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().build();
    }


}
