package com.lava.br.plugins.noPlateReaderRayabin.impl;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class NoPlate {

    String reportType;
    String customerID;
    String computerID;
    String cameraID;
    String plateNo;
    String registeration;
    String entranceExit;
    String sureName;
    String firstName;
    String personID;
    String nationalID;
    String comment;
    String mobileNo;
    String passDate;
    String passTime;
    String dataCRC;


}