package com.lava.br.plugins.gmailToken.routes;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.routes.BaseRouteBuilder;
import com.lava.br.plugins.gmailToken.impl.GmailToken;
import com.lava.br.plugins.gmailToken.processes.GmailTokenProcessor;
import com.lava.br.plugins.gmailToken.services.GmailTokenService;
import org.springframework.context.ApplicationContext;

public class GmailTokenRoute extends BaseRouteBuilder {

	public GmailTokenRoute(GmailToken gmailToken, String routeName) {
		super(gmailToken,routeName);
	}


	@Override
	public void configure() throws Exception {
		ApplicationContext applicationContext = null;
		applicationContext = ApplicationContextProvider.getApplicationContext();
		GmailTokenService gmailTokenService= (GmailTokenService) applicationContext.getBean("gmailTokenService");
		String token= gmailTokenService.getValueForKey(getPlugin().getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
				DbStoreType.ACCOUNTING,
				"account",
				getPlugin().getInfo().getValue(PluginInfo.CONFIG, "username").toString(),
				"token");

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(token);
		String accessToken = node.path("accessToken").asText();
		String refreshToken = node.path("refreshToken").asText();

		//markAsRead=true&
		from("google-mail-stream://test?markAsRead=true&delay=5000&maxResults=5&labels=Inbox" +
				"&clientId=278000558682-i02vo5aqnfs3jvcciemr1h5c2qbhrh51.apps.googleusercontent.com" +
				"&clientSecret=u_u8KkWCiSIUhy78t-IgjviQ" +
				"&accessToken="+accessToken +
				"&refreshToken="+refreshToken)
				.routeId(getRouteName()) //getPlugin().getInfo().getValue("CONFIG", "username").toString())
				.autoStartup(false)
				.routePolicy(getPolicy())
				.process(new GmailTokenProcessor((GmailToken) getPlugin()));

	}

}
