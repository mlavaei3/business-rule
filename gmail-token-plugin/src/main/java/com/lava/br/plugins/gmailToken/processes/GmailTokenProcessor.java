package com.lava.br.plugins.gmailToken.processes;

import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.processes.BaseProcessor;
import com.lava.br.core.repositories.bean.FieldAssigner;
import com.lava.br.plugins.gmailToken.impl.GmailToken;
import org.apache.camel.Exchange;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class GmailTokenProcessor extends BaseProcessor {
    public GmailTokenProcessor(GmailToken gmailToken) {
        super(gmailToken);
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        List<FieldAssigner> fieldAssigners=new ArrayList<FieldAssigner>();
        fieldAssigners.add(new FieldAssigner("username",this.getPlugin().getInfo().getValue(PluginInfo.CONFIG, "username").toString()));

        getPlugin().getInfo().setValue(PluginInfo.IN, "from", exchange.getIn().getHeader("CamelGoogleMailStreamFrom"));
        getPlugin().getInfo().setValue(PluginInfo.IN, "subject", exchange.getIn().getHeader("CamelGoogleMailStreamSubject"));
        if ( exchange.getIn().getBody() instanceof MimeMultipart){
            MimeMultipart f= (MimeMultipart) exchange.getIn().getBody();
            getPlugin().getInfo().setValue(PluginInfo.IN, "body", getTextFromMimeMultipart(f));
        }else if(exchange.getIn().getBody() instanceof Message){
            Message f= (Message) exchange.getIn().getBody();
            getPlugin().getInfo().setValue(PluginInfo.IN, "body", getTextFromMessage(f));
        }else
        {
            getPlugin().getInfo().setValue(PluginInfo.IN, "body", exchange.getIn().getBody());
        }

        sendMessageToServer();
    }

    private String getTextFromMessage(Message message) throws MessagingException, IOException {
        String result = "";
        if (message.isMimeType("text/plain")) {
            result = message.getContent().toString();
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }

    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart)  throws MessagingException, IOException {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                //result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart){
                result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }
}
