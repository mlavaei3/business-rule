package com.lava.br.plugins.gmailToken.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.gmailToken.impl.GmailToken;
import com.lava.br.plugins.gmailToken.restclients.GmailTokenRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(2)
public class GmailTokenService extends PluginService {
    @Autowired
    private GmailToken gmailToken;

    @Autowired
    private GmailTokenRestTemplateClient gmailTokenRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return gmailToken;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return gmailTokenRestTemplateClient;
    }
}
