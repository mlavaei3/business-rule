package com.lava.br.plugins.gmailToken.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.gmailToken.impl.GmailToken;
import com.lava.br.plugins.gmailToken.services.GmailTokenService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@RestController
public class GmailTokenController extends PluginController {

    @Autowired
    private GmailTokenService gmailTokenService;

    @Value("${lava.google.oauth2.client_id}")
    String client_id;

    @Value("${lava.google.oauth2.client_secret}")
    String client_secret;

    @Value("${lava.google.oauth2.redirect_uri}")
    String redirect_uri;

    @RequestMapping(value = "/oauth2Callback", method = RequestMethod.GET)
    public ResponseEntity oauth2Callback(@RequestParam(value = "scope", required = true) String scope,
                                         @RequestParam(value = "code", required = true) String code,
                                         @RequestParam(value = "state", required = true) String state) {
        String sessionId=state.split(" ")[0];
        String objectId=state.split(" ")[1];
        String userid=state.split(" ")[2];

        String url = "https://oauth2.googleapis.com/token";
         try {
            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> body
                    = new LinkedMultiValueMap<>();
            body.add("grant_type","authorization_code");
            body.add("code",code);
            body.add("client_id",client_id);
            body.add("client_secret",client_secret);

            String port= String.valueOf(getPluginService().getServicePort());

            body.add("redirect_uri",redirect_uri+"/v1/oauth2Callback");

            HttpEntity<MultiValueMap<String, Object>> requestEntity
                    = new HttpEntity<>(body, headers);

            ResponseEntity<String> rsp = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

            //(2)extract the Access Token From the recieved JSON response
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(rsp.getBody());
            String accessToken = node.path("access_token").asText();
            String refreshToken = node.path("refresh_token").asText();

            url="https://www.googleapis.com/gmail/v1/users/me/profile?access_token="+accessToken;
            rsp = restTemplate.getForEntity(url, String.class);
            //(2)extract the Access Token From the recieved JSON response
            mapper = new ObjectMapper();
            node = mapper.readTree(rsp.getBody());
            String emailAddress = node.path("emailAddress").asText();

            getPluginService().addKeyWithValue(userid,
                    DbStoreType.ACCOUNTING,
                    "account",
                    emailAddress,"token",
                    String.format("{\"accessToken\":\"%s\",\"refreshToken\":\"%s\"}",accessToken,refreshToken));

            gmailTokenService.sendToSocket(sessionId,objectId,"{\""+emailAddress+ "\":\""+emailAddress+"\"}");


        } catch (HttpClientErrorException e) {
            System.out.println("\n1:"+e.getStatusCode());
            System.out.println("\n2:"+e.getResponseBodyAsString());
            //model.addAttribute("message", e.getResponseBodyAsString());
            //return "error";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("<script language='javascript'>parentwin = window.self;\n" +
                "parentwin.opener = window.self;\n" +
                "parentwin.close(); </script>");
    }

    @RequestMapping(value = "/oauth2Callback/test", method = RequestMethod.GET)
    public ResponseEntity oauth2Callback() {
        try {
            PluginInfoClient pluginInfoClient= new PluginInfoClient();
            pluginInfoClient.setName("gmailToken");
            IPlugin iPlugin=getPluginService().newInstance(pluginInfoClient);
            getPluginService().getPlugin().consume(iPlugin);
        } catch (Exception e) {
            e.printStackTrace();

        }
        return ResponseEntity.ok().build();
    }

    @Override
    protected PluginService getPluginService() {
        return gmailTokenService;
    }
}