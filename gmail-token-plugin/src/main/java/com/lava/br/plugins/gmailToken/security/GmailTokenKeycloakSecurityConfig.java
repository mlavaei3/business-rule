package com.lava.br.plugins.gmailToken.security;


import com.lava.br.core.security.KeycloakSecurityConfig;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class GmailTokenKeycloakSecurityConfig extends KeycloakSecurityConfig {
    @Override
    protected  void configure(final HttpSecurity http) throws Exception {
     super.configure(http);
        http
                .authorizeRequests()
                .antMatchers("/v1/oauth2Callback/**").permitAll()
                .antMatchers("/v1/**")//.permitAll();
                .hasRole("USER")
                .anyRequest()
                .authenticated();
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v1/oauth2Callback/**");
    }
}