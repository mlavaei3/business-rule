package com.lava.br.plugins.gmailToken.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.Field;
import com.lava.br.core.repositories.bean.FieldValidator;
import com.lava.br.core.repositories.bean.PartyField;
import com.lava.br.plugins.gmailToken.routes.GmailTokenRoute;
import com.lava.br.plugins.gmailToken.services.GmailTokenService;
import com.netflix.discovery.EurekaClient;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Producer;
import org.apache.camel.component.google.mail.GoogleMailComponent;
import org.apache.camel.component.google.mail.GoogleMailConfiguration;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

//import com.lava.br.plugins.gmail.routes.GmailTokenRoute;

@Service("gmailToken")
@Pluggable(nameOfPlugin = "gmailToken")
@Order(1000)
public class GmailToken extends BasePluginWithCamel {
    @Autowired
    Environment environment;

    @Qualifier("eurekaClient")
    @Autowired
    EurekaClient eurekaClient;

    @Value("${lava.google.oauth2.redirect_uri}")
    String redirect_uri;

    @PostConstruct
    public void init() {

        this.getInfo().setIsConsumer(true);
        this.getInfo().setIsProducer(true);

        this.removeAction("default");
        this.addAction("send","send");

        this.removeTrigger("default");
        this.addTrigger("read","read");

        PartyField partyField=PartyField
                .builder()
                .name("username")
                .label("username")
                .type(TypeField.PartySelectList)
                .visibility(true)
                .refActions(Arrays.asList("send"))
                .refTriggers(Arrays.asList("read"))
                .order(1)
                .build();
        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        String port= String.valueOf(eurekaClient.getApplicationInfoManager().getInfo().getPort());
        partyField.setPartyUrl("https://accounts.google.com/o/oauth2/auth?scope=https://mail.google.com&approval_prompt=force&response_type=code&access_type=offline&redirect_uri="+
                                        UrlUtils.urlEncode(redirect_uri+"/v1/oauth2Callback")+"&client_id=278000558682-i02vo5aqnfs3jvcciemr1h5c2qbhrh51.apps.googleusercontent.com");


        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);

        partyField.setFieldValidators(fieldValidators);
        this.getInfo().getConfigVariables().add(partyField);

        // inVar
        Field field=new Field();
        field.setName("from");
        field.setLabel("from");
        field.setRefTriggers(Arrays.asList("read"));
        field.setFieldClass(String.class);
        field.setOrder(1);
        this.getInfo().getInVariables().add(field);//Field.builder().name("from").fieldClass( String.class).label("from").order(1).build());
        field=new Field();
        field.setName("subject");
        field.setLabel("subject");
        field.setRefTriggers(Arrays.asList("read"));
        field.setFieldClass(String.class);
        field.setOrder(2);
        this.getInfo().getInVariables().add(field);//Field.builder().name("subject").fieldClass( String.class).label("subject").order(2).build());
        field=new Field();
        field.setName("body");
        field.setLabel("body");
        field.setRefTriggers(Arrays.asList("read"));
        field.setFieldClass(String.class);
        field.setOrder(3);
        this.getInfo().getInVariables().add(field);//Field.builder().name("body").fieldClass( String.class).label("body").order(3).build());

        // outVar
        field=new Field();
        field.setName("to");
        field.setLabel("to");
        field.setRefActions(Arrays.asList("send"));
        field.setFieldClass(String.class);
        field.setOrder(3);
        fieldValidators=new ArrayList<>();
        fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);
        field.setFieldValidators(fieldValidators);
        this.getInfo().getOutVariables().add(field);//Field.builder().name("to").fieldClass( String.class).label("to").order(3).build());
        field=new Field();
        field.setName("subject");
        field.setLabel("subject");
        field.setRefActions(Arrays.asList("send"));
        field.setFieldClass(String.class);
        field.setOrder(4);
        this.getInfo().getOutVariables().add(field);//Field.builder().name("subject").fieldClass( String.class).label("subject").order(4).build());
        field=new Field();
        field.setName("body");
        field.setLabel("body");
        field.setRefActions(Arrays.asList("send"));
        field.setFieldClass(String.class);
        field.setOrder(5);
        this.getInfo().getOutVariables().add(field);

    }
    public final String CURRENT_USERID = "me";

    @Override
    public void consume(IPlugin plugin) {
        try {
            this.unConsume(plugin);
            getCamelContext().addRoutes(new GmailTokenRoute((GmailToken) plugin,this.getUniqueIdentity(plugin)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void produce(IPlugin plugin) {
        com.google.api.services.gmail.model.Message testEmail = null;
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            GmailTokenService gmailTokenService= (GmailTokenService) applicationContext.getBean("gmailTokenService");
            String token= gmailTokenService.getValueForKey(plugin.getInfo().getValue(PluginInfo.OUT, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "username").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);
            String accessToken = node.path("accessToken").asText();
            String refreshToken = node.path("refreshToken").asText();

            GoogleMailConfiguration gm=new GoogleMailConfiguration();
            gm.setClientId("278000558682-i02vo5aqnfs3jvcciemr1h5c2qbhrh51.apps.googleusercontent.com");
            gm.setClientSecret("u_u8KkWCiSIUhy78t-IgjviQ");
            gm.setApplicationName("camel-google-mail/1.0");
            gm.setAccessToken(accessToken);
            gm.setRefreshToken(refreshToken);

            GoogleMailComponent gmc=new GoogleMailComponent();
            gmc.setConfiguration(gm);
            gmc.setCamelContext(getCamelContext());

            Endpoint en=gmc.createEndpoint("google-mail://messages/send");//google-mail://users/getProfile

            testEmail = createTestEmail(plugin);

            Map<String, Object> headers = new HashMap<>();
            // parameter type is String
            headers.put("CamelGoogleMail.userId",CURRENT_USERID );//CURRENT_USERID
            // parameter type is com.google.api.services.gmail.model.Message
            headers.put("CamelGoogleMail.content", testEmail);

            Exchange exchange = en.createExchange();
            Message in = exchange.getIn();
            in.setHeaders(headers);
            in.setBody(testEmail);

            Producer producer = en.createProducer();
            producer.start();
            producer.process(exchange);
            producer.stop();
            //com.google.api.services.gmail.model.Message result = requestBodyAndHeaders("direct://SEND", null, headers);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*Endpoint endpoint = getCamelContext()
                .getEndpoint("google-mail://messages/send");
        Exchange exchange = endpoint.createExchange();
        Message in = exchange.getIn();
        Map<String, Object> headers = new HashMap<String, Object>();*/
        // in.addAttachment("attachment.zip", new
        // DataHandler(applicationContext.getResource("file:test.zip").getURL()));

        //runTemplateProducer(endpoint,exchange);

    }

    private com.google.api.services.gmail.model.Message createTestEmail(IPlugin plugin) throws IOException, MessagingException {
        com.google.api.services.gmail.model.Profile profile=null;// = requestBody("google-mail://users/getProfile?inBody=userId", CURRENT_USERID);
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        MimeMessage mm = new MimeMessage(session);
        mm.setRecipients(javax.mail.Message.RecipientType.TO,plugin.getInfo().getValue(PluginInfo.OUT, "to").toString());
        //mm.addRecipients(javax.mail.Message.RecipientType.TO, profile.getEmailAddress());
        mm.setSubject(plugin.getInfo().getValue(PluginInfo.OUT, "subject").toString());
        mm.setContent(plugin.getInfo().getValue(PluginInfo.OUT, "body").toString(), "text/plain");
        com.google.api.services.gmail.model.Message createMessageWithEmail = createMessageWithEmail(mm);
        return createMessageWithEmail;
    }

    private com.google.api.services.gmail.model.Message createMessageWithEmail(MimeMessage email) throws MessagingException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        email.writeTo(baos);
        String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
        com.google.api.services.gmail.model.Message message = new com.google.api.services.gmail.model.Message();
        message.setRaw(encodedEmail);
        return message;
    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "username")!=null?
                plugin.getInfo().getValue(PluginInfo.CONFIG, "username").toString().trim()
                :null;
    }

    public Map<String, String> getAccounts(Map<String, Object> objectMap){
        GmailTokenService gmailTokenService= ApplicationContextProvider.getApplicationContext().getBean("gmailTokenService",GmailTokenService.class);
        Map<String,String> accounts=gmailTokenService.getAllValueForKey(objectMap.get("userId").toString(),DbStoreType.ACCOUNTING,"account","token");
        accounts.forEach((k,v)->{accounts.put(k,k);});
        return accounts;
        /*Map<String, Object> map=new HashMap<>();
        map.put("matin@niligo.com","matin@niligo.com");
        return map;*/
    }
}