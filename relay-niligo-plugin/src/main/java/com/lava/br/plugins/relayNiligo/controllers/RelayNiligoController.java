package com.lava.br.plugins.relayNiligo.controllers;


import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.NiligoTriggerType;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.utils.niligo.Endpoints;
import com.lava.br.core.plugins.utils.niligo.M2MPayload;
import com.lava.br.core.repositories.bean.PluginRegistered;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.relayNiligo.impl.RelayNiligo;
import com.lava.br.plugins.relayNiligo.services.PluginRegisteredRepositoryService;
import com.lava.br.plugins.relayNiligo.services.RelayNiligoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class RelayNiligoController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(RelayNiligoController.class);

    @Value("${lava.plugins.relayNiligo.auth2.user_pass}")
    private String userPassForToken;

    @Value("${lava.plugins.relayNiligo.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.relayNiligo.auth2.redirect_uri}")
    private String redirectUri;

    @Autowired
    private RelayNiligoService relayNiligoService;

    @Override
    public PluginService getPluginService(){
        return relayNiligoService;
    }

    private Endpoints endpoints;

    private Endpoints getEndpoints(){
        if (endpoints==null) {
            endpoints = new Endpoints(userPassForToken, tokenUri, redirectUri, getPluginService());
        }
        return endpoints;
    }


    @RequestMapping(value="testDb/{brId}",method = RequestMethod.GET)
    public String testDb(@PathVariable("brId") String brId) {
        relayNiligoService.addKeyWithValue("mehdi",DbStoreType.ACCOUNTING,"account","acc1","token","ttt22");
        relayNiligoService.addKeyWithValue("mehdi",DbStoreType.ACCOUNTING,"account","acc2","token","ttt2222");

        Map<String,String> maps= relayNiligoService.getAllValueForKey("mehdi",DbStoreType.ACCOUNTING,"account","token");
        String gg= relayNiligoService.getValueForKey("mehdi",DbStoreType.ACCOUNTING,"account","acc1","token");
        return "ok";
    }

    @RequestMapping(value="/testSocket",method = RequestMethod.GET)
    public void testSocket(@RequestParam(value ="sessionId",required = true) String sessionId,
                           @RequestParam(value ="objectId",required = true) String objectId ,
                           @RequestParam(value ="email",required = true) String email) {

        relayNiligoService.sendToSocket(sessionId,objectId,"{\""+email+ "\":\""+email+"\"}");

    }

    @RequestMapping(value="/callbackFromParty",method = RequestMethod.GET)
    public ResponseEntity callbackFromParty(@RequestParam(value ="state",required = true) String state,
                                            @RequestParam(value ="code",required = true) String code)  {
        return getEndpoints().callbackFromParty(state,code);
        /*String sessionId=state.split(" ")[0];
        String objectId=state.split(" ")[1];
        String userid=state.split(" ")[2];

        //String userPass = "ghasem:sadeghi";
        String basicEncoded = Base64Utils.encodeToString(userPassForToken.getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + basicEncoded);
        HttpEntity<String> request = new HttpEntity<>(headers);

        String url = tokenUri+"/oauth/token?";
        url += "code=" + code;
        url += "&grant_type=authorization_code";
        url += "&redirect_uri="+redirectUri;

        try {
            RestTemplate restTemplate=new RestTemplate();
            ResponseEntity<String> rsp = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

            //(2)extract the Access Token From the recieved JSON response
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(rsp.getBody());
            String accessToken = node.path("access_token").asText();

            getPluginService().addKeyWithValue(userid,
                    "account",
                    node.path("username").asText(),"token",
                    String.format("{\"accessToken\":\"%s\"}",accessToken));

            relayNiligoService.sendToSocket(sessionId,objectId,"{\""+node.path("username").asText()+ "\":\""+node.path("username").asText()+"\"}");


        } catch (HttpClientErrorException e) {
            System.out.println("\n1:"+e.getStatusCode());
            System.out.println("\n2:"+e.getResponseBodyAsString());
            //model.addAttribute("message", e.getResponseBodyAsString());
            //return "error";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("<script language='javascript'>parentwin = window.self;\n" +
                "parentwin.opener = window.self;\n" +
                "parentwin.close(); </script>");*/

    }

    @RequestMapping(value="/getRelayNiligos",method = RequestMethod.GET)
    public ResponseEntity getRelayNiligos(Principal principal, @RequestParam(value ="account",required = true) String account) {
        Map<String, Object> objectMap=new HashMap<>();
        objectMap.put("userId",principal.getName());
        objectMap.put("query",account);
        Map<String, String> relayNiligoMap=((RelayNiligo)relayNiligoService.getPlugin()).getRelayNiligos(objectMap);
        return ResponseEntity.ok(relayNiligoMap);
    }

    @Autowired
    PluginRegisteredRepositoryService pluginRegisteredRepositoryService;

    @RequestMapping(value="/callbackFromRelayNiligo",method = RequestMethod.POST)
    public ResponseEntity callbackFromRelayNiligo(@RequestBody M2MPayload object) {
        try {
            System.out.print("Get From Relay: ");
            System.out.println(object);
            List<String> lstUser=object.getM2mUsers();
            if(lstUser==null || lstUser.size()==0){
                return null;
            }
            RelayNiligo relayNiligo=(RelayNiligo)  getPluginService().getNewInstancePlugin();
            relayNiligo.getInfo().setValue(PluginInfo.CONFIG,"account",object.getM2mUsers().get(0));//"ghasematsadeghi@gmail.com");
            relayNiligo.getInfo().setValue(PluginInfo.CONFIG,"relayNiligoList",object.getUuid());//
            relayNiligo.getInfo().setValue(PluginInfo.CONFIG,"side",object.getTriggerType());
            relayNiligo.getInfo().setValue(PluginInfo.CONFIG,"port",object.getPortFromRelay());

            String unique=getPluginService().getPlugin().getUniqueIdentity(relayNiligo);
            List<PluginRegistered> lstPluginsRegistered=pluginRegisteredRepositoryService.getBasicRepository()
                    .findByConsumerTypeAndPluginUniqueIdentity(getPluginService().getPlugin().getInfo().getName(),unique);

            if(lstPluginsRegistered.size()>0){
                System.out.print("Send From Relay To Server: ");
                System.out.println(relayNiligo);
                getPluginService().sendMessageToServer(relayNiligo);
            }


        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().build();
    }
}
