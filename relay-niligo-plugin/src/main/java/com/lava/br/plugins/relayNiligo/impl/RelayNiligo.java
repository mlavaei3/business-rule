package com.lava.br.plugins.relayNiligo.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.FieldValidator;
import com.lava.br.core.repositories.bean.LazySelectField;
import com.lava.br.core.repositories.bean.PartyField;
import com.lava.br.core.repositories.bean.SimpleSelectField;
import com.lava.br.plugins.relayNiligo.services.RelayNiligoService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("relayNiligo")
@Pluggable(nameOfPlugin = "relayNiligo")
public class RelayNiligo extends BasePluginWithCamel {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);
        this.getInfo().setIsProducer(true);

        // configs
        /**
         * account
         */
        PartyField partyField = PartyField
                .builder()
                .name("account")
                .label("account")
                .type(TypeField.PartySelectList)
                .visibility(true)
                .order(1)
                .build();
        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        partyField.setPartyUrl(tokenUri + "/oauth/authorize?response_type=code&scopes=read&client_id="+userPass.split(":")[0]+"&redirect_uri=" + UrlUtils.urlEncode(redirectUri));
        this.getInfo().getConfigVariables().add(partyField);
        /**
         * relayNiligoList
         */
        LazySelectField relayNiligoListField = LazySelectField.builder()
                .fieldClass(LazySelectField.class)
                .name("relayNiligoList")
                .label("relayNiligoList")
                .type(TypeField.LazySelectList)
                .visibility(true)
                .order(2)
                .build();

        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReset=FieldValidator.builder()
                .type(ValidatorType.DeptReset)
                .expression("account")
                .build();
        fieldValidators.add(fvReset);
        relayNiligoListField.setFieldValidators(fieldValidators);
        relayNiligoListField.setSelectFields(this::getRelayNiligos);
        relayNiligoListField.setRequestUrl("?query=${account}");
        this.getInfo().getConfigVariables().add(relayNiligoListField);

        /**
         * side config
         */
        SimpleSelectField side = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("side")
                .label("side")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(3)
                .build();
        Map<Object, String> map = new HashMap<>();
        map.put("RELAY_ON","RELAY_ON");
        map.put("RELAY_OFF", "RELAY_OFF");
        side.setSelectFields(map);

        side.removeDefaultTriggersAndActions();
        List<String> listTrigger=new ArrayList<>();
        listTrigger.add("default");
        side.setRefTriggers(listTrigger);

        this.getInfo().getConfigVariables().add(side);


        /**
         * port number config
         */
        SimpleSelectField portNumber = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("port")
                .label("port")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(4)
                .build();
        map = new HashMap<>();
        map.put("0","Port 0");
        map.put("1", "Port 1");
        portNumber.setSelectFields(map);

        portNumber.removeDefaultTriggersAndActions();
        listTrigger=new ArrayList<>();
        listTrigger.add("default");
        portNumber.setRefTriggers(listTrigger);

        this.getInfo().getConfigVariables().add(portNumber);

        /**
         * side Out
         */

        side = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("side")
                .label("side")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(5)
                .build();

        side.removeDefaultTriggersAndActions();
        List<String> listAction=new ArrayList<>();
        listAction.add("default");
        side.setRefActions(listAction);

        map = new HashMap<>();
        map.put("RELAY_ON","RELAY_ON");
        map.put("RELAY_OFF", "RELAY_OFF");
        side.setSelectFields(map);
        this.getInfo().getOutVariables().add(side);

        /**
         * port number out
         */
        portNumber = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("port")
                .label("port")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(6)
                .build();
        map = new HashMap<>();
        map.put("0","Port 0");
        map.put("1", "Port 1");
        portNumber.setSelectFields(map);

        portNumber.removeDefaultTriggersAndActions();
        listAction=new ArrayList<>();
        listAction.add("default");
        portNumber.setRefActions(listAction);

        this.getInfo().getOutVariables().add(portNumber);

    }

    @Value("${lava.plugins.relayNiligo.api_uri}")
    private String apiUri;

    @Value("${lava.plugins.relayNiligo.m2m_uri}")
    private String m2mUuri;

    @Value("${lava.plugins.relayNiligo.auth2.redirect_uri}")
    private String redirectUri;

    @Value("${lava.plugins.relayNiligo.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.relayNiligo.auth2.user_pass}")
    private String userPass;

    public Map<String, String> getAccounts(Map<String, Object> objectMap) {

        RelayNiligoService relayNiligoService = ApplicationContextProvider.getApplicationContext().getBean("relayNiligoService", RelayNiligoService.class);
        Map<String, String> accounts = relayNiligoService.getAllValueForKey(objectMap.get("userId").toString(),DbStoreType.ACCOUNTING, "account", "token");
        accounts.forEach((k, v) -> {
            accounts.put(k, k);
        });
        return accounts;
    }

    public Map<String, String> getRelayNiligos(Map<String, Object> objectMap) {
        Map<String, String> map = new HashMap<>();
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            RelayNiligoService relayNiligoService = (RelayNiligoService) applicationContext.getBean("relayNiligoService");
            String token = relayNiligoService.getValueForKey(objectMap.get("userId").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    objectMap.get("query").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);
            HttpEntity<String> request1 = new HttpEntity<>(headers1);

            String resourceUrl = apiUri + "/api/thing/list?type=relay";
            ResponseEntity<ThingDto[]> rsp1 = restTemplate.exchange(resourceUrl, HttpMethod.GET, request1, ThingDto[].class);

            for (ThingDto thingDto : rsp1.getBody()) {
                map.put(thingDto.getUuid(), thingDto.getName());
            }
            return map;
        } catch (IOException e) {
            return map;
        }
   }

    @Override
    public void consume(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            RelayNiligoService relayNiligoService = (RelayNiligoService) applicationContext.getBean("relayNiligoService");
            String token = relayNiligoService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);


            String actionType = plugin.getInfo().getValue(PluginInfo.CONFIG, "producerAction").toString();
            String resourceUrl = m2mUuri + "/m2m/integration/user/add";
            Map<String, String> map = new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid", plugin.getInfo().getValue(PluginInfo.CONFIG, "relayNiligoList").toString());
            map.put("trigger", plugin.getInfo().getValue(PluginInfo.CONFIG, "side").toString());

            HttpEntity<Map<String, String>> request1 = new HttpEntity<>(map, headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            if (rsp1.getStatusCodeValue() == 200 || rsp1.getStatusCodeValue() == 201) {
                //registerRelayNiligo.add(map.get("uuid")+ map.get("trigger"));
            }
            System.out.println(rsp1.getBody());
        } catch(
                IOException e)

        {
            e.printStackTrace();
        }
    }

    @Override
    public void unConsume(IPlugin plugin) {
        try {
            final Map<String,String> map=new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid", plugin.getInfo().getValue(PluginInfo.CONFIG, "relayNiligoList").toString());
            map.put("trigger", plugin.getInfo().getValue(PluginInfo.CONFIG, "side").toString());

            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            RelayNiligoService relayNiligoService = (RelayNiligoService) applicationContext.getBean("relayNiligoService");
            String token = relayNiligoService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);

            String resourceUrl = m2mUuri + "/m2m/integration/user/delete";

            HttpEntity<Map<String, String>> request1 = new HttpEntity<>(map, headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void produce(IPlugin plugin) {

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        //plugin.getInfo().getValue(PluginInfo.CONFIG, "account")!=null?
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "relayNiligoList").toString().trim()+
                        "-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "side").toString().trim()+
                            "-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "port").toString().trim();

    }
}