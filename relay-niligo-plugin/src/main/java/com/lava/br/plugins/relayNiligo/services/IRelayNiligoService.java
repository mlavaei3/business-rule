package com.lava.br.plugins.relayNiligo.services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Aria on 20/04/2018.
 */
@Path(value="/switchNiligo")
public interface IRelayNiligoService {

    @GET
    @Path("/{id}")
    @Produces({MediaType.TEXT_HTML,MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({MediaType.TEXT_HTML,MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    String  changeState(@PathParam("id") String id, @QueryParam("num_per_page") String numPerPage);
}
