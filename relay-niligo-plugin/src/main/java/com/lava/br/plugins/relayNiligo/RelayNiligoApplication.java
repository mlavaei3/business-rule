package com.lava.br.plugins.relayNiligo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

//import org.springframework.cloud.stream.annotation.EnableBinding;
//import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@ComponentScan(value = "com.lava.br")
@EnableMongoRepositories(basePackageClasses = com.lava.br.core.beans.IBusinessRuleRepository.class)
@EnableEurekaClient
@EnableCircuitBreaker
@EnableCaching
//@ImportResource({ "classpath:camel-context.xml" })
//@EnableResourceServer
//@Slf4j
public class RelayNiligoApplication {
    public static void main(String[] args) {
        SpringApplication.run(RelayNiligoApplication.class, args);

    }
	/*@Bean
	public AlwaysSampler defaultSampler() {
		return new AlwaysSampler();
	}
*/
}
