--DROP TABLE IF EXISTS users;
--DROP TABLE IF EXISTS user_roles;
--DROP TABLE IF EXISTS user_orgs;

/*IF OBJECT_ID(N'dbo.users', N'U') IS NOT NULL
BEGIN
	DROP TABLE users
END

IF OBJECT_ID(N'dbo.user_roles', N'U') IS NOT NULL
BEGIN
	DROP TABLE user_roles
END

IF OBJECT_ID(N'dbo.user_orgs', N'U') IS NOT NULL
BEGIN
	DROP TABLE user_orgs
END
CREATE  TABLE users (
  user_name NVARCHAR(100) NOT NULL ,
  password NVARCHAR(100) NOT NULL ,
  enabled bit NOT NULL ,
  PRIMARY KEY (user_name));

CREATE TABLE user_roles (
  user_role_id  [int] IDENTITY(1,1) NOT NULL,
  user_name NVARCHAR(100) NOT NULL,
  role NVARCHAR(100) NOT NULL,
  PRIMARY KEY (user_role_id));

CREATE TABLE user_orgs (
  organization_id   NVARCHAR(100)  NOT NULL,
  user_name         NVARCHAR(100)   NOT NULL,
  PRIMARY KEY (user_name));

INSERT INTO users(user_name,password,enabled) VALUES ('john.carnell','$2a$04$NX3QTkBJB00upxKeaKqFBeoIVc9JHvwVnj1lItxNphRj34wNx5wlu', 1);
INSERT INTO users(user_name,password,enabled) VALUES ('william.woodward','$2a$04$lM2hIsZVNYrQLi8mhvnTA.pheZtmzeivz6fyxCr9GZ6YSfP6YibCW', 1);

INSERT INTO user_roles (user_name, role) VALUES ('john.carnell', 'ROLE_USER');
INSERT INTO user_roles (user_name, role) VALUES ('william.woodward', 'ROLE_ADMIN');
INSERT INTO user_roles (user_name, role) VALUES ('william.woodward', 'ROLE_USER');

INSERT INTO user_orgs (organization_id, user_name) VALUES ('d1859f1f-4bd7-4593-8654-ea6d9a6a626e', 'john.carnell');
INSERT INTO user_orgs (organization_id, user_name) VALUES ('42d3d4f5-9f33-42f4-8aca-b7519d6af1bb', 'william.woodward');
*/