package com.lava.br.authentication.security;

import com.lava.br.authentication.repositories.CustomerUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;

//Imports removed for conciseness
@Configuration
public class JWTOAuth2Config extends AuthorizationServerConfigurerAdapter {

    @Autowired
    public void authenticationManager(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(customerUserDetailsService);
    }
    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;
    /*@Autowired
    private UserDetailsService userDetailsService;*/
    @Autowired
    CustomerUserDetailsService customerUserDetailsService;
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private DefaultTokenServices tokenServices;
    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    @Autowired
    private TokenEnhancer jwtTokenEnhancer;
    @Autowired
    MongoClientDetailsService mongoClientDetailsService;

    @Autowired
    private UserApprovalHandler userApprovalHandler;

    @Override
    public void configure(
            AuthorizationServerEndpointsConfigurer endpoints)
            throws Exception {
        TokenEnhancerChain tokenEnhancerChain =
                new TokenEnhancerChain();
        tokenEnhancerChain
                .setTokenEnhancers(
                        Arrays.asList(
                                //jwtTokenEnhancer ,
                                jwtAccessTokenConverter));
        endpoints
                .tokenStore(tokenStore)
               // .tokenEnhancer(tokenEnhancerChain)
                .accessTokenConverter(jwtAccessTokenConverter)
                //.userApprovalHandler(userApprovalHandler)
                .authenticationManager(authenticationManager)
                .setClientDetailsService(mongoClientDetailsService);
                //.userDetailsService(customerUserDetailsService);
    }
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
clients.withClientDetails(mongoClientDetailsService);
        /*clients.inMemory()
                .withClient("eagleeye")
                .secret("thisissecret")
                .authorizedGrantTypes("client_credentials","implicit", "refresh_token", "password", "authorization_code")
                .scopes("webclient", "mobileclient")
                .accessTokenValiditySeconds(60000);*/
                //.authorities("ROLE_ADMIN", "ROLE_USER");
    }


    //private static String REALM="MY_OAUTH_REALM";
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()");
               // .allowFormAuthenticationForClients();
         /*security
                .passwordEncoder(new StandardPasswordEncoder()) // added encoder
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()");*/

        //security.realm(REALM+"/client");
    }
}
