package com.lava.br.authentication.security;

import com.lava.br.authentication.repositories.CustomerUserDetailsService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;


@Configuration
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    /*@Bean
    public UserDetailsService mongoUserDetails() {
        return new CustomerUserDetailsService();
    }*/

    @Autowired
    CustomerUserDetailsService customerUserDetailsService;

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /*@Override
    @Bean
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return super.userDetailsServiceBean();
    }*/


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        StandardPasswordEncoder f=new StandardPasswordEncoder();
        String gg = f.encode("pass");

        //UserDetailsService userDetailsService = mongoUserDetails();
        auth.userDetailsService(customerUserDetailsService)
                .passwordEncoder(new StandardPasswordEncoder());
/*
        auth
                .inMemoryAuthentication()
                .withUser("john.carnell").password("password1").roles("USER")
                .and()
                .withUser("william.woodward").password("password2").roles("USER", "ADMIN");*/
    }


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http
                .authorizeRequests()
                .antMatchers("*/oauth/**")
                .permitAll()
        .and()
                .authorizeRequests()
                .antMatchers("*/user")
                .hasRole("USER");
        /*http
                .requestMatchers()
                .antMatchers("/oauth/**")
                .and()
                .authorizeRequests()
                .antMatchers("/oauth/**")
                .authenticated();*/
    }

    @Autowired
    MongoClientDetailsService mongoClientDetailsService;

    @Bean
    @Autowired
    public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore){
        TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
        handler.setTokenStore(tokenStore);
        handler.setRequestFactory(new DefaultOAuth2RequestFactory(mongoClientDetailsService));
        handler.setClientDetailsService(mongoClientDetailsService);
        return handler;
    }
    @Bean
    @Autowired
    public ApprovalStore approvalStore(TokenStore tokenStore) throws Exception {
        TokenApprovalStore store = new TokenApprovalStore();
        store.setTokenStore(tokenStore);
        return store;
    }

}
