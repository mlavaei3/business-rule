package com.lava.br.authentication.repositories;

import com.lava.br.authentication.repositories.bean.MongoUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
public class CustomerUserDetailsService implements UserDetailsService {
	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//MongoUserDetails ff=new MongoUserDetails("mehdi2", "pass",new String[] { "ADMIN"});
		//mongoTemplate.insert(ff);
		
		Query query = new Query(Criteria.where("username").is(username));
		MongoUserDetails user = mongoTemplate.find(query, MongoUserDetails.class).get(0);

		if (user != null) {
			return user;
		}
		return null;
	}
}
