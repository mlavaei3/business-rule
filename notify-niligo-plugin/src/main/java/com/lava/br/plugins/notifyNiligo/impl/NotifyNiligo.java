package com.lava.br.plugins.notifyNiligo.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.notifyNiligo.services.NotifyNiligoService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("notifyNiligo")
@Pluggable(nameOfPlugin = "notifyNiligo")
public class NotifyNiligo extends BasePluginWithCamel {
    @PostConstruct
    public void init() {

        // configs
        /**
         * account
         */
        PartyField partyField=PartyField
                .builder()
                .name("account")
                .label("account")
                .type(TypeField.PartySelectList)
                .refActions(Arrays.asList("default"))
                .visibility(true)
                .order(1)
                .build();
        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        partyField.setPartyUrl(tokenUri+"/oauth/authorize?response_type=code&scopes=read&client_id="+userPass.split(":")[0]+"&redirect_uri="+ UrlUtils.urlEncode(redirectUri));
        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);
        partyField.setFieldValidators(fieldValidators);
        this.getInfo().getConfigVariables().add(partyField);
        /**
         * notifyNiligoList
         */
        LazySelectField notifyNiligoListField=LazySelectField.builder()
                .fieldClass(LazySelectField.class)
                .name("notifyNiligoList")
                .label("notifyNiligoList")
                .refActions(Arrays.asList("default"))
                .type(TypeField.LazySelectList)
                .visibility(true)
                .order(2)
                .build();

        notifyNiligoListField.setSelectFields(this::getNotifyNiligos);
        notifyNiligoListField.setRequestUrl("?query=${account}");
        fieldValidators=new ArrayList<>();
        fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);
        notifyNiligoListField.setFieldValidators(fieldValidators);

        fieldValidators=new ArrayList<>();
        FieldValidator fvReset=FieldValidator.builder()
                .type(ValidatorType.DeptReset)
                .expression("account")
                .build();
        fieldValidators.add(fvReset);
        notifyNiligoListField.setFieldValidators(fieldValidators);

        this.getInfo().getConfigVariables().add(notifyNiligoListField);

        /**
         * message
         */
        Field field=new Field();
        field.setName("message");
        field.setVisibility(true);
        field.setLabel("message");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(4);
        fieldValidators=new ArrayList<>();
        fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);
        field.setFieldValidators(fieldValidators);
        this.getInfo().getOutVariables().add(field);

        /**
         * title
         */
        field=new Field();
        field.setName("title");
        field.setVisibility(true);
        field.setLabel("title");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(5);
        this.getInfo().getOutVariables().add(field);

        /**
         * summary
         */
        field=new Field();
        field.setName("summary");
        field.setVisibility(true);
        field.setLabel("summary");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(6);
        this.getInfo().getOutVariables().add(field);

        /**
         * ticker
         */
        field=new Field();
        field.setName("ticker");
        field.setVisibility(true);
        field.setLabel("ticker");
        field.setType(TypeField.Text);
        field.setFieldClass(String.class);
        field.setOrder(7);
        this.getInfo().getOutVariables().add(field);

        /**
         * time
         */
        field=new Field();
        field.setName("time");
        field.setVisibility(true);
        field.setLabel("time");
        field.setType(TypeField.Number);
        field.setFieldClass(Long.class);
        field.setOrder(8);

        fieldValidators=new ArrayList<>();
        FieldValidator fvMin=FieldValidator.builder()
                .type(ValidatorType.Min)
                .expression("0")
                .build();
        fieldValidators.add(fvMin);
        field.setFieldValidators(fieldValidators);
        this.getInfo().getOutVariables().add(field);



    }

    @Value("${lava.plugins.notifyNiligo.api_uri}")
    private String apiUri;

    @Value("${lava.plugins.notifyNiligo.m2m_uri}")
    private String m2mUuri;

    @Value("${lava.plugins.notifyNiligo.auth2.redirect_uri}")
    private String redirectUri;

    @Value("${lava.plugins.notifyNiligo.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.notifyNiligo.auth2.user_pass}")
    private String userPass;

    public Map<String, String> getAccounts(Map<String, Object> objectMap){

        NotifyNiligoService notifyNiligoService= ApplicationContextProvider.getApplicationContext().getBean("notifyNiligoService",NotifyNiligoService.class);
        Map<String,String> accounts=notifyNiligoService.getAllValueForKey(objectMap.get("userId").toString(),DbStoreType.ACCOUNTING,"account","token");
        accounts.forEach((k,v)->{accounts.put(k,k);});
        return accounts;
    }

    public Map<String, String> getNotifyNiligos(Map<String, Object> objectMap){
        Map<String,String> map=new HashMap<>();
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            NotifyNiligoService  notifyNiligoService= (NotifyNiligoService) applicationContext.getBean("notifyNiligoService");
            String token= notifyNiligoService.getValueForKey(objectMap.get("userId").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    objectMap.get("query").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);
            HttpEntity<String> request1=new HttpEntity<>(headers1);

            String resourceUrl=apiUri+"/api/thing/list?type=phone";
            ResponseEntity<ThingDto[]> rsp1 = restTemplate.exchange(resourceUrl, HttpMethod.GET, request1, ThingDto[].class);

            for (ThingDto thingDto: rsp1.getBody()){
                map.put(thingDto.getUuid(),thingDto.getName());
            }
            return map;
        } catch (IOException e) {
            return map;
        }
    }

    @Override
    public void consume(IPlugin plugin) {

    }

    public void produce(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            NotifyNiligoService  notifyNiligoService= (NotifyNiligoService) applicationContext.getBean("notifyNiligoService");
            String token= notifyNiligoService.getValueForKey(plugin.getInfo().getValue(PluginInfo.OUT,"userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG,"account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);


            String actionType=plugin.getInfo().getValue(PluginInfo.CONFIG,"producerAction").toString();
            String resourceUrl=m2mUuri+"/m2m/phone/notification";
            Map<String,String> map=new HashMap<>();
            map.put("uuid",plugin.getInfo().getValue(PluginInfo.CONFIG,"notifyNiligoList").toString());
            map.put("message",plugin.getInfo().getValue(PluginInfo.OUT,"message").toString());
            map.put("time",plugin.getInfo().getValue(PluginInfo.OUT,"time").toString());
            map.put("summary",plugin.getInfo().getValue(PluginInfo.OUT,"summary").toString());
            map.put("title",plugin.getInfo().getValue(PluginInfo.OUT,"title").toString());
            map.put("ticker",plugin.getInfo().getValue(PluginInfo.OUT,"ticker").toString());
            HttpEntity<Map<String,String>> request1=new HttpEntity<>(map,headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            System.out.println(rsp1.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "account")!=null?
                plugin.getInfo().getValue(PluginInfo.CONFIG, "notifyNiligoList").toString().trim()
                :null;
    }
}