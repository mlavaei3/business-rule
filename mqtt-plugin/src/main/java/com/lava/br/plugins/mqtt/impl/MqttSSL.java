package com.lava.br.plugins.mqtt.impl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

@Configuration
public class MqttSSL {
    @Bean(name="lavasslcontext")
    SSLContext getSSLContext() {
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            //InputStream jksInputStream=new FileInputStream(new File("C:\\Users\\Aria\\Downloads\\ssl\\niligo_jks.jks"));
            InputStream jksInputStream=getClass().getClassLoader().getResourceAsStream("niligo_jks.jks");

            ks.load(jksInputStream,"abcdef1".toCharArray());

            TrustManager[] trustManagers=null;
            TrustManagerFactory tmf=TrustManagerFactory.getInstance("X509");
            tmf.init(ks);
            trustManagers=tmf.getTrustManagers();


            SSLContext sslcontext = SSLContext.getInstance("TLSv1");
            sslcontext.init(null, trustManagers, null);

            return sslcontext ;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }
}
