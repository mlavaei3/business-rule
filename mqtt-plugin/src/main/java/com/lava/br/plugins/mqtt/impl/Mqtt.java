package com.lava.br.plugins.mqtt.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.Field;
import com.lava.br.plugins.mqtt.routes.MqttImapRoute;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.component.mqtt.MQTTEndpoint;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("mqtt")
@Pluggable(nameOfPlugin = "mqtt")
public class Mqtt extends BasePluginWithCamel {
    @PostConstruct
    public void init() {
        // configs
        /*String[] triggers=new String[1];
        String[] actions=new String[1];
        triggers[0]="default";
        actions[0]="default";
        this.getInfo().getConfigVariables().add(new Field("username", java.lang.String.class,  "username",triggers,actions,1));
        this.getInfo().getConfigVariables().add(new Field("password", java.lang.String.class,  "password",triggers,actions,2));
        this.getInfo().getConfigVariables().add(new Field("host", java.lang.String.class,  "host",triggers,actions,3));
        this.getInfo().getConfigVariables().add(new Field("port", java.lang.String.class,  "port",triggers,actions,4));
        this.getInfo().getConfigVariables().add(new Field("subscribeTopicName", java.lang.String.class,  "subscribeTopicName",triggers,null,5));
        this.getInfo().getConfigVariables().add(new Field("publishTopicName", java.lang.String.class,  "publishTopicName",null,actions,1));

        // inVar
        this.getInfo().getInVariables().add(new Field("subscribeTopicName", java.lang.String.class,  "subscribeTopicName",1));
        this.getInfo().getInVariables().add(new Field("message", java.lang.String.class,  "message",2));

        // outVar
        this.getInfo().getOutVariables().add(new Field("message", java.lang.String.class,  "message",2));

        this.getInfo().getOutVariables().add(new Field("typeConnect", java.lang.String.class,  "typeConnect",7));
        this.getInfo().getOutVariables().add(new Field("beanSSLName", java.lang.String.class,  "beanSSLName",8));
*/
    }


    @Override
    public void consume(IPlugin plugin) {
        try {
            this.unConsume(plugin);
            getCamelContext().addRoutes(new MqttImapRoute((Mqtt) plugin,this.getUniqueIdentity(plugin)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //tcp or ssl
    public void produce(IPlugin plugin) {
        String typeConnect=plugin.getInfo().getValue(PluginInfo.OUT, "typeConnect").toString();

        MQTTEndpoint endpoint = (MQTTEndpoint)  getCamelContext()
                .getEndpoint("mqtt:"+plugin.getInfo().getValue(PluginInfo.OUT, "userid")
                        +"?host="+typeConnect+"://"+plugin.getInfo().getValue(PluginInfo.CONFIG, "host")
                        +":"+plugin.getInfo().getValue(PluginInfo.CONFIG, "port")
                        + "&publishTopicName=" + plugin.getInfo().getValue(PluginInfo.OUT, "publishTopicName")
                        + "&userName=" + plugin.getInfo().getValue(PluginInfo.CONFIG, "username")
                        + "&sendWaitInSeconds=300&connectWaitInSeconds=30&clientId="+plugin.getInfo().getValue(PluginInfo.OUT, "username")
                        + "&password=" + plugin.getInfo().getValue(PluginInfo.CONFIG, "password")
                        +(typeConnect.equalsIgnoreCase("ssl")?"&sslContext=#"+plugin.getInfo().getValue(PluginInfo.OUT, "beanSSLName"):""));



        Exchange exchange = endpoint.createExchange();
        Message in = exchange.getIn();
        in.setBody(plugin.getInfo().getValue(PluginInfo.OUT, "message") == null ? "empty" : plugin.getInfo().getValue(PluginInfo.OUT, "message"));

        runTemplateProducer(endpoint,exchange);

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return (plugin.getInfo().getValue(PluginInfo.CONFIG, "host").toString()
                +plugin.getInfo().getValue(PluginInfo.CONFIG, "port").toString()
                +plugin.getInfo().getValue(PluginInfo.CONFIG, "subscribeTopicName").toString()
                +plugin.getInfo().getValue(PluginInfo.CONFIG, "username").toString()
                +plugin.getInfo().getValue(PluginInfo.CONFIG, "password").toString()).trim();

    }
}