package com.lava.br.plugins.mqtt.restclients;

import com.lava.br.core.restclients.PluginRestTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MqttRestTemplateClient extends PluginRestTemplateClient {
    private static final Logger logger = LoggerFactory.getLogger(MqttRestTemplateClient.class);
}
