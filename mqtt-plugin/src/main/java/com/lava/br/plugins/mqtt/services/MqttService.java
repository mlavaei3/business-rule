package com.lava.br.plugins.mqtt.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.mqtt.impl.Mqtt;
import com.lava.br.plugins.mqtt.restclients.MqttRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MqttService extends PluginService {
    @Autowired
    private Mqtt mqtt;

    @Autowired
    private MqttRestTemplateClient mqttRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return mqtt;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return mqttRestTemplateClient;
    }
}
