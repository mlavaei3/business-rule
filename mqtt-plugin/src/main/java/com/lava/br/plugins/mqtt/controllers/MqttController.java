package com.lava.br.plugins.mqtt.controllers;


import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.mqtt.services.MqttService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MqttController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(MqttController.class);

    @Autowired
    private MqttService mqttService;

    @Override
    public PluginService getPluginService(){
         return mqttService;
    }

}
