package com.lava.br.plugins.mqtt.processes;

import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.processes.BaseProcessor;
import com.lava.br.core.repositories.bean.FieldAssigner;
import com.lava.br.plugins.mqtt.impl.Mqtt;
import org.apache.camel.Exchange;

import java.util.ArrayList;
import java.util.List;

public class MqttProcessor extends BaseProcessor {
    public MqttProcessor(Mqtt mqtt) {
        super(mqtt);
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        List<FieldAssigner> fieldAssigners=new ArrayList<FieldAssigner>();
        fieldAssigners.add(new FieldAssigner("host",this.getPlugin().getInfo().getValue(PluginInfo.CONFIG, "host").toString()));
        fieldAssigners.add(new FieldAssigner("port",this.getPlugin().getInfo().getValue(PluginInfo.CONFIG, "port").toString()));
        fieldAssigners.add(new FieldAssigner("username",this.getPlugin().getInfo().getValue(PluginInfo.CONFIG, "username").toString()));
        fieldAssigners.add(new FieldAssigner("password",this.getPlugin().getInfo().getValue(PluginInfo.CONFIG, "password").toString()));
        fieldAssigners.add(new FieldAssigner("subscribeTopicName",this.getPlugin().getInfo().getValue(PluginInfo.CONFIG, "subscribeTopicName").toString()));

        byte[] bb= (byte[]) exchange.getIn().getBody();
        getPlugin().getInfo().setValue(PluginInfo.IN, "message", new String(bb));
        getPlugin().getInfo().setValue(PluginInfo.IN, "subscribeTopicName", exchange.getIn().getHeader(org.apache.camel.component.mqtt.MQTTConfiguration.MQTT_SUBSCRIBE_TOPIC));

        sendMessageToServer();
    }

}
