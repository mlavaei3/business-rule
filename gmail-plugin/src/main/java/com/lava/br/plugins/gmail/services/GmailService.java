package com.lava.br.plugins.gmail.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.gmail.impl.Gmail;
import com.lava.br.plugins.gmail.restclients.GmailRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GmailService extends PluginService {
    @Autowired
    private Gmail gmail;

    @Autowired
    private GmailRestTemplateClient gmailRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return gmail;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return gmailRestTemplateClient;
    }
}
