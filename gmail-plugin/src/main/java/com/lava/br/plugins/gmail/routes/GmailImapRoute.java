package com.lava.br.plugins.gmail.routes;


import com.lava.br.core.routes.BaseRouteBuilder;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.gmail.impl.Gmail;
import com.lava.br.plugins.gmail.processes.GmailProcessor;

public class GmailImapRoute extends BaseRouteBuilder {

	public GmailImapRoute(Gmail gmail,String routeName) {
		super(gmail,routeName);
	}
	@Override
	public void configure() throws Exception {
		from("imaps://imap.gmail.com?username=" + getPlugin().getInfo().getValue("CONFIG", "username")
				+ "@gmail.com&password=" + getPlugin().getInfo().getValue("CONFIG", "password")
				+ "&delete=false&unseen=true&consumer.delay=6000")
				.routeId(getRouteName()) //getPlugin().getInfo().getValue("CONFIG", "username").toString())
				.autoStartup(false)
				.routePolicy(getPolicy())
				.process(new GmailProcessor((Gmail) getPlugin()));

	}

}
