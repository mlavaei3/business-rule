package com.lava.br.plugins.gmail.controllers;


import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.gmail.services.GmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GmailController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(GmailController.class);

    @Autowired
    private GmailService gmailService;

    @Override
    public PluginService getPluginService(){
         return gmailService;
    }

}
