package com.lava.br.plugins.gmail.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.Field;
import com.lava.br.core.repositories.bean.FieldValidator;
import com.lava.br.plugins.gmail.routes.GmailImapRoute;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("gmail")
@Pluggable(nameOfPlugin = "gmail")
public class Gmail extends BasePluginWithCamel {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);
        this.getInfo().setIsProducer(true);

        // configs
/*        String[] triggers=new String[1];
        String[] actions=new String[1];
        triggers[0]="default";
        actions[0]="default";*/
        Field field=new Field();
        field.setName("username");
        field.setLabel("username");
        field.setFieldClass(String.class);
        field.setOrder(1);
        field.setUnique(true);
        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);
        field.setFieldValidators(fieldValidators);
        this.getInfo().getConfigVariables().add(field);//Field.builder().name("username").fieldClass(String.class).label("username").unique(true).order(1).build());
        field=new Field();
        field.setName("password");
        field.setLabel("password");
        field.setFieldClass(String.class);
        field.setOrder(2);
        fieldValidators=new ArrayList<>();
        fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);
        field.setFieldValidators(fieldValidators);
        this.getInfo().getConfigVariables().add(field);//Field.builder().name("password").fieldClass(String.class).label("password").order(2).build());

        // inVar
        field=new Field();
        field.setName("from");
        field.setLabel("from");
        field.setFieldClass(String.class);
        field.setOrder(1);
        this.getInfo().getInVariables().add(field);//Field.builder().name("from").fieldClass( String.class).label("from").order(1).build());
        field=new Field();
        field.setName("subject");
        field.setLabel("subject");
        field.setFieldClass(String.class);
        field.setOrder(2);
        this.getInfo().getInVariables().add(field);//Field.builder().name("subject").fieldClass( String.class).label("subject").order(2).build());
        field=new Field();
        field.setName("body");
        field.setLabel("body");
        field.setFieldClass(String.class);
        field.setOrder(3);
        this.getInfo().getInVariables().add(field);//Field.builder().name("body").fieldClass( String.class).label("body").order(3).build());

        // outVar
        field=new Field();
        field.setName("to");
        field.setLabel("to");
        field.setFieldClass(String.class);
        field.setOrder(3);
        fieldValidators=new ArrayList<>();
        fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvReq);
        field.setFieldValidators(fieldValidators);
        this.getInfo().getOutVariables().add(field);//Field.builder().name("to").fieldClass( String.class).label("to").order(3).build());
        field=new Field();
        field.setName("subject");
        field.setLabel("subject");
        field.setFieldClass(String.class);
        field.setOrder(4);
        this.getInfo().getOutVariables().add(field);//Field.builder().name("subject").fieldClass( String.class).label("subject").order(4).build());
        field=new Field();
        field.setName("body");
        field.setLabel("body");
        field.setFieldClass(String.class);
        field.setOrder(5);
        this.getInfo().getOutVariables().add(field);//Field.builder().name("body").fieldClass( String.class).label("body").order(5).build());
    }

    @Override
    public void consume(IPlugin plugin) {
        try {
            this.unConsume(plugin);
            getCamelContext().addRoutes(new GmailImapRoute((Gmail) plugin,this.getUniqueIdentity(plugin)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void produce(IPlugin plugin) {
        Endpoint endpoint = getCamelContext()
                .getEndpoint("smtps://smtp.gmail.com?username=" + plugin.getInfo().getValue(PluginInfo.CONFIG, "username").toString()
                        + "@gmail.com&password=" + plugin.getInfo().getValue(PluginInfo.CONFIG, "password").toString()

                );
        Exchange exchange = endpoint.createExchange();
        Message in = exchange.getIn();
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("to", plugin.getInfo().getValue(PluginInfo.OUT, "to").toString());
        headers.put("subject", plugin.getInfo().getValue(PluginInfo.OUT, "subject").toString());
        headers.put("contentType", "text/plain;charset=UTF-8");
        in.setHeaders(headers);
        in.setBody(plugin.getInfo().getValue(PluginInfo.OUT, "body") == null ? "empty" : plugin.getInfo().getValue("OUT", "body").toString());
        // in.addAttachment("attachment.zip", new
        // DataHandler(applicationContext.getResource("file:test.zip").getURL()));

        runTemplateProducer(endpoint,exchange);

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "username")!=null?
                plugin.getInfo().getValue(PluginInfo.CONFIG, "username").toString().trim()
                :null;
    }
}