package com.lava.br.plugins.gmail;

import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//import org.springframework.cloud.stream.annotation.EnableBinding;
//import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@ComponentScan(value = "com.lava.br")
@EnableMongoRepositories(basePackageClasses = com.lava.br.core.beans.IBusinessRuleRepository.class)
@EnableEurekaClient
@EnableCircuitBreaker
@EnableCaching
//@EnableResourceServer
//@Slf4j
public class GmailApplication {
    public static void main(String[] args) {
        SpringApplication.run(GmailApplication.class, args);
    }
	/*@Bean
	public AlwaysSampler defaultSampler() {
		return new AlwaysSampler();
	}
*/
}
