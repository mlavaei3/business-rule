package com.lava.br.plugins.prism.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.dbstore.impl.DbStore;
import com.lava.br.core.plugins.utils.niligo.Endpoints;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.prism.impl.Prism;
import com.lava.br.plugins.prism.services.PrismService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;


@RestController
public class PrismController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(PrismController.class);

    @Value("${lava.plugins.prism.auth2.user_pass}")
    private String userPassForToken;

    @Value("${lava.plugins.prism.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.prism.auth2.redirect_uri}")
    private String redirectUri;

    @Autowired
    private PrismService prismService;

    @Override
    public PluginService getPluginService(){
        return prismService;
    }

    private Endpoints endpoints;

    private Endpoints getEndpoints(){
        if (endpoints==null) {
            endpoints = new Endpoints(userPassForToken, tokenUri, redirectUri, getPluginService());
        }
        return endpoints;
    }

    @RequestMapping(value="testDb/{brId}",method = RequestMethod.GET)
    public String testDb(@PathVariable("brId") String brId) {
        prismService.addKeyWithValue("mehdi",DbStoreType.ACCOUNTING,"account","acc1","token","ttt22");
        prismService.addKeyWithValue("mehdi",DbStoreType.ACCOUNTING,"account","acc2","token","ttt2222");

        Map<String,String> maps= prismService.getAllValueForKey("mehdi",DbStoreType.ACCOUNTING,"account","token");
        String gg= prismService.getValueForKey("mehdi",DbStoreType.ACCOUNTING,"account","acc1","token");
        return "ok";
    }

    @RequestMapping(value="/testSocket",method = RequestMethod.GET)
    public void testSocket(@RequestParam(value ="sessionId",required = true) String sessionId,
                           @RequestParam(value ="objectId",required = true) String objectId ,
                           @RequestParam(value ="email",required = true) String email) {

        prismService.sendToSocket(sessionId,objectId,"{\""+email+ "\":\""+email+"\"}");

    }

    @RequestMapping(value="/callbackFromParty",method = RequestMethod.GET)
    public ResponseEntity callbackFromParty(@RequestParam(value ="state",required = true) String state,
                                            @RequestParam(value ="code",required = true) String code)  {
        return getEndpoints().callbackFromParty(state,code);
        /*String sessionId=state.split(" ")[0];
        String objectId=state.split(" ")[1];
        String userid=state.split(" ")[2];

        //String userPass = "ghasem:sadeghi";
        String basicEncoded = Base64Utils.encodeToString(userPassForToken.getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + basicEncoded);
        HttpEntity<String> request = new HttpEntity<>(headers);

        String url = tokenUri+"/oauth/token?";
        url += "code=" + code;
        url += "&grant_type=authorization_code";
        url += "&redirect_uri="+redirectUri;

        try {
            RestTemplate restTemplate=new RestTemplate();
            ResponseEntity<String> rsp = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

            //(2)extract the Access Token From the recieved JSON response
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(rsp.getBody());
            String accessToken = node.path("access_token").asText();

            getPluginService().addKeyWithValue(userid,
                    "account",
                    node.path("username").asText(),"token",
                    String.format("{\"accessToken\":\"%s\"}",accessToken));

            prismService.sendToSocket(sessionId,objectId,"{\""+node.path("username").asText()+ "\":\""+node.path("username").asText()+"\"}");


        } catch (HttpClientErrorException e) {
            System.out.println("\n1:"+e.getStatusCode());
            System.out.println("\n2:"+e.getResponseBodyAsString());
            //model.addAttribute("message", e.getResponseBodyAsString());
            //return "error";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("<script language='javascript'>parentwin = window.self;\n" +
                "parentwin.opener = window.self;\n" +
                "parentwin.close(); </script>");*/

    }

    @RequestMapping(value="/getPrisms",method = RequestMethod.GET)
    public ResponseEntity getPrisms(Principal principal, @RequestParam(value ="account",required = true) String account) {
        Map<String, Object> objectMap=new HashMap<>();
        objectMap.put("userId",principal.getName());
        objectMap.put("query",account);
        Map<String, String> prismMap=((Prism)prismService.getPlugin()).getPrisms(objectMap);
        return ResponseEntity.ok(prismMap);
    }
}
