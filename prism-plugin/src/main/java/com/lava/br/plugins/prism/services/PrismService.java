package com.lava.br.plugins.prism.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.dbstore.impl.DbStore;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.prism.impl.Prism;
import com.lava.br.plugins.prism.restclients.PrismRestTemplateClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class PrismService extends PluginService {


    @Autowired
    private Prism prism;

    @Autowired
    private PrismRestTemplateClient prismRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return prism;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return prismRestTemplateClient;
    }


  /*  public void addAccountWithToken(String userId,String account,String token){
        try {
            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid","mehdi");
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object","[{\"account\":\"mlavaei3\",\"token\":\"token1\"},{\"account\":\"mlavaei4\",\"token\":\"token4\"}]");
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name","gmail");
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",DbStoreType.ACCOUNTING);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","save");
            //dbStore.produce(dbStoreClient);

            DbStore dbStoreClient2=(DbStore)dbStore.getInstance();
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"userid","mehdi");
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"name","gmail");
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"dbStoreType",DbStoreType.ACCOUNTING);
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreClient2);
            String res=(String)dbStoreClient2.getInfo().getValue(PluginInfo.OUT,"result");
            final JSONArray obj = new JSONArray(res);
            final JSONObject geodata = obj.getJSONObject(0);
            res=geodata.getString("account");
            Map<String,String> mp=new HashMap<>();
            mp.put("account","acc1");
            mp.put("token","tt1");
            obj.put(2,mp);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object",obj.toString());
            dbStore.produce(dbStoreClient);


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
*/
}
