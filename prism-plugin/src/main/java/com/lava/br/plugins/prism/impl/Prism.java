package com.lava.br.plugins.prism.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.prism.services.PrismService;
import org.apache.camel.*;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.common.util.UrlUtils;
import org.apache.cxf.message.MessageContentsList;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("prism")
@Pluggable(nameOfPlugin = "prism")
public class Prism extends BasePluginWithCamel {
    @PostConstruct
    public void init() {

        // configs
        String[] triggers=new String[1];
        String[] actions=new String[1];
        triggers[0]="default";
        actions[0]="default";
        this.removeAction("default");
        this.addAction("changeState","changeState");
        this.addAction("changeStatePower","changeStatePower");
        this.addAction("changeStateColor","changeStateColor");
        this.addAction("notify","notify");
        /**
         * account
         */
        PartyField partyField=PartyField
                .builder()
                .name("account")
                .label("account")
                .type(TypeField.PartySelectList)
                .visibility(true)
                .refActions(Arrays.asList("changeState","changeStatePower","changeStateColor","notify"))
                .order(1)
                .build();
        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        partyField.setPartyUrl(tokenUri+"/oauth/authorize?response_type=code&scopes=read&client_id="+userPass.split(":")[0]+"&redirect_uri="+ UrlUtils.urlEncode(redirectUri));
        this.getInfo().getConfigVariables().add(partyField);
        /**
         * prismList
         */
        LazySelectField prismListField=LazySelectField.builder()
                .fieldClass(LazySelectField.class)
                .name("prismList")
                .label("prismList")
                .type(TypeField.LazySelectList)
                .visibility(true)
                .refActions(Arrays.asList("changeState","changeStatePower","changeStateColor","notify"))
                .order(2)
                .build();

        prismListField.setSelectFields(this::getPrisms);
        prismListField.setRequestUrl("?query=${account}");

        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReset=FieldValidator.builder()
                .type(ValidatorType.DeptReset)
                .expression("account")
                .build();
        fieldValidators.add(fvReset);
        prismListField.setFieldValidators(fieldValidators);

        this.getInfo().getConfigVariables().add(prismListField);

        /**
         * power
         */
        SimpleSelectField powerField=SimpleSelectField.builder()
                .name("power")
                .label("power")
                .visibility(true)
                .type(TypeField.SimpleSelectList)
                .fieldClass(SimpleSelectField.class)
                .order(3)
                .refActions(Arrays.asList("changeState","changeStatePower","notify"))
                .build();
        Map<Object,String> selectFields=new LinkedHashMap<>();
        selectFields.put(0,"OFF");
        selectFields.put(1,"ON");
        powerField.setSelectFields(selectFields);
        this.getInfo().getOutVariables().add(powerField);

        /**
         * color
         */
        Field field=new Field();
        field.setName("color");
        field.setVisibility(true);
        field.setLabel("color");
        field.setType(TypeField.Color);
        field.setFieldClass(String.class);
        field.setOrder(4);
        field.setRefActions(Arrays.asList("changeState","changeStateColor"));
        Field colorField=field;// Field.builder().name("color").label("color").type(TypeField.Color).visibility(true).fieldClass(String.class).order(4)
        // .refActions(Arrays.asList("changeState","changeStateColor","notify")) .build();
        this.getInfo().getOutVariables().add(colorField);
        /**
         * color Arr
         */
        Field fieldNotifyGroup=new Field();
        fieldNotifyGroup.setName("colorArr");
        fieldNotifyGroup.setVisibility(true);
        fieldNotifyGroup.setLabel("color");
        fieldNotifyGroup.setType(TypeField.Color);
        fieldNotifyGroup.setFieldClass(String.class);
        fieldNotifyGroup.setOrder(4);
        fieldNotifyGroup.setRefActions(Arrays.asList("notify"));
        // Field.builder().name("color").label("color").type(TypeField.Color).visibility(true).fieldClass(String.class).order(4)
        // .refActions(Arrays.asList("changeState","changeStateColor","notify")) .build();
        fieldValidators=new ArrayList<>();
        FieldValidator fvGroupArr=FieldValidator.builder()
                .type(ValidatorType.GroupArray)
                .expression("notifyArr")
                .build();
        FieldValidator fvReq=FieldValidator.builder()
                .type(ValidatorType.Required)
                .build();
        fieldValidators.add(fvGroupArr);
        fieldValidators.add(fvReq);

        fieldNotifyGroup.setFieldValidators(fieldValidators);

        this.getInfo().getOutVariables().add(fieldNotifyGroup);
        /**
         * fade
         */
        field=new Field();
        field.setName("fade");
        field.setVisibility(true);
        field.setLabel("fade");
        field.setType(TypeField.Number);
        field.setFieldClass(String.class);
        field.setOrder(5);
        field.setRefActions(Arrays.asList("changeState","changeStateColor"));
        Field fadeField=field;/*Field.builder()
                .name("fade")
                .label("fade")
                .visibility(true)
                .fieldClass(String.class)
                .type(TypeField.Number)
                .order(5)
                .refActions(Arrays.asList("changeState","changeStateColor","notify"))
                .build();*/
        fieldValidators=new ArrayList<>();
        FieldValidator fvMin=FieldValidator.builder()
                .type(ValidatorType.Min)
                .expression("0")
                .build();
        fieldValidators.add(fvMin);

        FieldValidator fvMax=FieldValidator.builder()
                .type(ValidatorType.Maxlength)
                .expression("640000")
                .build();
        fieldValidators.add(fvMax);

        fadeField.setFieldValidators(fieldValidators);
        this.getInfo().getOutVariables().add(fadeField);

        /**
         * fade group
         */
        field=new Field();
        field.setName("fadeArr");
        field.setVisibility(true);
        field.setLabel("fade");
        field.setType(TypeField.Number);
        field.setFieldClass(String.class);
        field.setOrder(5);
        field.setRefActions(Arrays.asList("notify"));
        Field fadeFieldArr=field;/*Field.builder()
                .name("fade")
                .label("fade")
                .visibility(true)
                .fieldClass(String.class)
                .type(TypeField.Number)
                .order(5)
                .refActions(Arrays.asList("changeState","changeStateColor","notify"))
                .build();*/
        fieldValidators=new ArrayList<>();
        fvMin=FieldValidator.builder()
                .type(ValidatorType.Min)
                .expression("0")
                .build();
        fieldValidators.add(fvMin);

        fvMax=FieldValidator.builder()
                .type(ValidatorType.Maxlength)
                .expression("640000")
                .build();
        fieldValidators.add(fvMax);

        fvGroupArr=FieldValidator.builder()
                .type(ValidatorType.GroupArray)
                .expression("notifyArr")
                .build();
        fieldValidators.add(fvGroupArr);

        fadeFieldArr.setFieldValidators(fieldValidators);
        this.getInfo().getOutVariables().add(fadeFieldArr);

        /**
         * delay
         */
        field=new Field();
        field.setName("delayArr");
        field.setVisibility(true);
        field.setLabel("delay");
        field.setType(TypeField.Number);
        field.setFieldClass(String.class);
        field.setOrder(6);
        field.setRefActions(Arrays.asList("notify"));
        Field delayField=field;/*Field.builder()
                .name("delay")
                .label("delay")
                .visibility(true)
                .fieldClass(String.class)
                .type(TypeField.Number)
                .order(6)
                .refActions(Arrays.asList("notify"))
                .build();*/
        fieldValidators=new ArrayList<>();
        fvMin=FieldValidator.builder()
                .type(ValidatorType.Min)
                .expression("0")
                .build();
        fieldValidators.add(fvMin);

        fvMax=FieldValidator.builder()
                .type(ValidatorType.Maxlength)
                .expression("640000")
                .build();
        fieldValidators.add(fvMax);

        fvGroupArr=FieldValidator.builder()
                .type(ValidatorType.GroupArray)
                .expression("notifyArr")
                .build();
        fieldValidators.add(fvGroupArr);

        delayField.setFieldValidators(fieldValidators);
        this.getInfo().getOutVariables().add(delayField);


        /**
         * portNumber
         */
        field=new Field();
        field.setName("portNumber");
        field.setVisibility(true);
        field.setLabel("portNumber");
        field.setType(TypeField.Number);
        field.setFieldClass(String.class);
        field.setOrder(7);
        field.setRefActions(Arrays.asList("changeState","changeStateColor","notify"));
        Field portNumberField=field;
        fieldValidators=new ArrayList<>();
        FieldValidator fvDef=FieldValidator.builder()
                .type(ValidatorType.DefaultValue)
                .expression("0")
                .build();
        fieldValidators.add(fvDef);
        field.setFieldValidators(fieldValidators);
        /*Field.builder()
                .name("portNumber")
                .label("portNumber")
                .visibility(true)
                .fieldClass(String.class)
                .type(TypeField.Number)
                .order(7)
                .refActions(Arrays.asList("changeState","changeStateColor","notify"))
                .build();*/
        this.getInfo().getOutVariables().add(portNumberField);

    }

    @Value("${lava.plugins.prism.api_uri}")
    private String apiUri;

    @Value("${lava.plugins.prism.m2m_uri}")
    private String m2mUuri;

    @Value("${lava.plugins.prism.auth2.user_pass}")
    private String userPass;

    @Value("${lava.plugins.prism.auth2.redirect_uri}")
    private String redirectUri;

    @Value("${lava.plugins.prism.auth2.token_uri}")
    private String tokenUri;


    public Map<String, String> getAccounts(Map<String, Object> objectMap){

        PrismService prismService= ApplicationContextProvider.getApplicationContext().getBean("prismService",PrismService.class);
        Map<String,String> accounts=prismService.getAllValueForKey(objectMap.get("userId").toString(),DbStoreType.ACCOUNTING,"account","token");
        accounts.forEach((k,v)->{accounts.put(k,k);});
        return accounts;
    }

    public Map<String, String> getPrisms(Map<String, Object> objectMap){
        Map<String,String> map=new HashMap<>();
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            PrismService  prismService= (PrismService) applicationContext.getBean("prismService");
            String token= prismService.getValueForKey(objectMap.get("userId").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    objectMap.get("query").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);
            HttpEntity<String> request1=new HttpEntity<>(headers1);

            String resourceUrl=apiUri+"/api/thing/list?type=prism";
            ResponseEntity<ThingDto[]> rsp1 = restTemplate.exchange(resourceUrl, HttpMethod.GET, request1, ThingDto[].class);
            for (ThingDto thingDto: rsp1.getBody()){
                map.put(thingDto.getUuid(),thingDto.getName());
            }
            return map;
        } catch (IOException e) {
            return map;
        }
    }

    @Override
    public void consume(IPlugin plugin) {

    }

    public void produce(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            PrismService  prismService= (PrismService) applicationContext.getBean("prismService");
            String token= prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.OUT,"userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG,"account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);


            String actionType=plugin.getInfo().getValue(PluginInfo.CONFIG,"producerAction").toString();
            String resourceUrl=m2mUuri+"/m2m/prism/";
            Map<String,String> map=new HashMap<>();
            map.put("port",plugin.getInfo().getValue(PluginInfo.OUT,"portNumber").toString());
            map.put("uuid",plugin.getInfo().getValue(PluginInfo.CONFIG,"prismList").toString());

            if(actionType.equals("changeState")){
                resourceUrl+="state";
                map.put("power",plugin.getInfo().getValue(PluginInfo.OUT,"power").toString());
                map.put("color",plugin.getInfo().getValue(PluginInfo.OUT,"color").toString().replaceAll("#",""));
                map.put("fade",plugin.getInfo().getValue(PluginInfo.OUT,"fade").toString());

            }else if(actionType.equals("changeStatePower")){
                resourceUrl+="state";
                map.put("power",plugin.getInfo().getValue(PluginInfo.OUT,"power").toString());

            }else if(actionType.equals("changeStateColor")){
                resourceUrl+="state";
                map.put("power","1");
                map.put("color",plugin.getInfo().getValue(PluginInfo.OUT,"color").toString().replaceAll("#",""));
                map.put("fade",plugin.getInfo().getValue(PluginInfo.OUT,"fade").toString());

            }else if(actionType.equals("notify")){
                resourceUrl+="notify";
                map.put("delays",plugin.getInfo().getValue(PluginInfo.OUT,"delayArr").toString()
                        .replaceAll("#","")
                        .replaceAll("\\[","")
                        .replaceAll("]","")
                        .replaceAll("\"",""));
                map.put("colors",plugin.getInfo().getValue(PluginInfo.OUT,"colorArr").toString()
                        .replaceAll("#","")
                        .replaceAll("\\[","")
                        .replaceAll("]","")
                        .replaceAll("\"",""));
                map.put("fades",plugin.getInfo().getValue(PluginInfo.OUT,"fadeArr").toString() .replaceAll("#","")
                        .replaceAll("\\[","")
                        .replaceAll("]","")
                        .replaceAll("\"",""));
            }
            HttpEntity<Map<String,String>> request1=new HttpEntity<>(map,headers1);
            System.out.println(request1.getBody());
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            System.out.println(rsp1.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*Endpoint endpoint = getCamelContext()
                .getEndpoint("cxfrs:bean:rsClient");
        Exchange exchange = endpoint.createExchange();
        exchange.setPattern(ExchangePattern.InOut);

        Message inMessage = exchange.getIn();
        // set the operation name
        inMessage.setHeader(CxfConstants.OPERATION_NAME,  plugin.getInfo().getValue(PluginInfo.CONFIG,"operationName"));
        // using the proxy client API
        inMessage.setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);

        //creating the request
        MessageContentsList req = new MessageContentsList();

        req.add(plugin.getInfo().getValue(PluginInfo.CONFIG,"username"));
        req.add(plugin.getInfo().getValue(PluginInfo.CONFIG,"producerAction"));
        req.addAll((ArrayList)plugin.getInfo().getValue(PluginInfo.OUT,"paramsList"));

        inMessage.setBody(req);
        runTemplateProducer(endpoint,exchange);

        try {
            ProducerTemplate templatePrism=getCamelContext().createProducerTemplate();
            Map response = templatePrism.requestBody("direct:marsha2json", exchange.getOut().getBody(String.class),Map.class);
            plugin.getInfo().setValue(PluginInfo.IN,"result",response.get("entity"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
        }*/

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "account")!=null?
                plugin.getInfo().getValue(PluginInfo.CONFIG, "prismList").toString().trim()
                :null;
    }
}