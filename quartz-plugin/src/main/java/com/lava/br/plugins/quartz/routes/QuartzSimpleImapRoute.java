package com.lava.br.plugins.quartz.routes;


import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.routes.BaseRouteBuilder;
import com.lava.br.plugins.quartz.impl.Quartz;
import com.lava.br.plugins.quartz.processes.QuartzProcessor;

public class QuartzSimpleImapRoute extends BaseRouteBuilder {

	public QuartzSimpleImapRoute(Quartz quartz, String routeName) {
		super(quartz,routeName);
	}
	@Override
	public void configure() throws Exception {
	 	from("timer://"+ getRouteName()
				+"/?period="
				+ getPlugin().getInfo().getValue(PluginInfo.CONFIG, "repeatInterval").toString().trim()
				+ "s&repeatCount=" //000 add 1000 to change miliseconds
				+ (Integer.parseInt(getPlugin().getInfo().getValue(PluginInfo.CONFIG, "repeatCount").toString()))
				+ "&delay="
				+ (Integer.parseInt(getPlugin().getInfo().getValue(PluginInfo.CONFIG, "repeatInterval").toString()))
				+"s"
		)
				.routeId(getRouteName()) //getPlugin().getInfo().getValue("CONFIG", "username").toString())
				//.autoStartup(false)
				//.routePolicy(getPolicy())
				.process(new QuartzProcessor((Quartz) getPlugin()));

	}

}
