package com.lava.br.plugins.quartz.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.quartz.impl.Quartz;
import com.lava.br.plugins.quartz.restclients.QuartzRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuartzService extends PluginService {
    @Autowired
    private Quartz quartz;

    @Autowired
    private QuartzRestTemplateClient quartzRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return quartz;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return quartzRestTemplateClient;
    }
}
