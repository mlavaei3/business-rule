package com.lava.br.plugins.quartz.restclients;

import com.lava.br.core.restclients.PluginRestTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class QuartzRestTemplateClient extends PluginRestTemplateClient {
    private static final Logger logger = LoggerFactory.getLogger(QuartzRestTemplateClient.class);
}
