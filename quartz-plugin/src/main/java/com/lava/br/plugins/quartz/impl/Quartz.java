package com.lava.br.plugins.quartz.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.Field;

import com.lava.br.plugins.quartz.routes.QuartzCronImapRoute;
import com.lava.br.plugins.quartz.routes.QuartzSimpleImapRoute;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("quartz")
@Pluggable(nameOfPlugin = "quartz")
public class Quartz extends BasePluginWithCamel {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);
        this.getInfo().setIsProducer(false);
        // configs
        this.removeTrigger("default");
        this.addTrigger("SimpleTrigger","Simple Trigger");
        this.addTrigger("CronTrigger","Cron Trigger");

        Field cronField= new Field();//("cron", String.class, "cron",true,new String[]{"CronTrigger"},null,1);
        cronField.removeDefaultTriggersAndActions();
        cronField.setName("cron");
        cronField.setLabel("cron");
        cronField.setType(TypeField.Cron);
        cronField.setFieldClass(java.lang.String.class);
        List<String> listAction=new ArrayList<>();
        listAction.add("CronTrigger");
        cronField.setRefTriggers(listAction);
        cronField.setOrder(1);
        this.getInfo().getConfigVariables().add(cronField);

        Field repeatCountField=new Field();//("repeatCount", Integer.class, "repeatCount",true,new String[]{"SimpleTrigger"},null,2);
        repeatCountField.removeDefaultTriggersAndActions();
        repeatCountField.setName("repeatCount");
        repeatCountField.setLabel("repeatCount");
        repeatCountField.setType(TypeField.Number);
        repeatCountField.setFieldClass(java.lang.Integer.class);
        listAction=new ArrayList<>();
        listAction.add("SimpleTrigger");
        repeatCountField.setRefTriggers(listAction);
        repeatCountField.setOrder(2);
        this.getInfo().getConfigVariables().add(repeatCountField);

        Field repeatIntervalField=new Field();//("repeatInterval", Long.class, "repeatInterval",true,new String[]{"SimpleTrigger"},null,3);
        repeatIntervalField.removeDefaultTriggersAndActions();
        repeatIntervalField.setName("repeatInterval");
        repeatIntervalField.setLabel("repeatInterval");
        repeatIntervalField.setType(TypeField.Number);
        repeatIntervalField.setFieldClass(java.lang.Long.class);
        listAction=new ArrayList<>();
        listAction.add("SimpleTrigger");
        repeatIntervalField.setRefTriggers(listAction);
        repeatIntervalField.setOrder(3);
        this.getInfo().getConfigVariables().add(repeatIntervalField);

        /*Field uniqieIdField=new Field();//("uniqueId", Long.class, "uniqueId",true,new String[]{"CronTrigger","SimpleTrigger"},null,84);
        uniqieIdField.removeDefaultTriggersAndActions();
        uniqieIdField.setName("uniqueId");
        uniqieIdField.setLabel("uniqueId");
        uniqieIdField.setType(TypeField.Text);
        uniqieIdField.setFieldClass(java.lang.String.class);
        listAction=new ArrayList<>();
        listAction.add("SimpleTrigger");
        listAction.add("CronTrigger");
        uniqieIdField.setRefTriggers(listAction);
        uniqieIdField.setOrder(4);
        uniqieIdField.setReadonly(true);
        uniqieIdField.setVisibility(false);
        uniqieIdField.setValue(String.valueOf(ObjectId.get()));
        this.getInfo().getConfigVariables().add(uniqieIdField);*/
    }

    @Override
    public void consume(IPlugin plugin) {
        try {
            switch (plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().toLowerCase()) {
                case "crontrigger":
                    getCamelContext().addRoutes(new QuartzCronImapRoute((Quartz) plugin,this.getUniqueIdentity(plugin)));
                    break;
                case "simpletrigger":
                    getCamelContext().addRoutes(new QuartzSimpleImapRoute((Quartz) plugin,this.getUniqueIdentity(plugin)));
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void produce(IPlugin plugin) {
    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        switch (plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().toLowerCase()) {
            case "crontrigger":
                return (plugin.getInfo().getBusinessRuleId()!=null?plugin.getInfo().getBusinessRuleId().trim():"")+
                        "-"+
                        plugin.getInfo().getValue(PluginInfo.CONFIG, "cron").toString().trim();
            case "simpletrigger":
                return  (plugin.getInfo().getBusinessRuleId()!=null?plugin.getInfo().getBusinessRuleId().trim():"")+
                        "-"+
                        plugin.getInfo().getValue(PluginInfo.CONFIG, "repeatInterval").toString().trim()+
                        "-"+
                        plugin.getInfo().getValue(PluginInfo.CONFIG, "repeatCount").toString().trim();
                        /*+"-"+
                        plugin.getInfo().getValue(PluginInfo.CONFIG, "uniqueId").toString().trim();*/

        }
        return null;
    }

   /* @Override
    public IPlugin getInstance() throws InstantiationException, IllegalAccessException {
        IPlugin instance = super.getInstance();
        instance.getInfo().setValue(PluginInfo.CONFIG,"uniqueId",String.valueOf(ObjectId.get()));
        return instance;
    }*/

}