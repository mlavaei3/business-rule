package com.lava.br.plugins.quartz.processes;

import com.lava.br.core.processes.BaseProcessor;
import com.lava.br.plugins.quartz.impl.Quartz;
import org.apache.camel.Exchange;

public class QuartzProcessor extends BaseProcessor {
    public QuartzProcessor(Quartz quartz) {
        super(quartz);
    }

    @Override
    public void process(Exchange exchange) throws Exception {

        sendMessageToServer();
    }
}
