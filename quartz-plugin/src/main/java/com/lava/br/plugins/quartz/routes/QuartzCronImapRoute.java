package com.lava.br.plugins.quartz.routes;


import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.routes.BaseRouteBuilder;
import com.lava.br.plugins.quartz.impl.Quartz;
import com.lava.br.plugins.quartz.processes.QuartzProcessor;

public class QuartzCronImapRoute extends BaseRouteBuilder {

	public QuartzCronImapRoute(Quartz quartz,String routeName) {
		super(quartz,routeName);
	}
	@Override
	public void configure() throws Exception {
		from("quartz2://"+ getRouteName()
				.replace("/","slash")
				.replace("+","plus")
				.replace("*","multiply")
				.replace("-","dash")
				.replace(",","comma")
				.replace("#","sharp")
				.replace("?","question")
				+"/myTimerName?cron="
				+ getPlugin().getInfo().getValue(PluginInfo.CONFIG, "cron"))
				.routeId(getRouteName()) //getPlugin().getInfo().getValue("CONFIG", "username").toString())
				.autoStartup(false)
				.routePolicy(getPolicy())
				.process(new QuartzProcessor((Quartz) getPlugin()));
	}

}
