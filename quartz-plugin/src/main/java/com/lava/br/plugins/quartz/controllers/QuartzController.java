package com.lava.br.plugins.quartz.controllers;


import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.quartz.services.QuartzService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class QuartzController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(QuartzController.class);

    @Autowired
    private QuartzService quartzService;

    @Override
    public PluginService getPluginService(){
         return quartzService;
    }

}
