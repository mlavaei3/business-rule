package com.lava.br.core.repositories;

import com.lava.br.core.beans.IPluginRegisteredRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

public abstract class PluginRegisteredRepositoryService {
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IPluginRegisteredRepository iPluginRegisteredRepository;

    public IPluginRegisteredRepository getBasicRepository() {
        return iPluginRegisteredRepository;
    }
}
