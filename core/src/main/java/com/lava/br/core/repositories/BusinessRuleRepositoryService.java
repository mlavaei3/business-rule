package com.lava.br.core.repositories;

import com.lava.br.core.beans.IBusinessRuleRepository;
import com.lava.br.core.beans.IRuleProducer;
import com.lava.br.core.enums.BusinessRuleStatus;
import com.lava.br.core.enums.BusinessRuleTemplateType;
import com.lava.br.core.repositories.bean.BusinessRule;
import com.lava.br.core.repositories.bean.FieldAssigner;
import com.mongodb.WriteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.geo.Shape;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.repository.support.PageableExecutionUtils;

import java.util.ArrayList;
import java.util.List;

//@Service("businessRuleRepositoryService")
public abstract class BusinessRuleRepositoryService {

	/*@Autowired
	public BusinessRuleRepositoryService(IBusinessRuleRepository iBusinessRuleRepository){
		this.iBusinessRuleRepository=iBusinessRuleRepository;
	}*/
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	IBusinessRuleRepository iBusinessRuleRepository;

	public IBusinessRuleRepository getBasicRepository() {
		return iBusinessRuleRepository;
	}

	public List<BusinessRule> findByConsumerTypeAndConsumerConfigFieldName(String consumerType, String fieldName, String value){
		Query query = new Query(Criteria.where("consumerType").is(consumerType)
				.and("consumerConfig").elemMatch(Criteria.where("fieldName").is(fieldName)
						.and("value").is(value)));
		List<BusinessRule> result = mongoTemplate.find(query,BusinessRule.class);

		return result;
	}

	public List<BusinessRule> findByConsumerTypeAndFieldAssigners(String consumerType, List<FieldAssigner> fieldAssigners){
		Criteria criteria=Criteria.where("consumerType").is(consumerType);

		List<Criteria> list = new ArrayList<Criteria>();

		for(FieldAssigner fieldAssigner:fieldAssigners){
			list.add(Criteria.where("consumerConfig").elemMatch(
					Criteria.where("fieldName").is(fieldAssigner.getFieldName())
							.and("value").is(fieldAssigner.getValue())));
		}

		Criteria[] CriteriaAll=new Criteria[fieldAssigners.size()];
		CriteriaAll = (Criteria[]) list.toArray(CriteriaAll);
		//criteria.where("consumerConfig").all(CriteriaAll)
		Query query = new Query(criteria.andOperator(CriteriaAll));

		List<BusinessRule> result = mongoTemplate.find(query,BusinessRule.class);

		return result;
	}

	public List<BusinessRule> findByUsernameAndConsumerTypeAndFieldAssigners(String username,String consumerType, List<FieldAssigner> fieldAssigners){
		Criteria criteria=Criteria.where("consumerType").is(consumerType).and("username").is(username);

		List<Criteria> list = new ArrayList<Criteria>();

		for(FieldAssigner fieldAssigner:fieldAssigners){
			list.add(Criteria.where("consumerConfig").elemMatch(
					Criteria.where("fieldName").is(fieldAssigner.getFieldName())
							.and("value").is(fieldAssigner.getValue())));
		}

		Criteria[] CriteriaAll=new Criteria[fieldAssigners.size()];
		CriteriaAll = (Criteria[]) list.toArray(CriteriaAll);
		//criteria.where("consumerConfig").all(CriteriaAll)
		Query query = new Query(criteria.andOperator(CriteriaAll));

		List<BusinessRule> result = mongoTemplate.find(query,BusinessRule.class);

		return result;
	}

	public long updateBR(int brId, String conName) {
		Query query1 = new Query(Criteria.where("id").is(new ObjectId("5a78941a22cd1b35bcce139e")));

		Query query = new Query(Criteria.where("id").is(0).and("consumerFilters.property").is("subject"));

		Update update = new Update();
		update.set("consumerFilters.$.value", "rrrrrrrrrrrr");

		boolean ff=mongoTemplate.exists(query1, BusinessRule.class);


		UpdateResult result = mongoTemplate.updateFirst(query, update, BusinessRule.class);

		if (result != null)
			return result.getMatchedCount();
		else
			return 0;

	}

	public long insertProducerToBR(String brId, IRuleProducer ruleProducer) {
		Query query = new Query(Criteria.where("id").is(brId));
		Update update = new Update();
		update.addToSet("ruleProducers", ruleProducer);
		UpdateResult result = mongoTemplate.updateFirst(query, update, BusinessRule.class);

		if (result != null)
			return result.getMatchedCount();
		else
			return 0;

	}

	public Page<BusinessRule> getBusinessRuleAbbrClientByPagination(String userId,Integer page, Integer size, String orderBy){
		Sort sort = new Sort(Sort.Direction.ASC, orderBy);
		Pageable pageable = new PageRequest(page, size,sort);
		Query pluginsDynamicQuery = new Query()
				.addCriteria(Criteria.where("username").is(userId))
				.addCriteria(Criteria.where("status").ne(BusinessRuleStatus.TEMPLATE))
				.with(pageable);
		pluginsDynamicQuery
				.fields()
				.exclude("versionId")
				.exclude("artifactId")
				.exclude("consumerFilters")
				//.exclude("consumerConfig")
				.exclude("consumerFiltersExpression");
		pluginsDynamicQuery
				.fields()
				.exclude("ruleProducers.pluginOutputVariables")
				//.exclude("ruleProducers.pluginConfigVariables")
				.exclude("ruleProducers.producerFilters")
				.exclude("ruleProducers.producerFiltersExpression");

		// Add criteria's according to your wish to patientsDynamicQuery
		List<BusinessRule> filteredPluginsDefine =
				mongoTemplate.find(pluginsDynamicQuery, BusinessRule.class, "businessRules");
		Page<BusinessRule> BusinessRuleAbbrClients = PageableExecutionUtils.getPage(
				filteredPluginsDefine,
				pageable,
				() -> mongoTemplate.count(pluginsDynamicQuery, BusinessRule.class));
		return BusinessRuleAbbrClients;
	}

	public Page<BusinessRule> getTemplateBusinessRuleAbbrClientByPagination(String type,Integer page, Integer size, String orderBy){
		Sort sort = new Sort(Sort.Direction.ASC, orderBy);
		Pageable pageable = new PageRequest(page, size,sort);
		Query pluginsDynamicQuery = new Query()
				.addCriteria(Criteria.where("status").is(BusinessRuleStatus.TEMPLATE))
				.with(pageable);

		if(type.equalsIgnoreCase(BusinessRuleTemplateType.ALL.getValue())==false){
			pluginsDynamicQuery
					.addCriteria(Criteria.where("artifactId").is(type));
		}
		pluginsDynamicQuery
				.fields()
				.exclude("versionId")
				.exclude("artifactId")
				.exclude("consumerFilters")
				//.exclude("consumerConfig")
				.exclude("consumerFiltersExpression");
		pluginsDynamicQuery
				.fields()
				.exclude("ruleProducers.pluginOutputVariables")
				//.exclude("ruleProducers.pluginConfigVariables")
				.exclude("ruleProducers.producerFilters")
				.exclude("ruleProducers.producerFiltersExpression");

		// Add criteria's according to your wish to patientsDynamicQuery
		List<BusinessRule> filteredPluginsDefine =
				mongoTemplate.find(pluginsDynamicQuery, BusinessRule.class, "businessRules");
		Page<BusinessRule> BusinessRuleAbbrClients = PageableExecutionUtils.getPage(
				filteredPluginsDefine,
				pageable,
				() -> mongoTemplate.count(pluginsDynamicQuery, BusinessRule.class));
		return BusinessRuleAbbrClients;
	}
}
