package com.lava.br.core.plugins.utils.dbstore;

import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.dbstore.impl.DbStore;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CRUDKeyValue {
    public static void addKeyWithValue(DbStore dbStore,String pluginName,String userId, DbStoreType dbStoreType, String objectKeyName, String objectKeyValue, String objectValueName, String objectValue){
        try {
            DbStore dbStoreFind=(DbStore)dbStore.getInstance();
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"name",pluginName);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreFind);

            String res=(String)dbStoreFind.getInfo().getValue(PluginInfo.OUT,"result");
            JSONArray objArr=new JSONArray();
            boolean flag=false;
            if(res!=null && !res.isEmpty()){
                objArr = new JSONArray(res);
                for (int i=0;i<objArr.length();i++){
                    JSONObject objData = objArr.getJSONObject(i);
                    String keyValue= objData.getString(objectKeyName);
                    if(keyValue!=null && keyValue.equalsIgnoreCase(objectKeyValue)){
                        Map<String,String> mp=new HashMap<>();
                        mp.put(objectKeyName,objectKeyValue);
                        mp.put(objectValueName,objectValue);
                        objArr.put(i,mp);
                        flag=true;
                    }
                }
            }
            if(flag==false){
                Map<String,String> mp=new HashMap<>();
                mp.put(objectKeyName,objectKeyValue);
                mp.put(objectValueName,objectValue);
                objArr.put(mp);
            }

            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object",objArr!=null?objArr.toString():String.format("[{\"%s\":\"%s\",\"%s\":\"%s\"}]",objectKeyName,objectKeyValue,objectValueName,objectValue));
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",pluginName);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","save");
            dbStore.produce(dbStoreClient);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static String getValueForKey(DbStore dbStore,String pluginName,String userId,DbStoreType dbStoreType,String objectKeyName,String objectKeyValue,String objectValueName){
        try {
            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",pluginName);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreClient);
            String res=(String)dbStoreClient.getInfo().getValue(PluginInfo.OUT,"result");
            final JSONArray objArr = new JSONArray(res);
            for (int i=0;i<objArr.length();i++){
                JSONObject objData = objArr.getJSONObject(i);
                String keyValue= objData.getString(objectKeyName);
                if(keyValue!=null && keyValue.equalsIgnoreCase(objectKeyValue)){
                    return objData.getString(objectValueName);
                }
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static Map<String,String> getAllValueForKey(DbStore dbStore,String pluginName,String userId,DbStoreType dbStoreType,String objectKeyName,String objectValueName){
        Map<String,String> result=new HashMap<>();
        try {

            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",pluginName);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreClient);
            String res=(String)dbStoreClient.getInfo().getValue(PluginInfo.OUT,"result");
            if(res!=null) {
                final JSONArray objArr = new JSONArray(res);
                for (int i = 0; i < objArr.length(); i++) {
                    JSONObject objData = objArr.getJSONObject(i);
                    String keyValue = objData.getString(objectKeyName);
                    if (keyValue != null) {
                        result.put(keyValue, objData.getString(objectValueName));
                    }
                }
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void removeKeyWithValue(DbStore dbStore,String pluginName,String userId,DbStoreType dbStoreType,String objectKeyName,String objectKeyValue){
        try {
            DbStore dbStoreFind=(DbStore)dbStore.getInstance();
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"name",pluginName);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreFind);

            String res=(String)dbStoreFind.getInfo().getValue(PluginInfo.OUT,"result");
            JSONArray objArr=new JSONArray();
            boolean flag=false;
            if(res!=null && !res.isEmpty()){
                objArr = new JSONArray(res);
                for (int i=0;i<objArr.length();i++){
                    JSONObject objData = objArr.getJSONObject(i);
                    String keyValue= objData.getString(objectKeyName);
                    if(keyValue!=null && keyValue.equalsIgnoreCase(objectKeyValue)){
                        objArr.remove(i);
                        flag=true;
                        break;
                    }
                }
            }
            if(flag){
                DbStore dbStoreClient=(DbStore)dbStore.getInstance();
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object",objArr.toString());
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",pluginName);
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","save");
                dbStore.produce(dbStoreClient);
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
