package com.lava.br.core.beans;

public interface IMessageSocket {
    String getSessionId();
    void setSessionId(String sessionId);

    String getObjectId();
    void setObjectId(String objectId);

    String getMessage();
    void setMessage(String message);

}
