package com.lava.br.core.beans;

public interface IPartyField extends ILazySimpleSelectField {
    String getRequestUrl();
    void setRequestUrl(String requestUrl);
}
