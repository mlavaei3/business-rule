package com.lava.br.core.repositories.bean;


import com.lava.br.core.beans.IFieldFilter;
import com.lava.br.core.enums.Operator;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.TypeAlias;

@TypeAlias("filter")
@Getter
@Setter
public class FieldFilter implements IFieldFilter {

	private String fieldName;
	/**
	 * Value to be evaluated.
	 */
	private Object value;
	/**
	 * Operator used to compare the data.
	 */
	private Operator operator;


}
