package com.lava.br.core.controllers;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.exceptions.BaseException;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.repositories.bean.LazySelectField;
import com.lava.br.core.repositories.bean.PartyField;
import com.lava.br.core.repositories.bean.PluginRegistered;
import com.lava.br.core.services.PluginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value="v1")
public abstract class PluginController {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private HttpServletRequest request;

    protected abstract PluginService getPluginService();

    @RequestMapping(value="test/{brId}",method = RequestMethod.GET)
    public String test(@PathVariable("brId") String brId) {
        Principal principal=this.request.getUserPrincipal();
        return "ok";
    }

    @RequestMapping(value="/registerPlugin",method = RequestMethod.POST)
    public PluginRegistered registerPlugin(@RequestBody PluginInfoClient pluginInfoClient) {
        try {
            Principal principal=this.request.getUserPrincipal();
            IPlugin newPlugin = getPluginService().getNewInstancePlugin();
            PluginMapper.pluginInfoCopyTo(pluginInfoClient,newPlugin);
            getPluginService().registerPlugin(newPlugin);

            PluginRegistered pluginRegistered=PluginRegistered.builder()
                    .instanceId(getPluginService().getServiceInstanceId())
                    .consumerType(newPlugin.getInfo().getName())
                    .businessRuleId(pluginInfoClient.getBusinessRuleId())
                    .pluginUniqueIdentity(getPluginService().getPlugin().getUniqueIdentity(newPlugin))
                    .build();

            return pluginRegistered;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="/unRegisterPlugin",method = RequestMethod.POST)
    public Boolean unRegisterPlugin(@RequestBody PluginInfoClient pluginInfoClient) {
        try {
            IPlugin newPlugin = getPluginService().getNewInstancePlugin();
            PluginMapper.pluginInfoCopyTo(pluginInfoClient,newPlugin);
            getPluginService().unRegisterPlugin(newPlugin);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping(value="/getTemplateRegisterPlugin",method = RequestMethod.POST)
    public PluginRegistered getTemplateRegisterPlugin(@RequestBody PluginInfoClient pluginInfoClient) {
        try {
            IPlugin newPlugin = getPluginService().getNewInstancePlugin();
            PluginMapper.pluginInfoCopyTo(pluginInfoClient,newPlugin);

            PluginRegistered pluginRegistered=PluginRegistered.builder()
                    .consumerType(newPlugin.getInfo().getName())
                    .pluginUniqueIdentity(getPluginService().getPlugin().getUniqueIdentity(newPlugin))
                    .build();

            return pluginRegistered;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }


    @RequestMapping(value="/producePlugin",method = RequestMethod.POST
            ,produces = "application/json")
    public Boolean producePlugin(@RequestBody PluginInfoClient pluginInfoClient) {
        try {
            IPlugin newPlugin =getPluginService().newInstance(pluginInfoClient);
            getPluginService().producePlugin(newPlugin).toString();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping(value="/getPluginInfoClient",method = RequestMethod.GET
            ,produces = "application/json")
    public PluginInfoClient getPluginInfoClient() throws BaseException {
        try {
            // return PluginMapper.toPluginInfoClient(getPluginService().getPlugin());
            PluginInfoClient pluginInfoClient=getPluginService().getPluginInfoClient();
            return pluginInfoClient;
        } catch (Exception e) {
            throw new BaseException();
        }
    }

    @RequestMapping(value="/getLazySelectList/{fieldName}",method = RequestMethod.GET
            ,produces = "application/json")
    public Map<String, String> getLazySelectList(@PathVariable("fieldName") String fieldName,
                                                 @RequestParam(defaultValue = "",value ="query",required = false) String query,
                                                 Principal principal) throws BaseException {
        try {
            LazySelectField fieldQuery= (LazySelectField) getPluginService().getPlugin().getInfo().getConfigVariables().stream()
                    .filter(field -> field.getName().equalsIgnoreCase(fieldName)).findFirst().get();
            Map<String, Object> objectMap=new HashMap<>();
            objectMap.put("userId",principal.getName());
            objectMap.put("query",query);
            return fieldQuery.getSelectFields().apply(objectMap);
        } catch (Exception e) {
            return null;
        }        }


    @RequestMapping(value="/getPartySelectList/{fieldName}",method = RequestMethod.GET
            ,produces = "application/json")
    public Map<String, String> getPartySelectList(@PathVariable("fieldName") String fieldName,
                                                  @RequestParam(defaultValue = "",value ="query",required = false) String query,
                                                  Principal principal) throws BaseException {
        try {
            PartyField fieldQuery= (PartyField) getPluginService().getPlugin().getInfo().getConfigVariables().stream()
                    .filter(field -> field.getName().equalsIgnoreCase(fieldName)).findFirst().get();
            Map<String, Object> objectMap=new HashMap<>();
            objectMap.put("userId",principal.getName());
            objectMap.put("query",query);
            return fieldQuery.getSelectFields().apply(objectMap);
        } catch (Exception e) {
            return null;
        }
    }


}
