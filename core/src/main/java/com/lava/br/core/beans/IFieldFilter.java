package com.lava.br.core.beans;


import com.lava.br.core.enums.Operator;

public interface IFieldFilter extends IFieldAssigner {

	Operator getOperator();

	void setOperator(Operator operator);

}
