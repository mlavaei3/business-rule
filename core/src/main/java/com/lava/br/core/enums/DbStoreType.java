package com.lava.br.core.enums;

public enum DbStoreType {
    PERSONAL, ACCOUNTING, PLUGIN
}
