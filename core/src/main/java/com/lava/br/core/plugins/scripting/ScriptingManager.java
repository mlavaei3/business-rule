package com.lava.br.core.plugins.scripting;

import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.beans.IScriptService;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.dbstore.impl.DbStore;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aria on 05/06/2018.
 */
public class ScriptingManager {

   // RequestForProducer.StreamProduceGateway streamGateway=null;
    IScriptService service;
    ApplicationContext applicationContext = null;
    ScriptingDbManager scriptingDbManager=new ScriptingDbManager();
    String userid=null;
    public IPlugin consumerPlugin=null;

    public ScriptingManager(String userid, IScriptService service, IPlugin consumerPlugin) {
        applicationContext = ApplicationContextProvider.getApplicationContext();
        //streamGateway=  applicationContext.getBean(RequestForProducer.StreamProduceGateway.class);
        this.service=service;
        this.userid=userid;
        this.consumerPlugin=consumerPlugin;
    }

    public IPlugin getPlugin(String pluginName) {
        BasePlugin plugin = new BasePlugin();
        plugin.getInfo().setName(pluginName);
        plugin.getInfo().setValue(PluginInfo.OUT,"userid",userid);
        return plugin;
    }

    public void exec(IPlugin iPlugin) {
         service.exec(iPlugin);
        //Message<PluginInfoClient> ff=streamGateway.process(MessageBuilder.withPayload(pluginClient.getPluginInfoClient())
        //        .setHeader("type",iPlugin.getInfo().getName()).build());

    }

    public ScriptingVariables getConfigs(IPlugin iPlugin) {
        return new ScriptingVariables(iPlugin,PluginInfo.CONFIG);
    }

    public ScriptingVariables getVariables(IPlugin iPlugin) {
        return new ScriptingVariables(iPlugin,PluginInfo.OUT);
    }

    public ScriptingVariables getConsumer_Vars() {
        return new ScriptingVariables(this.consumerPlugin,PluginInfo.IN);
    }

    public void saveDbObject(String name,Object object) throws IllegalAccessException, InstantiationException {
        DbStore dbStore= (DbStore) applicationContext.getBean("dbstore");
        DbStore dbStoreChild= (DbStore) dbStore.getInstance();
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"userid",userid);
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"object",object);
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"dbStoreType",DbStoreType.PERSONAL);

        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"name",name);
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"action","save");
        dbStore.produce(dbStoreChild);
    }

    public String loadDbObject(String name) throws IllegalAccessException, InstantiationException {
        DbStore dbStore= (DbStore) applicationContext.getBean("dbstore");
        DbStore dbStoreChild= (DbStore) dbStore.getInstance();
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"userid",userid);
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"dbStoreType",DbStoreType.PERSONAL);
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"action","load");
        dbStoreChild.getInfo().setValue(PluginInfo.OUT,"name",name);

        dbStore.produce(dbStoreChild);

        return dbStoreChild.getInfo().getValue(PluginInfo.OUT,"result").toString();

    }
/*

    public FiltersDsl getFilterDsl() {
        return new FiltersDsl();
    }

    public IBusinessRule createBR(String name, PluginDsl pluginConsumer, FiltersDsl filterDsl) {
        IBusinessRule br = new BusinessRule();
        br.setId(new ObjectId().toString());
        br.setArtifactId(name);

        br.setConsumerType(pluginConsumer.getPluginTypeName());

        br.setConsumerFilters(filterDsl.getFilters());
        br.setConsumerFiltersExpression(filterDsl.getExpression());

        br.setConsumerConfig(pluginConsumer.getPluginConfig().asListAssigner());

        br.setRuleProducers(new ArrayList<IRuleProducer>());

        businessRuleService.getBasicRepository().insert((BusinessRule)br);

        return br;

    }

    public void addProducerToBR(IBusinessRule br, PluginDsl pluginProducer, Integer priority) {
        IRuleProducer irp = new RuleProducer();
        irp.setId(new ObjectId().toString());
        irp.setPluginType(pluginProducer.getPluginTypeName());
        irp.setPriority(priority);
        irp.setPluginOutputVariables(pluginProducer.getPluginOutputVariableDsl().asListAssigner());
        irp.setPluginConfigs(pluginProducer.getPluginConfig().asListAssigner());
        br.getRuleProducers().add(irp);
        businessRuleService.insertProducerToBR(br.getId(), irp);

    }
*/


}
