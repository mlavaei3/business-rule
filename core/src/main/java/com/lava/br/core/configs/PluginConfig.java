package com.lava.br.core.configs;

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.enums.CommunicationType;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

/**
 * Created by Aria on 03/06/2018.
 */
@Component
public class PluginConfig {
    public String getConditionForAction(String action) {
        if (getNameOfPlugin()!=null) {
            return "headers['type']=='"+getNameOfPlugin()+"' && headers['action']=='"+action+"'" ;
        }
        return  "";
    }

    public String getNameOfPlugin() {
        try {
            Pluggable pluginName = null;
            Reflections reflections = new Reflections("com.lava.br.plugins");
            Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Pluggable.class);

            Optional<Class<?>> firstElement = annotated.stream().findFirst();

            if (annotated.size() == 1) {
                pluginName = firstElement.get().getAnnotation(Pluggable.class);
                return pluginName.nameOfPlugin();
            }
            return  null;
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return  null;
        }
    }

    @Value("${lava.communicationType}")
    private String communicationType;

    public CommunicationType getCommunicationType() {
        return Enum.valueOf(CommunicationType.class,communicationType);
    }



}
