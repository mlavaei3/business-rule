package com.lava.br.core.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.dbstore.impl.DbStore;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.netflix.discovery.EurekaClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;

import java.util.HashMap;
import java.util.Map;

public abstract class PluginService {
    @Qualifier("eurekaClient")
    @Autowired
    EurekaClient eurekaClient;

    @Autowired
    DbStore dbStore;

    public abstract IPlugin getPlugin();

    public abstract PluginRestTemplateClient getPluginRestTemplateClient();

    public Boolean sendToSocket(String sessionId,String objectId,String message){
        System.out.println("send Message To Socket");
        return getPluginRestTemplateClient().sendToSocket(sessionId,objectId,message);
    }

    public Boolean registerPlugin(IPlugin plugin){
        Thread thread = new Thread(() -> {
            System.out.println("Thread registerPlugin Running");
            getPlugin().register(plugin);
        });
        thread.start();
        thread=null;
        return true;
    }

    public Boolean producePlugin(IPlugin plugin){
        Thread thread = new Thread(() -> {
            System.out.println("Thread producePlugin Running");
            getPlugin().produce(plugin);
        });
        thread.start();
        thread=null;
        return true;
    }

    public Boolean unRegisterPlugin(IPlugin plugin){
        Thread thread = new Thread(() -> {
            System.out.println("Thread unRegisterPlugin Running");
            getPlugin().unConsume(plugin);
        });
        thread.start();
        thread=null;
        return true;
    }

    public void sendMessageToServer(IPlugin plugin){
        Thread thread = new Thread(() -> {
            System.out.println("Thread sendMessageToServer Running");
            getPluginRestTemplateClient().fireRules(plugin);
        });
        thread.start();
    }

    public IPlugin getNewInstancePlugin() throws IllegalAccessException, InstantiationException{
        return getPlugin().getInstance();
    }

    public IPlugin newInstance(PluginInfoClient pluginInfoClient){
        try {
            IPlugin newPlugin = getNewInstancePlugin();
            PluginMapper.pluginInfoCopyTo(pluginInfoClient,newPlugin);
            return newPlugin;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Cacheable(value = "serviceInstanceId")
    public String getServiceInstanceId(){
        return eurekaClient.getApplicationInfoManager().getInfo().getInstanceId();
    }

    public String getServiceIp(){
        return eurekaClient.getApplicationInfoManager().getInfo().getIPAddr();
    }

    public int getServicePort(){
        return eurekaClient.getApplicationInfoManager().getInfo().getPort();
    }

    //@Cacheable(value = "getPluginInfoClient",key = "#root.targetClass.toGenericString()+#root.methodName") //key = "#result.name", unless="#result==null"
    public PluginInfoClient getPluginInfoClient(){
        return PluginMapper.toPluginInfoClient(getPlugin()) ;
    }

    public void addKeyWithValue(String userId,DbStoreType dbStoreType,String objectKeyName,String objectKeyValue,String objectValueName,String objectValue){
        try {
            DbStore dbStoreFind=(DbStore)dbStore.getInstance();
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"name",this.getPlugin().getInfo().getName());
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreFind);

            String res=(String)dbStoreFind.getInfo().getValue(PluginInfo.OUT,"result");
            JSONArray objArr=new JSONArray();
            boolean flag=false;
            if(res!=null && !res.isEmpty()){
                objArr = new JSONArray(res);
                for (int i=0;i<objArr.length();i++){
                    JSONObject objData = objArr.getJSONObject(i);
                    String keyValue= objData.getString(objectKeyName);
                    if(keyValue!=null && keyValue.equalsIgnoreCase(objectKeyValue)){
                        Map<String,String> mp=new HashMap<>();
                        mp.put(objectKeyName,objectKeyValue);
                        mp.put(objectValueName,objectValue);
                        objArr.put(i,mp);
                        flag=true;
                    }
                }
            }
            if(flag==false){
                Map<String,String> mp=new HashMap<>();
                mp.put(objectKeyName,objectKeyValue);
                mp.put(objectValueName,objectValue);
                objArr.put(mp);
            }

            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object",objArr!=null?objArr.toString():String.format("[{\"%s\":\"%s\",\"%s\":\"%s\"}]",objectKeyName,objectKeyValue,objectValueName,objectValue));
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",this.getPlugin().getInfo().getName());
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","save");
            dbStore.produce(dbStoreClient);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String getValueForKey(String userId,DbStoreType dbStoreType,String objectKeyName,String objectKeyValue,String objectValueName){
        try {
            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",this.getPlugin().getInfo().getName());
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreClient);
            String res=(String)dbStoreClient.getInfo().getValue(PluginInfo.OUT,"result");
            final JSONArray objArr = new JSONArray(res);
            for (int i=0;i<objArr.length();i++){
                JSONObject objData = objArr.getJSONObject(i);
                String keyValue= objData.getString(objectKeyName);
                if(keyValue!=null && keyValue.equalsIgnoreCase(objectKeyValue)){
                    return objData.getString(objectValueName);
                }
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return "";
    }

    public Map<String,String> getAllValueForKey(String userId,DbStoreType dbStoreType,String objectKeyName,String objectValueName){
        Map<String,String> result=new HashMap<>();
        try {

            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",this.getPlugin().getInfo().getName());
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreClient);
            String res=(String)dbStoreClient.getInfo().getValue(PluginInfo.OUT,"result");
            final JSONArray objArr = new JSONArray(res);
            for (int i=0;i<objArr.length();i++){
                JSONObject objData = objArr.getJSONObject(i);
                String keyValue= objData.getString(objectKeyName);
                if(keyValue!=null){
                    result.put(keyValue,objData.getString(objectValueName));
                }
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void removeKeyWithValue(String userId,DbStoreType dbStoreType,String objectKeyName,String objectKeyValue){
        try {
            DbStore dbStoreFind=(DbStore)dbStore.getInstance();
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"userid",userId);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"name",this.getPlugin().getInfo().getName());
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
            dbStoreFind.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreFind);

            String res=(String)dbStoreFind.getInfo().getValue(PluginInfo.OUT,"result");
            JSONArray objArr=new JSONArray();
            boolean flag=false;
            if(res!=null && !res.isEmpty()){
                objArr = new JSONArray(res);
                for (int i=0;i<objArr.length();i++){
                    JSONObject objData = objArr.getJSONObject(i);
                    String keyValue= objData.getString(objectKeyName);
                    if(keyValue!=null && keyValue.equalsIgnoreCase(objectKeyValue)){
                        objArr.remove(i);
                        flag=true;
                        break;
                    }
                }
            }
            if(flag){
                DbStore dbStoreClient=(DbStore)dbStore.getInstance();
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid",userId);
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object",objArr.toString());
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name",this.getPlugin().getInfo().getName());
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",dbStoreType);
                dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","save");
                dbStore.produce(dbStoreClient);
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
