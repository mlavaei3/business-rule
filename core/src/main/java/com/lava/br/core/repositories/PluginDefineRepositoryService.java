package com.lava.br.core.repositories;

import com.lava.br.core.beans.IPluginDefineRepository;
import com.lava.br.core.repositories.bean.PluginDefine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;

import java.util.List;

public abstract class PluginDefineRepositoryService {
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IPluginDefineRepository iPluginDefineRepository;

    public IPluginDefineRepository getBasicRepository() {

        return iPluginDefineRepository;
    }

    public Page<PluginDefine> getPluginsDefineByPagination(Integer page,Integer size,String orderBy){

        Sort sort = new Sort(Sort.Direction.ASC, orderBy);
        Pageable pageable = new PageRequest(page, size,sort);
        Query pluginsDynamicQuery = new Query().with(pageable);
        // Add criteria's according to your wish to patientsDynamicQuery
        List<PluginDefine> filteredPluginsDefine =
                mongoTemplate.find(pluginsDynamicQuery, PluginDefine.class, "pluginsDefine");
        Page<PluginDefine> pluginDefinePage = PageableExecutionUtils.getPage(
                filteredPluginsDefine,
                pageable,
                () -> mongoTemplate.count(pluginsDynamicQuery, PluginDefine.class));
        return pluginDefinePage;
    }
}

