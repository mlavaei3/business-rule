package com.lava.br.core.plugins.utils.niligo;

import com.lava.br.core.enums.NiligoTriggerType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationDto {

    private String username;
    private String uuid;
    private NiligoTriggerType trigger;
    private String switchVibrationCount;
}
