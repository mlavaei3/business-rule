package com.lava.br.core.exceptions;

/**
 * Created by Aria on 02/05/2018.
 */
public class BaseException extends Exception{
    /**
     *
     */
    private static final long serialVersionUID = -4845026886028224402L;

    public BaseException() {
        super();
    }

    public BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }
}

