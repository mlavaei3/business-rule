package com.lava.br.core.enums;

public enum ValidatorType {
   Compare,Required,Regex,Notempty,Excludes,Includes,Min, Max,Minlength, Maxlength
   ,DeptReset,DeptVisible,DeptValid,DefaultValue,GroupArray
}
