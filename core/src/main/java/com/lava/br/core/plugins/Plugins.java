package com.lava.br.core.plugins;

/**
 * Created by Aria on 02/05/2018.
 */

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
//@ConfigurationProperties(prefix = "plugins")
public class Plugins {
    private List<PluginInfo> pluginList = new ArrayList<PluginInfo>();
    public Plugins(){
        //empty ctor
    }
    public List<PluginInfo> getPluginList(){
        return pluginList;
    }

    public void setPluginList(List<PluginInfo> plugins){
        this.pluginList = plugins;
    }
}

