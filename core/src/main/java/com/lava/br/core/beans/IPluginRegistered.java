package com.lava.br.core.beans;

public interface IPluginRegistered {
    String getId();

    void setId(String id);

    String getBusinessRuleId();

    void setBusinessRuleId(String businessRuleId);

    String getInstanceId();

    void setInstanceId(String instanceId);

    String getConsumerType();

    void setConsumerType(String consumerType);

    String getPluginUniqueIdentity();

    void setPluginUniqueIdentity(String pluginUniqueIdentity);


}
