package com.lava.br.core.repositories.bean;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.TypeAlias;

@TypeAlias("mapper")
@Getter
@Setter
public class FieldAssigner {

	private String fieldName;
	private Object value;

	public  FieldAssigner(String fieldName,Object value){
		this.fieldName=fieldName;
		this.value=value;
	}

	public  FieldAssigner(){
	}
}
