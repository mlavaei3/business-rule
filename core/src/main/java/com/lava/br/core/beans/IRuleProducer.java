package com.lava.br.core.beans;

import com.lava.br.core.repositories.bean.FieldAssigner;
import com.lava.br.core.repositories.bean.FieldFilter;

import java.util.List;

public interface IRuleProducer {
	String getId();

	void setId(String id);
	
	String getPluginType();

	void setPluginType(String pluginType);
	
	int getPriority();

	void setPriority(int priority);
	
	List<FieldAssigner> getPluginOutputVariables();
	
	void setPluginOutputVariables(List<FieldAssigner> pluginOutputVariables);

	List<FieldAssigner> getPluginConfigVariables();

	void setPluginConfigVariables(List<FieldAssigner> pluginConfigVariables);

	List<FieldFilter> getProducerFilters();

	void setProducerFilters(List<FieldFilter> producerFilters);

	String getProducerFiltersExpression();

	void setProducerFiltersExpression(String producerFiltersExpression);
	
}
