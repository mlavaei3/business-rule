package com.lava.br.core.beans;

public interface IPluginDefine {
    String getId();

    void setId(String id);

    String getName();

    void setName(String name);

    String getLabel();

    void setLabel(String label);

    Boolean getIsProducer();

    void setIsProducer(Boolean isProducer);

    Boolean getIsConsumer();

    void setIsConsumer(Boolean isConsumer);

    Boolean getIsEnabled();

    void setIsEnabled(Boolean enabled);

    Boolean getHasAccounting();

    void setHasAccounting(Boolean hasAccounting);

}
