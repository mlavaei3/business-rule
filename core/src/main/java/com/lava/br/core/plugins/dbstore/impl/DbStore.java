package com.lava.br.core.plugins.dbstore.impl;


import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.DbStoreObjectRepositoryService;
import com.lava.br.core.repositories.bean.DbStoreObject;
import com.lava.br.core.repositories.bean.Field;
import com.lava.br.core.repositories.bean.SimpleSelectField;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service("dbstore")
@Pluggable(nameOfPlugin = "dbstore")
public class DbStore extends BasePlugin {
    @PostConstruct
    public void init() {
        // outVar
        Field field=new Field();
        field.setName("result");
        field.setLabel("result");
        field.setFieldClass(java.lang.Object.class);
        field.setOrder(10);
        this.getInfo().getOutVariables().add(field);// Field.builder().name("result").fieldClass(java.lang.Object.class).label("result").order(5).build());

        field=new Field();
        field.setName("object");
        field.setLabel("object");
        field.setFieldClass(java.lang.Object.class);
        field.setOrder(1);
        this.getInfo().getOutVariables().add(field);// Field.builder().name("object").fieldClass(java.lang.Object.class).label( "object").order(1).build());

        field=new Field();
        field.setName("name");
        field.setLabel("name");
        field.setFieldClass(java.lang.String.class);
        field.setOrder(2);
        this.getInfo().getOutVariables().add(field);// Field.builder().name("name").fieldClass(java.lang.String.class).label("name").order(2).build());

        field=new Field();
        field.setName("dbStoreType");
        field.setLabel("dbStoreType");
        field.setFieldClass(DbStoreType.class);
        field.setOrder(3);
        this.getInfo().getOutVariables().add(field);// Field.builder().name("dbStoreType").fieldClass(DbStoreType.class).label("dbStoreType").order(3).build());

        SimpleSelectField simpleSelectField= SimpleSelectField.builder().name("action")
                .fieldClass(java.lang.String.class).label("action").order(4).build();
        Map<Object, String> stringObjectMap=new LinkedHashMap<>();
        stringObjectMap.put("load from db","load");
        stringObjectMap.put("save to db","save");
        simpleSelectField.setSelectFields(stringObjectMap);
        this.getInfo().getOutVariables().add(simpleSelectField);

        this.setPluginsRegisterd(new ArrayList<IPlugin>());
    }

    @Override
    public void produce(IPlugin plugin) {
        ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
        DbStoreObjectRepositoryService dbStoreObjectService= (DbStoreObjectRepositoryService) applicationContext.getBean("dbStoreObjectService");

        //Object oo=dbStoreObjectService.getBasicRepository().findByUsernameAndName("mehdi","hhhh").get(0).getObject();

        if(plugin.getInfo().getValue(PluginInfo.OUT,"action").equals("save")){
            DBObject dbObject = (DBObject)JSON.parse(plugin.getInfo().getValue(PluginInfo.OUT,"object").toString());
            DbStoreObject dbStoreObject=new DbStoreObject();
            dbStoreObject.setDbStoreType((DbStoreType)plugin.getInfo().getValue(PluginInfo.OUT,"dbStoreType"));
            dbStoreObject.setName(plugin.getInfo().getValue(PluginInfo.OUT,"name").toString());
            dbStoreObject.setUsername(plugin.getInfo().getValue(PluginInfo.OUT,"userid").toString());
            List<DbStoreObject> dbStoreObjectFinds= dbStoreObjectService.getBasicRepository().findByUsernameAndNameAndDbStoreType(
                    plugin.getInfo().getValue(PluginInfo.OUT,"userid").toString(),
                    plugin.getInfo().getValue(PluginInfo.OUT,"name").toString(),
                    (DbStoreType)plugin.getInfo().getValue(PluginInfo.OUT,"dbStoreType")
            );
            if(dbStoreObjectFinds.size()!=0){
                DbStoreObject dbStoreObjectFind=dbStoreObjectFinds.get(0);
                dbStoreObjectFind.setObject(dbObject);
                dbStoreObjectService.getBasicRepository().save(dbStoreObjectFind);
            }else{
                dbStoreObject.setObject(dbObject);
                dbStoreObjectService.getBasicRepository().insert(dbStoreObject);
            }
        }
        if(plugin.getInfo().getValue(PluginInfo.OUT,"action").equals("load")){
            List<DbStoreObject> objects=dbStoreObjectService.getBasicRepository()
                    .findByUsernameAndNameAndDbStoreType(
                            plugin.getInfo().getValue(PluginInfo.OUT,"userid").toString()
                            ,plugin.getInfo().getValue(PluginInfo.OUT,"name").toString()
                            ,DbStoreType.valueOf(plugin.getInfo().getValue(PluginInfo.OUT,"dbStoreType").toString()));


            if(objects.size()>0){
                plugin.getInfo().setValue(PluginInfo.OUT,"result",
                        JSON.serialize(objects.get(0).getObject()));
            }

        }

    }
}
