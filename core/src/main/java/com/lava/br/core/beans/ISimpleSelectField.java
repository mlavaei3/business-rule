package com.lava.br.core.beans;

import java.util.Map;

public interface ISimpleSelectField extends IField {

    Map<Object, String> getSelectFields();

    void setSelectFields(Map<Object, String> selectFields);
}
