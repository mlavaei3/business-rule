package com.lava.br.core.beans;

import com.lava.br.core.repositories.bean.PluginDefine;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
//import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface IPluginDefineRepository extends MongoRepository<PluginDefine, String> {
    @Query("{ 'name' : ?0 }")
    PluginDefine findFirstByName(String name);

    @Query("{ 'hasAccounting' : ?0 }")
    List<PluginDefine> findByHasAccounting(Boolean hasAccounting);

}
