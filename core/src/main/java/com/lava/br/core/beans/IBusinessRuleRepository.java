package com.lava.br.core.beans;

import com.lava.br.core.repositories.bean.BusinessRule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
public interface IBusinessRuleRepository extends MongoRepository<BusinessRule, String> {
	@Query("{ 'artifactId' : ?0 }")
	List<BusinessRule> findByArtifactId(String artifactId);

	@Query("{ 'username' : ?0 }")
	List<BusinessRule> findByUsername(String username);

/*	@Query("{ 'id' : ?0 }")
	BusinessRule findById(String id);*/

}
