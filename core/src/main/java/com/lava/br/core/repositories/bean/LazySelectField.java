package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.ILazySimpleSelectField;
import com.lava.br.core.beans.ISimpleSelectField;
import com.lava.br.core.enums.TypeField;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Getter
@Setter
//@SuperBuilder
public class LazySelectField extends Field implements ILazySimpleSelectField {
    Function<Map<String, Object>, Map<String, String>> selectFields=null;
    String requestUrl="";

    public LazySelectField(){
        super();
        this.setType(TypeField.SimpleSelectList);
    }

    @Builder
    private LazySelectField(int order,
                       Boolean visibility,
                       String label,
                       String name,
                       Class<?> fieldClass,
                       Object value,
                       TypeField type,
                       String helpText,
                       Boolean readonly ,
                       Boolean unique,
                       List<String> refTriggers,
                       List<String> refActions,
                       List<IFieldValidator> fieldValidators,
                       String requestUrl,
                       Function<Map<String, Object>, Map<String, String>> selectFields){

        super(order,visibility,label,name,fieldClass,
                value,type,helpText,readonly,unique,
                refTriggers,refActions,fieldValidators);
        this.requestUrl = requestUrl;
        this.selectFields=selectFields;
        this.setType(TypeField.LazySelectList);



        setFieldValidators(new ArrayList<IFieldValidator>());
        if(this.refTriggers == null || this.refTriggers.size()==0){
            setRefTriggers(new ArrayList<>());
            this.refTriggers.add("default");
        }
        if(this.refActions == null || this.refActions.size()==0){
            setRefActions(new ArrayList<>());
            this.refActions.add("default");
        }

    }
    /*public LazySelectField(String name, Class<?> fieldClass, String label, int order) {
        super(name,fieldClass,label,TypeField.SimpleSelectList,order);

    }*/

}
