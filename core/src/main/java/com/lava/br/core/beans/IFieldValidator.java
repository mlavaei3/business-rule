package com.lava.br.core.beans;

import com.lava.br.core.enums.ValidatorType;

import java.io.Serializable;

public interface IFieldValidator extends Serializable {
    ValidatorType getType();

    void setType(ValidatorType validatorType);

    String getExpression();

    void setExpression(String expression);

}
