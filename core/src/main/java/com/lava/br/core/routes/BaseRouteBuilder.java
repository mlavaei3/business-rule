package com.lava.br.core.routes;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.services.PluginService;
import lombok.Getter;
import lombok.Setter;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.routepolicy.quartz2.SimpleScheduledRoutePolicy;
//import org.apache.camel.routepolicy.quartz.SimpleScheduledRoutePolicy;

import java.util.Date;

/**
 * Created by Aria on 17/04/2018.
 */
@Getter
@Setter
public class BaseRouteBuilder extends RouteBuilder {

    private IPlugin plugin;
    private String routeName;

    private SimpleScheduledRoutePolicy policy;

    public BaseRouteBuilder(IPlugin plugin,String routeName) {
        setPlugin(plugin);
        setRouteName(routeName);
        configPolicy();
    }

    /*public void setPlugin(IPlugin plugin) {
        this.plugin = plugin;
    }

    public IPlugin getPlugin() {
        return plugin;
    }

    public void setPolicy(SimpleScheduledRoutePolicy policy) {
        this.policy = policy;
    }

    public SimpleScheduledRoutePolicy getPolicy() {
        return policy;
    }
*/
    private void configPolicy(){
        policy = new SimpleScheduledRoutePolicy();
        long startTime = System.currentTimeMillis() + 3000L;
        policy.setRouteStartDate(new Date(startTime));
        //policy.setRouteResumeDate(new Date(startTime));
        //policy.setRouteStartRepeatCount(1);
        //policy.setRouteStartRepeatInterval(3000);
    }

    @Override
    public void configure() throws Exception {

    }
}
