package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.ISimpleSelectField;
import com.lava.br.core.enums.TypeField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
//@SuperBuilder
public class SimpleSelectField extends Field implements ISimpleSelectField {
    Map<Object,String> selectFields=new LinkedHashMap<>();

    public SimpleSelectField(){
        super();
        this.setType(TypeField.SimpleSelectList);
    }
   /* public SimpleSelectField(String name,Class<?> fieldClass,String label,int order) {
        super(name,fieldClass,label,TypeField.SimpleSelectList,order);
    }*/

    @Builder
    private SimpleSelectField(int order,
                            Boolean visibility,
                            String label,
                            String name,
                            Class<?> fieldClass,
                            Object value,
                            TypeField type,
                            String helpText,
                            Boolean readonly ,
                            Boolean unique,
                            List<String> refTriggers,
                            List<String> refActions,
                            List<IFieldValidator> fieldValidators,
                            Map<Object,String>  selectFields){
        super(order,visibility,label,name,fieldClass,
                value,type,helpText,readonly,unique,
                refTriggers,refActions,fieldValidators);
        this.selectFields=selectFields;
        this.type=TypeField.SimpleSelectList;
        this.selectFields=new LinkedHashMap<>();

        setFieldValidators(new ArrayList<IFieldValidator>());
        if(this.refTriggers == null || this.refTriggers.size()==0){
            setRefTriggers(new ArrayList<>());
            this.refTriggers.add("default");
        }
        if(this.refActions == null || this.refActions.size()==0){
            setRefActions(new ArrayList<>());
            this.refActions.add("default");
        }

    }

}
