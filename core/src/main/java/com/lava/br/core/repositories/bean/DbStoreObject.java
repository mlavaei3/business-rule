package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IDbStoreObject;
import com.lava.br.core.enums.DbStoreType;
import jdk.nashorn.api.scripting.JSObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dbStoreObjects")
@TypeAlias("dbStoreObject")
@Getter
@Setter
public class DbStoreObject implements IDbStoreObject {

    @Id
    private String id;

    private String name;

    private DbStoreType dbStoreType;

    private String username;

    private Object object;


}
