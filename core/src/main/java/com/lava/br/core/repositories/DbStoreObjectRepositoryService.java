package com.lava.br.core.repositories;

import com.lava.br.core.beans.IDbStoreObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

//@Service(value = "dbStoreObjectService")
public abstract class DbStoreObjectRepositoryService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IDbStoreObjectRepository iDbStoreObjectRepository;

    public IDbStoreObjectRepository getBasicRepository() {
        return iDbStoreObjectRepository;
    }

}
