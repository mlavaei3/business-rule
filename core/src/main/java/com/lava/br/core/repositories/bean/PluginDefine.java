package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IPluginDefine;
import com.lava.br.core.enums.CommunicationType;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "pluginsDefine")
@TypeAlias("pluginDefine")
@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class PluginDefine implements IPluginDefine,Serializable {
    public PluginDefine(){

    }
    @Id
    private String id;
    private String name;
    private String label;
    private Boolean isProducer;
    private Boolean isConsumer;
    private Boolean isEnabled;
    private Boolean hasAccounting;
    private CommunicationType communicationType;

}
