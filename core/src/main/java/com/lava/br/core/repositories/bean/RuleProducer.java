package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IRuleProducer;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.TypeAlias;

import java.util.List;

@TypeAlias("producer")
@Getter
@Setter

public class RuleProducer implements IRuleProducer {
	public RuleProducer(){}
	private String id;
	private String pluginType;
	private int priority;
	private List<FieldAssigner> pluginOutputVariables;
	private List<FieldAssigner> pluginConfigVariables;
	private List<FieldFilter> producerFilters;
	private String producerFiltersExpression;
}
