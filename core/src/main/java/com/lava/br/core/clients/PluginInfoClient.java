package com.lava.br.core.clients;

import com.lava.br.core.enums.CommunicationType;
import com.lava.br.core.repositories.bean.Field;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aria on 20/05/2018.
 */
@Getter
@Setter
public class PluginInfoClient implements Serializable {
    String name;
    String label;
    Class<?> type;
    Boolean isProducer;
    Boolean isConsumer;
    List<Field> configVariables=new ArrayList<>();
    List<Field> inVariables=new ArrayList<>();
    List<Field> outVariables=new ArrayList<>();
    String businessRuleId;

    public PluginInfoClient(){}

}
