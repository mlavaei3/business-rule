package com.lava.br.core.enums;

public  enum BusinessRuleTemplateType {
    ALL("ALL"),SALES("SALES"),IT("IT");

    private String value;


    BusinessRuleTemplateType(String value){
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}
