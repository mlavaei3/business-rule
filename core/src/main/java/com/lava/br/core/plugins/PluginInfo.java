package com.lava.br.core.plugins;

/**
 * Created by Aria on 02/05/2018.
 */

import com.lava.br.core.beans.IField;
import com.lava.br.core.beans.IPluginInfo;
import com.lava.br.core.enums.CommunicationType;
import com.lava.br.core.repositories.bean.Field;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PluginInfo implements IPluginInfo {

    public static final String CONFIG = "CONFIG";
    public static final String IN = "IN";
    public static final String OUT = "OUT";

    private String name = "plugin";

    private String label = "";

    private Class type = null;

    private Boolean isProducer = true;

    private Boolean isConsumer = false;

    private String businessRuleId = null;

    public List<Field> configVariables =  new ArrayList<Field>();

    public List<Field> inVariables = new ArrayList<Field>();

    public List<Field> outVariables = new ArrayList<Field>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label=label;
    }

    public Boolean getIsProducer() {
        return isProducer;
    }

    public void setIsProducer(Boolean isProducer) {
        this.isProducer = isProducer;
    }

    public Boolean getIsConsumer() {
        return isConsumer;
    }

    public void setIsConsumer(Boolean isConsumer) {
        this.isConsumer = isConsumer;
    }

    public List<Field> getInVariables() {
        return inVariables;
    }

    public void setInVariables(List<Field> inVariables) {
        this.inVariables = inVariables;
    }

    public List<Field> getOutVariables() {
        return outVariables;
    }

    public void setOutVariables(List<Field> outVariables) {
        this.outVariables = outVariables;
    }

    public Class getType() {

        return type;
    }

    public void setType(Class type) {
        this.type = type;

    }

    public List<Field> getConfigVariables() {
        return configVariables;
    }

    public void setConfigVariables(List<Field> configVariables) {
        this.configVariables = configVariables;

    }

    public Object getValue(String typeVar, String fieldName) {
        Object result = null;
        if (typeVar.equals(IN)) {
            if (getInVariables() != null) {
                for (IField fi : getInVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        return fi.getValue();
                    }
                }
            }
        }
        if (typeVar.equals(OUT)) {
            if (getOutVariables() != null) {
                for (IField fi : getOutVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        return fi.getValue();
                    }
                }
            }
        }
        if (typeVar.equals(CONFIG)) {
            if (getConfigVariables() != null) {
                for (IField fi : getConfigVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        return fi.getValue();
                    }
                }
            }
        }
        return null;

    }

    public void setValue(String typeVar, String fieldName, Object fieldValue) {
        if (typeVar.equals("IN")) {
            if (getInVariables() != null) {
                for (IField fi : getInVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        fi.setValue(fieldValue);
                    }
                }
            }
        }
        if (typeVar.equals("OUT")) {
            if (getOutVariables() != null) {
                for (IField fi : getOutVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        fi.setValue(fieldValue);
                    }
                }
            }
        }
        if (typeVar.equals("CONFIG")) {
            if (getConfigVariables() != null) {
                for (IField fi : getConfigVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        fi.setValue(fieldValue);
                    }
                }
            }
        }

    }

    @Override
    public Field getFieldByName(String typeVar, String fieldName) {
        if (typeVar.equals(IN)) {
            if (getInVariables() != null) {
                for (IField fi : getInVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        return (Field) fi;
                    }
                }
            }
        }
        if (typeVar.equals(OUT)) {
            if (getOutVariables() != null) {
                for (IField fi : getOutVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        return (Field) fi;
                    }
                }
            }
        }
        if (typeVar.equals(CONFIG)) {
            if (getConfigVariables() != null) {
                for (IField fi : getConfigVariables()) {
                    if (fi.getName().equalsIgnoreCase(fieldName)) {
                        return (Field) fi;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getBusinessRuleId() {
        return businessRuleId;
    }

    @Override
    public void setBusinessRuleId(String businessRuleId) {
        this.businessRuleId = businessRuleId;

    }

}
