package com.lava.br.core.beans;

public interface IDbStoreObject {
    String getId();

    void setId(String id);

    String getName();

    void setName(String name);

    String getUsername();

    void setUsername(String username);

    Object getObject();

    void setObject(Object object);
}
