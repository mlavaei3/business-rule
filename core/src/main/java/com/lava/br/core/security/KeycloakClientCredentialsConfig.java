package com.lava.br.core.security;

import java.util.ArrayList;
import java.util.List;

import com.lava.br.core.restTemplates.KeycloakClientCredentialsRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.RetryLoadBalancerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.stereotype.Service;

@Service
public class KeycloakClientCredentialsConfig {

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;

    @Value("${keycloak.resource}")
    private String clientId;

    @Value("${keycloak.credentials.secret}")
    private String clientSecret;

    @Autowired
    RetryLoadBalancerInterceptor retryLoadBalancerInterceptor;

    @Bean
    @Primary
    @Qualifier("keycloakClientCredentialsRestTemplate")
    public KeycloakClientCredentialsRestTemplate createRestTemplate() {
        KeycloakClientCredentialsRestTemplate keycloakClientCredentialsRestTemplate= new KeycloakClientCredentialsRestTemplate(getClientCredentialsResourceDetails(),
                new DefaultOAuth2ClientContext());
        keycloakClientCredentialsRestTemplate.getInterceptors().add(retryLoadBalancerInterceptor);
        return keycloakClientCredentialsRestTemplate;
    }

    @Bean
    @Primary
    @Qualifier("keycloakClientCredentialsRestTemplateNoBalance")
    public KeycloakClientCredentialsRestTemplate createRestTemplateNoBalance() {
        KeycloakClientCredentialsRestTemplate keycloakClientCredentialsRestTemplate= new KeycloakClientCredentialsRestTemplate(getClientCredentialsResourceDetails(),
                new DefaultOAuth2ClientContext());
        return keycloakClientCredentialsRestTemplate;
    }

    private ClientCredentialsResourceDetails getClientCredentialsResourceDetails() {
        String accessTokenUri = String.format("%s/realms/%s/protocol/openid-connect/token",
                authServerUrl, realm);
        List<String> scopes = new ArrayList<String>(0); // TODO introduce scopes
        scopes.add("webclient");
        ClientCredentialsResourceDetails clientCredentialsResourceDetails =
                new ClientCredentialsResourceDetails();

        clientCredentialsResourceDetails.setAccessTokenUri(accessTokenUri);
        clientCredentialsResourceDetails.setAuthenticationScheme(AuthenticationScheme.header);
        clientCredentialsResourceDetails.setClientId(clientId);
        clientCredentialsResourceDetails.setClientSecret(clientSecret);
        clientCredentialsResourceDetails.setScope(scopes);

        return clientCredentialsResourceDetails;
    }

}