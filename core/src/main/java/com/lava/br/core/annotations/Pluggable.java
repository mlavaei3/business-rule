package com.lava.br.core.annotations;

import com.lava.br.core.enums.CommunicationType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
public @interface Pluggable {
	String nameOfPlugin() default "";
}
