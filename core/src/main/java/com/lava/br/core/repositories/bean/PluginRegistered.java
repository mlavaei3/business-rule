package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IPluginRegistered;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "pluginsRegistered")
@TypeAlias("pluginRegistered")
@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class PluginRegistered implements IPluginRegistered {
    public PluginRegistered(){

    }
    @Id
    private String id;

    private String businessRuleId;

    private String instanceId;

    private String consumerType;

    private String pluginUniqueIdentity;


}
