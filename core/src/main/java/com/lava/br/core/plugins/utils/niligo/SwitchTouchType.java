package com.lava.br.core.plugins.utils.niligo;

public enum SwitchTouchType {
    Long1,
    Long2,
    Long3,
    Long4,
    Long5,
    Long6,
    Long7,
    Long8,
    Long9,
    Long10,
    Double,
    Third,
    Fourth,
    Fifth,
    Sixth,
    Seventh,
    On,
    Off
}
