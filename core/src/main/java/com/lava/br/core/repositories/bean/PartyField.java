package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPartyField;
import com.lava.br.core.enums.TypeField;
import lombok.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Getter
@Setter
//@SuperBuilder
@NoArgsConstructor
//@Builder
public class PartyField extends Field implements IPartyField {

    String requestUrl;
    String partyUrl;
    Function<Map<String, Object>, Map<String, String>> selectFields;

    @Builder
    private PartyField(int order,
            Boolean visibility,
            String label,
            String name,
            Class<?> fieldClass,
            Object value,
            TypeField type,
            String helpText,
            Boolean readonly ,
            Boolean unique,
            List<String>refTriggers,
            List<String> refActions,
            List<IFieldValidator> fieldValidators,
            String requestUrl,
            String partyUrl,
            Function<Map<String, Object>, Map<String, String>> selectFields){
        super(order,visibility,label,name,fieldClass,
                value,type,helpText,readonly,unique,
                refTriggers,refActions,fieldValidators);
        this.requestUrl = requestUrl;
        this.partyUrl=partyUrl;
        this.selectFields=selectFields;
        this.setType(TypeField.PartySelectList);

        setFieldValidators(new ArrayList<IFieldValidator>());
        if(this.refTriggers == null || this.refTriggers.size()==0){
            setRefTriggers(new ArrayList<>());
            this.refTriggers.add("default");
        }
        if(this.refActions == null || this.refActions.size()==0){
            setRefActions(new ArrayList<>());
            this.refActions.add("default");
        }
    }
    /*public PartySelectList(String name,Class<?> fieldClass,String label,int order) {
        super(name,fieldClass,label,TypeField.PartField,order);

    }*/
}
