package com.lava.br.core.repositories.bean;


import com.lava.br.core.beans.IField;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.enums.TypeField;
import lombok.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
//@SuperBuilder
@AllArgsConstructor
//@Builder
public class Field implements IField {
	int order;
	Boolean visibility=true;
	String label;
	String name;
	Class<?> fieldClass=String.class;
	Object value;
	TypeField type=TypeField.Text;
	String helpText="";
	Boolean readonly =false;
	Boolean unique=false;
	List<String> refTriggers=new ArrayList<>();
	List<String> refActions=new ArrayList<>();
	List<IFieldValidator> fieldValidators=new ArrayList<>();

	public Field() {
		refTriggers.add("default");
		refActions.add("default");
	}

	/*public static Builder builder() {
		return new Builder2();
	}

	public static class Builder {

		private String name;
		private String label;
		private int order;
		private Class<?> fieldClass;
		private Boolean visibility;

		public Builder2 name(String name) {
			this.name = name;
			return this;
		}

		public Builder2 label(String label) {
			this.label = label;
			return this;
		}

		public Builder2 visibility(Boolean visibility) {
			this.visibility = visibility;
			return this;
		}
		public Builder2 fieldClass(Class<?> fieldClass) {
			this.fieldClass = fieldClass;
			return this;
		}

		public Builder2 order(int order) {
			this.order = order;
			return this;
		}
		public Field build() {
			Field field=new Field();
			field.setName(name);
			field.setLabel(label);
			field.setFieldClass(fieldClass);
			field.setOrder(order);
			field.setVisibility(visibility);
			return field;
		}
	}*/

	/*public Field(String name,int order) {
		this();
		this.setName(name);
		this.setOrder(order);
	}

	public Field(String name,String label,int order) {
		this(name,order);
		this.setLabel(label);
	}

	public Field(String name,Class<?> fieldClass,String label,int order) {
		this(name,label,order);
		this.fieldClass=fieldClass;
	}
	public Field(String name, Class<?> fieldClass ,String label, TypeField type,int order) {
		this(name,fieldClass,label,order);
		this.setType(type);
	}
	public Field(String name, Class<?> fieldClass,  String label, Boolean unique,int order) {
		this(name,fieldClass,label,order);
		this.setUnique(unique);
	}
	public Field(String name, Class<?> fieldClass,  String label,String[] triggers,String[] actions,int order) {
		this(name,fieldClass,label,false,triggers,actions,order);
    }

	public Field(String name, Class<?> fieldClass,  String label, Boolean unique,String[] triggers,String[] actions,int order) {
		this(name,fieldClass,label,order);
		this.setUnique(unique);
		if (actions!=null)
		for (String action : actions) {
			getRefActions().add(action);
		}

		if (triggers!=null)
		for (String trigger : triggers) {
				getRefTriggers().add(trigger);
		}
	}*/

	public void removeDefaultTriggersAndActions(){
		getRefTriggers().remove("default");
		getRefActions().remove("default");
	}

	@Override
	public List<String> getRefTriggers() {
		return refTriggers;
	}

	@Override
	public List<String> getRefActions() {
		return refActions;
	}

	@Override
	public List<IFieldValidator> getFieldValidators() {
		return fieldValidators;
	}

	@Override
	public void setFieldValidators(List<IFieldValidator> fieldValidators) {
		this.fieldValidators=fieldValidators;
	}
}
