package com.lava.br.core.plugins.scripting;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.Field;

/**
 * Created by Aria on 05/06/2018.
 */
public class ScriptingVariables {
    IPlugin iPlugin;
    String typeVariables;

    public ScriptingVariables(IPlugin iPlugin,String typeVariables){
        this.iPlugin=iPlugin;
        this.typeVariables=typeVariables;
    }

    public void set(String name, Object value) {

        Field field=new Field();//.builder().name(name).order(1).build();
        field.setName(name);
        field.setValue(value);
        switch (typeVariables){
            case PluginInfo.CONFIG:
                this.iPlugin.getInfo().getConfigVariables().add(field);
                break;
            case PluginInfo.OUT:
                this.iPlugin.getInfo().getOutVariables().add(field);
                break;
            case PluginInfo.IN:
                this.iPlugin.getInfo().getInVariables().add(field);
                break;
        }
    }

    public Object get(String name) {
        return this.iPlugin.getInfo().getValue(typeVariables,name);
    }
}
