package com.lava.br.core.restTemplates;

import com.lava.br.core.contexts.UserContextInterceptor;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

//@Configuration
public class CustomRestTemplate {
    @LoadBalanced
    @Bean
    @Qualifier("restTemplate")
    public RestTemplate getCustomRestTemplate() {

        RestTemplate template = new RestTemplate();
        List interceptors = template.getInterceptors();
        if (interceptors == null) {
            template.setInterceptors(
                    Collections.singletonList(
                            new UserContextInterceptor()));
        } else {
            interceptors.add(new UserContextInterceptor());
            template.setInterceptors(interceptors);
        }

        return template;
    }
}
