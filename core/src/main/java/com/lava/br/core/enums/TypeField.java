package com.lava.br.core.enums;

/**
 * Created by Aria on 22/04/2018.
 */
public enum TypeField {
    Text,Number,Date,Url,Email,SimpleSelectList, LazySelectList, PartySelectList,Color,Script,Cron
}
