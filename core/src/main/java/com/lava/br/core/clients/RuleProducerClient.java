package com.lava.br.core.clients;

import com.lava.br.core.repositories.bean.FieldAssigner;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Aria on 18/05/2018.
 */
@Getter
@Setter
public class RuleProducerClient {
    String id;
    String pluginType;
    int priority;
    List<FieldAssigner> pluginOutputVariables;
    List<FieldAssigner> pluginConfigs;
}
