package com.lava.br.core.beans;

import com.lava.br.core.enums.BusinessRuleStatus;
import com.lava.br.core.repositories.bean.FieldAssigner;

import java.util.List;

public interface IBusinessRule {
	String getId();

	void setId(String id);
	
	String getName();

	void setName(String name);

	BusinessRuleStatus getStatus();

	void setStatus(BusinessRuleStatus status);


	String getConsumerType();

	void setConsumerType(String consumerType);

	List<FieldAssigner> getConsumerConfig();

	void setConsumerConfig(List<FieldAssigner> consumerConfig);

	List<IFieldFilter> getConsumerFilters();

	void setConsumerFilters(List<IFieldFilter> consumerFilters);

	String getConsumerFiltersExpression();

	void setConsumerFiltersExpression(String consumerFiltersExpression);

	String getUsername();

	void setUsername(String username);

	String getArtifactId();

	void setArtifactId(String artifactId);

	String getVersionId();

	void setVersionId(String versionId);

	List<IRuleProducer> getRuleProducers();

	void setRuleProducers(List<IRuleProducer> ruleProducers);

}
