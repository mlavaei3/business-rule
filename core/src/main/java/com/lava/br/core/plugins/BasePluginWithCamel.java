package com.lava.br.core.plugins;

import com.lava.br.core.beans.IPlugin;
import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Producer;
import org.apache.camel.model.RouteDefinition;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class BasePluginWithCamel extends BasePlugin{

    @Autowired
    private CamelContext camelContext;

    public BasePluginWithCamel(){
        super();
    }

    public CamelContext getCamelContext() {
        return this.camelContext;
    }

    public void setCamelContext(CamelContext camelContext) {
        this.camelContext=camelContext;
    }

    public void runTemplateProducer(Endpoint endpoint, Exchange exchange){
        Producer producer = null;
        try {
            producer = endpoint.createProducer();
            producer.start();
            producer.process(exchange);
            producer.stop();

            //endpoint.shutdown();
            getCamelContext().removeEndpoint(endpoint);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            producer = null;
            endpoint = null;
        }

    }

    @Override
    public void unConsume(IPlugin plugin) {
        try {
            String routeName=getUniqueIdentity(plugin);

            getCamelContext().stopRoute(routeName);
            RouteDefinition routedefinition = getCamelContext().getRouteDefinition(routeName);
            List<RouteDefinition> list = new ArrayList<>();
            list.add(routedefinition);
            getCamelContext().removeRouteDefinitions(list);
            getCamelContext().removeRoute(routeName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
