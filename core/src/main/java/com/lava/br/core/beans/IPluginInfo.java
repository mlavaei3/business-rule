package com.lava.br.core.beans;


import com.lava.br.core.enums.CommunicationType;
import com.lava.br.core.repositories.bean.Field;

import java.util.List;

public interface IPluginInfo {

	 String getName();

	 void setName(String name);

	 String getLabel();

	 void setLabel(String label);
	
	 Class<?> getType();

	 void setType(Class<?> type);

	 Boolean getIsProducer();

	 void setIsProducer(Boolean isProducer);

	 Boolean getIsConsumer();

	 void setIsConsumer(Boolean isConsumer);

	 List<Field> getConfigVariables();

	 void setConfigVariables(List<Field> configVariables);

	 List<Field> getInVariables();

	 void setInVariables(List<Field> inVariables);

	 List<Field> getOutVariables();

	 void setOutVariables(List<Field> outVariables);

	 Object getValue(String typeVar, String fieldName);

	 void setValue(String typeVar, String fieldName, Object fieldValue);

	 Field getFieldByName(String typeVar, String fieldName);

	String getBusinessRuleId();

	void setBusinessRuleId(String businessRuleId);

}
