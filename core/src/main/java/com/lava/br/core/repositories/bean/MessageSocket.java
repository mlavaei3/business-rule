package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IMessageSocket;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

//@Builder
@Getter
@Setter
public class MessageSocket implements IMessageSocket  {
    String objectId;
    String message;
    String sessionId;

    public MessageSocket(){

    }
}
