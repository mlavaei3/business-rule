package com.lava.br.core.mappers;

import com.lava.br.core.beans.IField;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.clients.PluginClient;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.Field;
import com.lava.br.core.repositories.bean.LazySelectField;
import com.lava.br.core.repositories.bean.PartyField;
import com.lava.br.core.repositories.bean.SimpleSelectField;
//import org.hibernate.validator.internal.xml.FieldType;


/**
 * Created by Aria on 20/05/2018.
 */

public class PluginMapper {
    public static PluginInfoClient toPluginInfoClient(IPlugin iPlugin){
        PluginInfoClient pc=new PluginInfoClient();
        pc.setBusinessRuleId(iPlugin.getInfo().getBusinessRuleId());
        pc.setName(iPlugin.getInfo().getName());
        pc.setLabel(iPlugin.getInfo().getLabel());
        pc.setIsConsumer(iPlugin.getInfo().getIsConsumer());
        pc.setIsProducer(iPlugin.getInfo().getIsProducer());
        for (Field field: iPlugin.getInfo().getConfigVariables()){
            pc.getConfigVariables().add(field);
        }
        for (Field field: iPlugin.getInfo().getInVariables()){
            pc.getInVariables().add(field);
        }
        for (Field field: iPlugin.getInfo().getOutVariables()){
            pc.getOutVariables().add(field);
        }
        return pc;
    }

    public static void fieldMapper(IField source, IField destination){
        destination.setName(source.getName());
        destination.setFieldClass(source.getFieldClass());
        destination.setValue(source.getValue());
        destination.setVisibility(source.getVisibility());
        destination.setType(source.getType());
        destination.setReadonly(source.getReadonly());
        destination.setUnique(source.getUnique());
        destination.setRefActions(source.getRefActions());
        destination.setRefTriggers(source.getRefTriggers());
        destination.setOrder(source.getOrder());

    }
    public static Field cloneField(Field field){
        Field newField=null;
        if(field.getType().equals(TypeField.Text)
                || field.getType().equals(TypeField.Number)
                || field.getType().equals(TypeField.Url)
                || field.getType().equals(TypeField.Cron)
                || field.getType().equals(TypeField.Email)
                || field.getType().equals(TypeField.Date)
                || field.getType().equals(TypeField.Script)
                || field.getType().equals(TypeField.Color)){
            newField = new Field();
        }
        if(field.getType().equals(TypeField.SimpleSelectList)){
            newField = new SimpleSelectField();
        }
        if(field.getType().equals(TypeField.LazySelectList)){
            newField = new LazySelectField();
        }
        if(field.getType().equals(TypeField.PartySelectList)){
            newField = new PartyField();
        }
        fieldMapper(field,newField);
        return newField;
    }

    public static void pluginInfoCloneTo(IPlugin source, IPlugin destination){
        destination.getInfo().setName(source.getInfo().getName());
        if (source.getInfo().getInVariables() != null) {
            //destination.getInfo().setInVariables(new ArrayList<Field>());
            for (Field field : source.getInfo().getInVariables()) {
                Field destField=destination.getInfo().getFieldByName(PluginInfo.IN,field.getName());
                if ( destField !=null){
                    fieldMapper(field,destField);
                }else{
                    destField=cloneField(field);
                    destination.getInfo().getInVariables().add(destField);
                }


                /*boolean flag=false;
                for (IField field1 :destination.getInfo().getInVariables()){
                    if (field1.getName().equals(field.getName())){
                        fieldMapper(field,field1);
                        flag=true;
                    }
                }
                if(flag) continue;

                Field f=null;
                if(field.getGenre().equals("primitive")){
                    f = new Field();
                }
                if(field.getGenre().equals("simpleselect")){
                    f = new SimpleSelectField();
                }

                fieldMapper(field,f);
                destination.getInfo().getInVariables().add(f);*/
            }
        }
        if (source.getInfo().getOutVariables() != null) {
            for (Field field : source.getInfo().getOutVariables()) {
                Field destField=destination.getInfo().getFieldByName(PluginInfo.OUT,field.getName());
                if ( destField !=null){
                    fieldMapper(field,destField);
                }else{
                    destField=cloneField(field);
                    destination.getInfo().getOutVariables().add(destField);
                }
            }
        }

        if (source.getInfo().getConfigVariables() != null) {
            for (Field field : source.getInfo().getConfigVariables()) {
                Field destField=destination.getInfo().getFieldByName(PluginInfo.CONFIG,field.getName());
                if ( destField !=null){
                    fieldMapper(field,destField);
                }else{
                    destField=cloneField(field);
                    destination.getInfo().getConfigVariables().add(destField);
                }
            }
        }
    }

    public static void pluginInfoCloneTo(PluginInfoClient source, IPlugin destination){
        destination.getInfo().setName(source.getName());
        destination.getInfo().setBusinessRuleId(source.getBusinessRuleId());
        if (source.getInVariables() != null) {
            //destination.getInfo().setInVariables(new ArrayList<Field>());
            for (Field field : source.getInVariables()) {
                Field destField=destination.getInfo().getFieldByName(PluginInfo.IN,field.getName());
                if ( destField !=null){
                    fieldMapper(field,destField);
                }else{
                    destField=cloneField(field);
                    destination.getInfo().getInVariables().add(destField);
                }


                /*boolean flag=false;
                for (IField field1 :destination.getInfo().getInVariables()){
                    if (field1.getName().equals(field.getName())){
                        fieldMapper(field,field1);
                        flag=true;
                    }
                }
                if(flag) continue;

                Field f=null;
                if(field.getGenre().equals("primitive")){
                    f = new Field();
                }
                if(field.getGenre().equals("simpleselect")){
                    f = new SimpleSelectField();
                }

                fieldMapper(field,f);
                destination.getInfo().getInVariables().add(f);*/
            }
        }
        if (source.getOutVariables() != null) {
            for (Field field : source.getOutVariables()) {
                Field destField=destination.getInfo().getFieldByName(PluginInfo.OUT,field.getName());
                if ( destField !=null){
                    fieldMapper(field,destField);
                }else{
                    destField=cloneField(field);
                    destination.getInfo().getOutVariables().add(destField);
                }
            }
        }

        if (source.getConfigVariables() != null) {
            for (Field field : source.getConfigVariables()) {
                Field destField=destination.getInfo().getFieldByName(PluginInfo.CONFIG,field.getName());
                if ( destField !=null){
                    fieldMapper(field,destField);
                }else{
                    destField=cloneField(field);
                    destination.getInfo().getConfigVariables().add(destField);
                }
            }
        }
    }

    public static void pluginInfoCopyTo(IPlugin source, IPlugin destination){
        destination.getInfo().setName(source.getInfo().getName());

        for (IField field : source.getInfo().getInVariables()) {
            destination.getInfo().setValue(PluginInfo.IN,field.getName(),field.getValue());
        }
        for (IField field : source.getInfo().getOutVariables()) {
            destination.getInfo().setValue(PluginInfo.OUT,field.getName(),field.getValue());
        }
        for (IField field : source.getInfo().getConfigVariables()) {
            destination.getInfo().setValue(PluginInfo.CONFIG,field.getName(),field.getValue());
        }
    }

    public static void pluginInfoCopyTo(PluginInfoClient source, IPlugin destination){
        destination.getInfo().setName(source.getName());
        destination.getInfo().setBusinessRuleId(source.getBusinessRuleId());

        for (IField field : source.getInVariables()) {
            destination.getInfo().setValue(PluginInfo.IN,field.getName(),field.getValue());
        }
        for (IField field : source.getOutVariables()) {
            destination.getInfo().setValue(PluginInfo.OUT,field.getName(),field.getValue());
        }
        for (IField field : source.getConfigVariables()) {
            destination.getInfo().setValue(PluginInfo.CONFIG,field.getName(),field.getValue());
        }
    }

}
