package com.lava.br.core.plugins.utils.niligo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.services.PluginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public class Endpoints {


    private String userPassForToken;


    private String tokenUri;


    private String redirectUri;


    private PluginService pluginService;

    public Endpoints(String userPassForToken, String tokenUri, String redirectUri,PluginService pluginService){
        this.userPassForToken=userPassForToken;
        this.tokenUri=tokenUri;
        this.redirectUri=redirectUri;
        this.pluginService=pluginService;
    }

    public ResponseEntity callbackFromParty( String state,String code)  {
        String sessionId=state.split(" ")[0];
        String objectId=state.split(" ")[1];
        String userid=state.split(" ")[2];

        //String userPass = "ghasem:sadeghi";
        String basicEncoded = Base64Utils.encodeToString(userPassForToken.getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + basicEncoded);
        HttpEntity<String> request = new HttpEntity<>(headers);

        String url = tokenUri+"/oauth/token?";
        url += "code=" + code;
        url += "&grant_type=authorization_code";
        url += "&redirect_uri="+redirectUri;

        try {
            RestTemplate restTemplate=new RestTemplate();
            ResponseEntity<String> rsp = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

            //(2)extract the Access Token From the recieved JSON response
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(rsp.getBody());
            String accessToken = node.path("access_token").asText();

            pluginService.addKeyWithValue(userid,
                    DbStoreType.ACCOUNTING,
                    "account",
                    node.path("username").asText(),"token",
                    String.format("{\"accessToken\":\"%s\"}",accessToken));

            pluginService.sendToSocket(sessionId,objectId,"{\""+node.path("username").asText()+ "\":\""+node.path("username").asText()+"\"}");


        } catch (HttpClientErrorException e) {
            System.out.println("\n1:"+e.getStatusCode());
            System.out.println("\n2:"+e.getResponseBodyAsString());
            //model.addAttribute("message", e.getResponseBodyAsString());
            //return "error";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("<script language='javascript'>parentwin = window.self;\n" +
                "parentwin.opener = window.self;\n" +
                "parentwin.close(); </script>");

    }
}
