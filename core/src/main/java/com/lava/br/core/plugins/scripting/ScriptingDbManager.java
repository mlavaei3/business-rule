package com.lava.br.core.plugins.scripting;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ScriptingDbManager {
    Invocable invocable;
    ScriptEngine engineMain;
    public ScriptingDbManager()  {
        engineMain = new ScriptEngineManager().getEngineByName("JavaScript");
        try {
            engineMain.eval("var fun1 = function(str) {\n" +
                    "    return JSON.parse(str);\n" +
                    "};\n" +
                    " var fun2 = function (object) {\n" +
                    "   return JSON.stringify(object);\n" +
                    "};"
            );

        } catch (ScriptException e) {
            e.printStackTrace();
        }
        invocable = (Invocable) engineMain;

    }

    public Object save(Object obj)  {
        try {
            String hh=invocable.invokeFunction("fun2",obj).toString();
            return invocable.invokeFunction("fun2",obj);
        } catch (ScriptException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Object load(String str) throws NoSuchMethodException, ScriptException {
        return invocable.invokeFunction("fun1",str);
    }
}
