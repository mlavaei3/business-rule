package com.lava.br.core.processes;

import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.configs.PluginConfig;
import com.lava.br.core.enums.CommunicationType;
import com.lava.br.core.services.PluginService;
//for stream
//import com.lava.br.core.streams.plugin.RequestToServer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.context.ApplicationContext;

/**
 * Created by Aria on 19/04/2018.
 */
public class BaseProcessor implements Processor {
    private IPlugin plugin;
    private ApplicationContext applicationContext = null;
    // private ServerService serverService;
    private PluginService pluginService;
    //for stream
    //private RequestToServer requestToServer;
    private PluginConfig pluginConfig;

    public BaseProcessor(IPlugin plugin) {
        setPlugin(plugin);
        applicationContext = ApplicationContextProvider.getApplicationContext();
        this.pluginService=(PluginService) applicationContext.getBean(plugin.getInfo().getName()+"Service");
        this.pluginConfig=(PluginConfig) applicationContext.getBean("pluginConfig");
        //for stream
        /*if(this.pluginConfig.getCommunicationType()==CommunicationType.Stream){
            this.requestToServer=(RequestToServer) applicationContext.getBean(plugin.getInfo().getName()+"RequestToServer");
        }*/
     }

    public void sendMessageToServer(){
        if(this.pluginConfig.getCommunicationType()==CommunicationType.Rest)
            pluginService.sendMessageToServer(getPlugin());
        //else
            //for stream
            //requestToServer.sendMessageToServer(getPlugin());


    }

    /*public List<String> getArtifactIds(List<FieldAssigner> fieldAssigners) {
        BusinessRuleRepositoryService businessRuleService=(BusinessRuleRepositoryService) ApplicationContextProvider.getApplicationContext().getBean("businessRuleService");
        List<BusinessRule> brs=null;
        if(fieldAssigners.size()==1){
            brs= businessRuleService.findByConsumerTypeAndConsumerConfigFieldName(getPlugin().getInfo().getName(),fieldAssigners.get(0).getFieldName(),getPlugin().getInfo().getValue(PluginInfo.CONFIG, fieldAssigners.get(0).getFieldName()).toString());   //exchange.getIn().getHeader("username").toString());
        }else{
            brs= businessRuleService.findByConsumerTypeAndFieldAssigners(getPlugin().getInfo().getName(),fieldAssigners);
        }
        List<String> lstArtifactIds=new ArrayList<String>();

        for (BusinessRule br:brs ) {
            if(lstArtifactIds.contains(br.getArtifactId())==false){
                lstArtifactIds.add(br.getArtifactId());
            }
        }
        return lstArtifactIds;

    }

    public void setArtifactIds(List<String> artifactIds) {
        this.artifactIds = artifactIds;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
*/
    @Override
    public void process(Exchange exchange) throws Exception {
    }

    public void setPlugin(IPlugin plugin) {
        this.plugin = plugin;
    }

    public IPlugin getPlugin() {
        return plugin;
    }
}
