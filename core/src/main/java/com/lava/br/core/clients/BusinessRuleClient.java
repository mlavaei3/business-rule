package com.lava.br.core.clients;

import com.lava.br.core.enums.BusinessRuleStatus;
import com.lava.br.core.repositories.bean.FieldAssigner;
import com.lava.br.core.repositories.bean.FieldFilter;
import com.lava.br.core.repositories.bean.RuleProducer;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * Created by Aria on 18/05/2018.
 */
@Getter
@Setter

public class BusinessRuleClient {

    public BusinessRuleClient(){
        id=ObjectId.get().toString();
    }

    String id;

    String username;

    String name;

    String artifactId;

    String versionId;

    String consumerType;

    List<RuleProducer> ruleProducers;

    List<FieldFilter> consumerFilters;

    List<FieldAssigner> consumerConfig;

    String consumerFiltersExpression;

    BusinessRuleStatus status;
}
