package com.lava.br.core.beans;

import com.lava.br.core.repositories.bean.PluginRegistered;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface IPluginRegisteredRepository extends MongoRepository<PluginRegistered, String> {
    @Query("{ 'businessRuleId' : ?0 }")
    PluginRegistered findFirstByBusinessRuleId(String businessRuleId);

    @Query("{ 'businessRuleId' : ?0 }")
    List<PluginRegistered> findByBusinessRuleId(String businessRuleId);

    @Query("{ 'instanceId' : ?0 }")
    List<PluginRegistered> findByInstanceId(String instanceId);

    @Query("{ 'consumerType' : ?0 , 'pluginUniqueIdentity' : ?1}")
    List<PluginRegistered> findByConsumerTypeAndPluginUniqueIdentity(String consumerType,String pluginUniqueIdentity);

    @Query("{ 'consumerType' : ?0 , 'pluginUniqueIdentity' : ?1 , 'instanceId' : ?2}")
    List<PluginRegistered> findByConsumerTypeAndPluginUniqueIdentityAndInstanceId(String consumerType,String pluginUniqueIdentity,String instanceId);

}