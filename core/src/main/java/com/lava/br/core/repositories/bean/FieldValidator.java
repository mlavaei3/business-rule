package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.enums.ValidatorType;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class FieldValidator implements IFieldValidator {
    ValidatorType type;
    String expression;
}
