package com.lava.br.core.beans;

import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
public interface IScriptService {
    void exec(IPlugin plugin);
}
