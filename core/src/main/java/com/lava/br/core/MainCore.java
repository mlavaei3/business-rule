package com.lava.br.core;

/**
 * Created by Aria on 07/05/2018.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ImportResource({ "classpath:spring/camel-context.xml" })
@ComponentScan(value = "com.lava.br")
public class MainCore {

    public static void main(String[] args) {
        SpringApplication.run(MainCore.class, args);
    }
}

