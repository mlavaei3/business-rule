package com.lava.br.core.plugins.scripting;

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.BusinessRuleRepositoryService;
import com.lava.br.core.repositories.bean.Field;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

/**
 * Created by Aria on 05/06/2018.
 */

public abstract class  Scripting extends BasePlugin {


    @PostConstruct
    public void init() {
        /*Pluggable pluginName=null;
        if (this.getClass().isAnnotationPresent(Pluggable.class)) {
            pluginName = this.getClass().getAnnotation(Pluggable.class);
        }
        this.setInfo(new PluginInfo());
        this.getInfo().setName(pluginName.nameOfPlugin());

        this.getInfo().setConfigVariables(new ArrayList<Field>());
        this.getInfo().setInVariables(new ArrayList<Field>());
        this.getInfo().setOutVariables(new ArrayList<Field>());*/
        // configs
        Field field=new Field();
        field.setName("name");
        field.setLabel("name");
        field.setType(TypeField.Text);
        field.setFieldClass(java.lang.String.class);
        field.setOrder(1);
        this.getInfo().getOutVariables().add(field);//Field.builder().name("name").fieldClass(java.lang.String.class).label("name").order(1).build());

        // outVar
        field=new Field();
        field.setName("script");
        field.setLabel("script");
        field.setType(TypeField.Script);
        field.setFieldClass(java.lang.String.class);
        field.setOrder(2);
        this.getInfo().getOutVariables().add(field);// Field.builder().name("script").fieldClass(java.lang.String.class).label("script").order(1).build());
        //this.getInfo().getOutVariables().add(new Field("input", IPlugin.class, null, "input", TypeField.System));
        //this.getInfo().getOutVariables().add(new Field("result", java.lang.Object.class, null, "result",TypeField.System));

        //this.setPluginsRegisterd(new ArrayList<IPlugin>());
    }

    public abstract void produce(IPlugin plugin);

}
