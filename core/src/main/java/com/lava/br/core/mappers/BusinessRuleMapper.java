package com.lava.br.core.mappers;

/**
 * Created by Aria on 02/05/2018.
 */
import com.lava.br.core.beans.IRuleProducer;
import com.lava.br.core.clients.BusinessRuleClient;
import com.lava.br.core.clients.RuleProducerClient;
import com.lava.br.core.repositories.bean.BusinessRule;
import com.lava.br.core.repositories.bean.FieldAssigner;
import com.lava.br.core.repositories.bean.FieldFilter;
import com.lava.br.core.repositories.bean.RuleProducer;

import java.util.ArrayList;

/**
 * Created by Aria on 23/03/2018.
 */
public class BusinessRuleMapper {

    public static BusinessRule toBusinessRuleClient(BusinessRuleClient br){
        BusinessRule businessRule=new BusinessRule();
        br.getConsumerConfig().forEach(p->businessRule.getConsumerConfig().add(p));
        br.getConsumerFilters().forEach(p->businessRule.getConsumerFilters().add(p));
        br.getRuleProducers().forEach(p->businessRule.getRuleProducers().add(p));

        businessRule.setArtifactId(br.getArtifactId());
        businessRule.setConsumerFiltersExpression(br.getConsumerFiltersExpression());
        businessRule.setConsumerType(br.getConsumerType());
        businessRule.setId(br.getId());
        businessRule.setName(br.getName());
        businessRule.setUsername(br.getUsername());
        businessRule.setVersionId(br.getVersionId());
        businessRule.setStatus(br.getStatus());

        return businessRule;

    }

    public static BusinessRuleClient toBusinessRuleClient(BusinessRule br){
        BusinessRuleClient businessRule=new BusinessRuleClient();
        businessRule.setConsumerConfig(new ArrayList<>());
        businessRule.setConsumerFilters(new ArrayList<>());
        businessRule.setRuleProducers(new ArrayList<>());
        br.getConsumerConfig().forEach(p->businessRule.getConsumerConfig().add(p));
        br.getConsumerFilters().forEach(p->businessRule.getConsumerFilters().add((FieldFilter) p));
        br.getRuleProducers().forEach(p->businessRule.getRuleProducers().add((RuleProducer) p));

        businessRule.setArtifactId(br.getArtifactId());
        businessRule.setConsumerFiltersExpression(br.getConsumerFiltersExpression());
        businessRule.setConsumerType(br.getConsumerType());
        businessRule.setId(br.getId());
        businessRule.setName(br.getName());
        businessRule.setUsername(br.getUsername());
        businessRule.setVersionId(br.getVersionId());
        businessRule.setStatus(br.getStatus());

        return businessRule;

    }

    public void setConstraintsOnMissionTreeDTO(BusinessRuleClient businessRuleClient,BusinessRule businessRule) { // do not forget the @MappingTarget annotation or it will not work
       /* businessRule.setRuleProducers(new ArrayList<IRuleProducer>());

        for (RuleProducerClient v:businessRuleClient.getRuleProducers()){
            businessRule.getRuleProducers().add((IRuleProducer) toRuleProducer(v));
        }
        *//*businessRuleClient.getRuleProducers().forEach(v -> {
            businessRule.getRuleProducers().add((IRuleProducer) toRuleProducer(v));
        });*//*
        businessRule.setName(businessRuleClient.getName()); // can do any additional logic here, using services etc.
    */
    }
}
