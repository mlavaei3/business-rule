package com.lava.br.core.plugins;

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.beans.IPluginInfo;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.repositories.bean.Field;
import com.lava.br.core.repositories.bean.SimpleSelectField;
import com.lava.br.core.services.PluginService;
import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Producer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aria on 20/04/2018.
 */
@Pluggable(nameOfPlugin = "basePlugin")
public class BasePlugin implements IPlugin {
    // String businessRuleId="";

    public BasePlugin(){
        Pluggable pluginName=null;
        if (this.getClass().isAnnotationPresent(Pluggable.class)) {
            pluginName = this.getClass().getAnnotation(Pluggable.class);
        }
        this.setInfo(new PluginInfo());
        this.getInfo().setName(pluginName.nameOfPlugin());

        this.getInfo().setConfigVariables(new ArrayList<Field>());
        this.getInfo().setInVariables(new ArrayList<Field>());
        this.getInfo().setOutVariables(new ArrayList<Field>());
        //default
        Field useridField=new Field();//.builder().name("userid").fieldClass(java.lang.String.class).label("userid").order(100).build();
        useridField.setName("userid");
        useridField.setFieldClass(java.lang.String.class);
        useridField.setLabel("userid");
        useridField.setOrder(100);
        useridField.setVisibility(false);
        this.getInfo().getConfigVariables().add(useridField);

        SimpleSelectField simpleSelectFieldTriggers=SimpleSelectField.builder().name("consumerTrigger")
        .fieldClass(List.class).label("consumerTrigger").order(110).build();
        simpleSelectFieldTriggers.getSelectFields().put("default","Default");
        this.getInfo().getConfigVariables().add(simpleSelectFieldTriggers);

        SimpleSelectField simpleSelectFieldActions=SimpleSelectField.builder().name("producerAction")
        .fieldClass(List.class).label("producerAction").order(111).build();
        simpleSelectFieldActions.getSelectFields().put("default","Default");
        this.getInfo().getConfigVariables().add(simpleSelectFieldActions);

        useridField=new Field();//.builder().name("userid").fieldClass(java.lang.String.class).label("userid").order(101).build();
        useridField.setVisibility(false);
        useridField.setName("userid");
        useridField.setFieldClass(java.lang.String.class);
        useridField.setLabel("userid");
        useridField.setOrder(101);
        useridField.setVisibility(false);
        this.getInfo().getOutVariables().add(useridField);

        Field inputField=new Field();//.builder().name("input").fieldClass(IPlugin.class).label("input").order(102).build();
        inputField.setVisibility(false);
        inputField.setFieldClass(IPlugin.class);
        inputField.setLabel("input");
        inputField.setName("input");
        inputField.setOrder(102);
        this.getInfo().getOutVariables().add(inputField);

        Field errorField=new Field();//.builder().name("error").fieldClass(java.lang.String.class).label("error").order(103).build();
        errorField.setVisibility(false);
        errorField.setFieldClass(java.lang.String.class);
        errorField.setLabel("error");
        errorField.setName("error");
        errorField.setOrder(103);
        this.getInfo().getOutVariables().add(errorField);


    }

/*
    public String getBusinessRuleId() {
        return businessRuleId;
    }

    public void setBusinessRuleId(String businessRuleId) {
        this.businessRuleId = businessRuleId;
    }
*/


    private IPluginInfo pluginInfo=new com.lava.br.core.plugins.PluginInfo();

    private List<IPlugin> pluginsRegisterd = new ArrayList<IPlugin>();


    public IPluginInfo getInfo() {
        return pluginInfo;
    }

    public void setInfo(IPluginInfo info) {
        pluginInfo = info;
    }

    @Override
    public void consume(IPlugin plugin) {

    }

    public void unConsume(IPlugin plugin) {

    }

    public void produce(IPlugin plugin) {

    }

    public void register(IPlugin plugin) {
        pluginsRegisterd.add(plugin);
        consume(plugin);
    }

    public void unRegister(IPlugin plugin) {
        unConsume(plugin);
        pluginsRegisterd.remove(plugin);
        plugin=null;
    }

    public List<IPlugin> getPluginsRegisterd() {

        return pluginsRegisterd;
    }

    public void setPluginsRegisterd(List<IPlugin> pluginsRegisterd  ) {
        this.pluginsRegisterd=pluginsRegisterd;
    }

    @Override
    public IPlugin getInstance() throws InstantiationException, IllegalAccessException {
        IPlugin instance = this.getClass().newInstance();
        PluginMapper.pluginInfoCloneTo(this,instance);
        return instance;
    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return null;
    }

    public void addAction(String actionName,String actionLabel){
        SimpleSelectField simpleSelectField= (SimpleSelectField)this.getInfo().getConfigVariables().stream().filter(p->p.getName().equalsIgnoreCase("producerAction"))
                .findFirst().get();

        simpleSelectField.getSelectFields().put(actionName,actionLabel);
    }

    public void removeAction(String actionName){
        SimpleSelectField simpleSelectField= (SimpleSelectField)this.getInfo().getConfigVariables().stream().filter(p->p.getName().equalsIgnoreCase("producerAction"))
                .findFirst().get();

        simpleSelectField.getSelectFields().remove(actionName);
    }

    public void addTrigger(String triggerName,String triggerabel){
        SimpleSelectField simpleSelectField= (SimpleSelectField)this.getInfo().getConfigVariables().stream().filter(p->p.getName().equalsIgnoreCase("consumerTrigger"))
                .findFirst().get();

        simpleSelectField.getSelectFields().put(triggerName,triggerabel);
    }

    public void removeTrigger(String triggerName){
        SimpleSelectField simpleSelectField= (SimpleSelectField)this.getInfo().getConfigVariables().stream().filter(p->p.getName().equalsIgnoreCase("consumerTrigger"))
                .findFirst().get();

        simpleSelectField.getSelectFields().remove(triggerName);
    }
}
