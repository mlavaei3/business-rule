package com.lava.br.core.restclients;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.repositories.bean.MessageSocket;
import com.lava.br.core.restTemplates.KeycloakClientCredentialsRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

public abstract class PluginRestTemplateClient {

    @Autowired
    @Qualifier("keycloakClientCredentialsRestTemplate")
    KeycloakClientCredentialsRestTemplate keycloakClientCredentialsRestTemplate;


    private static final Logger logger = LoggerFactory.getLogger(PluginRestTemplateClient.class);

    public String fireRules(IPlugin plugin) {
        logger.debug("In Register Plugin Info: {}", plugin);
        PluginInfoClient pluginInfoClient=PluginMapper.toPluginInfoClient(plugin);
        HttpEntity<PluginInfoClient> requestEntity = new HttpEntity<PluginInfoClient>(pluginInfoClient);
        ResponseEntity<String> restExchange =
                keycloakClientCredentialsRestTemplate.exchange(
                        "http://zuulservice/api/rules/v1/fireRules",
                        HttpMethod.POST,
                        requestEntity, String.class);

        /*Save the record from cache*/
        String org = restExchange.getBody();
        return org;
    }


    public Boolean sendToSocket(String sessionId,String objectId,String message) {
        logger.debug("Send To Socket Params: {0},{1},{2}", sessionId,objectId,message);
        MessageSocket messageSocket=new MessageSocket();
                // MessageSocket.builder()
        messageSocket.setMessage(message);
        messageSocket.setSessionId(sessionId);
        messageSocket.setObjectId(objectId);
        //messageSocket.build();

        HttpEntity<MessageSocket> requestEntity = new HttpEntity<>(messageSocket);
        ResponseEntity<Boolean> restExchange =
                keycloakClientCredentialsRestTemplate.exchange(
                        "http://zuulservice/api/rules/v1/sendToSocket",
                        HttpMethod.POST,
                        requestEntity, Boolean.class);

        /*Save the record from cache*/
        Boolean result = restExchange.getBody();
        return result;
    }
}
