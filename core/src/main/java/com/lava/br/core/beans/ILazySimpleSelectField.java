package com.lava.br.core.beans;

import java.util.Map;
import java.util.function.Function;

public interface ILazySimpleSelectField {
    Function<Map<String, Object>,Map<String, String>> getSelectFields();
    void setSelectFields(Function<Map<String, Object>,Map<String, String>> selectFields);
}
