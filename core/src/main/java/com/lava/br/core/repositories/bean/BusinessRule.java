package com.lava.br.core.repositories.bean;

import com.lava.br.core.beans.IBusinessRule;
import com.lava.br.core.beans.IFieldFilter;
import com.lava.br.core.beans.IRuleProducer;
import com.lava.br.core.enums.BusinessRuleStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "businessRules")
@TypeAlias("businessRule")
@Getter
@Setter
public class BusinessRule implements IBusinessRule {

	@Id
	private String id;

	private BusinessRuleStatus status;

	private String username;
	
	private String name;

	private String artifactId;

	private String versionId;

	private String consumerType;

	private List<IRuleProducer> ruleProducers= new ArrayList<>();

	private List<IFieldFilter> consumerFilters= new ArrayList<>();

	private List<FieldAssigner> consumerConfig= new ArrayList<>();
	
	private String consumerFiltersExpression;

}
