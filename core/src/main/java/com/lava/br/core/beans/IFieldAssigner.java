package com.lava.br.core.beans;

public interface IFieldAssigner {
    String getFieldName();

    void setFieldName(String fieldName);

    Object getValue();

    void setValue(Object value);
}
