package com.lava.br.core.beans;

import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.repositories.bean.BusinessRule;
import com.lava.br.core.repositories.bean.DbStoreObject;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface IDbStoreObjectRepository extends MongoRepository<DbStoreObject, String> {

    @Query("{ 'username' : ?0 , 'name' : ?1 , 'dbStoreType': ?2 }")
    List<DbStoreObject> findByUsernameAndNameAndDbStoreType(String username, String name, DbStoreType dbStoreType);

}
