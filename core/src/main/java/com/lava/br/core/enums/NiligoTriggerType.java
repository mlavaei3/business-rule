package com.lava.br.core.enums;

public enum NiligoTriggerType {
    SWITCH_LEFT,
    SWITCH_RIGHT,
    IRIS_MOTION,
    IRIS_NO_MOTION,
    IRIS_LUXMETER,
    RELAY_ON,
    RELAY_OFF,
    SENSOR_TEMPERATURE,
    SENSOR_HUMIDITY,
    GPIOT_EDGE_DOWN,
    GPIOT_EDGE_UP,
    GPIOT_LEVEL_UP,
    GPIOT_LEVEL_DOWN

}
