package com.lava.br.core.plugins.utils.niligo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.enums.NiligoTriggerType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class M2MPayload {
    private String topic;
    private String message;
    private String uuid;
    private List<String> m2mUsers;
    private List<String> integrationUsers;
    NiligoTriggerType triggerType;
    SwitchTouchType switchTouchType;

    public String getPortFromRelay() throws IOException {
        //if(Pattern.matches("/get/[0-9a-zA-Z-]{36}/relay/state/",topic)){
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(message);
        return node.path("port").asText();
        //}else{
        //    return "";
        //}
    }

/*    public String getEventForLongClick() throws IOException {
        if(Pattern.matches("/get/[0-9a-zA-Z-]{36}/switch/event/[0-1]{1}/([0-9]|1[0-6])",topic)){
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(message);
            return node.path("event").asText();
        }else{
            return "";
        }
    }*/


    /*public NiligoTriggerType getType()
    {
        try {
            if (Pattern.compile("/uuid/[0-9a-zA-Z-]{36}/sensor/motion").matcher(topic).find()) {
                //this is motion topic case...
                //message format =  {"last-event": 1559806094}
                return NiligoTriggerType.IRIS_MOTION;
            } else if (Pattern.compile("/uuid/[0-9a-zA-Z-]{36}/sensor/light").matcher(topic).find()) {
                //this is lux-meter topic case...
                //message format = {"time": 1559806134, "light": 2.339}
                return NiligoTriggerType.IRIS_LUXMETER;
            }
            // For Switch
            else if (Pattern.matches("/uuid/[0-9a-zA-Z-]{36}/switch/state/0", topic)) {
                //this is lux-meter topic case...
                //message format = {"time": 1559806134, "light": 2.339}
                return NiligoTriggerType.SWITCH_LEFT_CLICK_ON;
            } else if (Pattern.matches("/uuid/[0-9a-zA-Z-]{36}/switch/state/1", topic)) {
                //this is lux-meter topic case...
                //message format = {"time": 1559806134, "light": 2.339}
                return NiligoTriggerType.SWITCH_RIGHT_CLICK_ON;
            } else if (Pattern.matches("/uuid/[0-9a-zA-Z-]{36}/switch/event", topic)) {
                //this is lux-meter topic case...
                //message format = {"time": 1559806134, "light": 2.339}
                return NiligoTriggerType.SWITCH_MULTIPLE_CLICK;
            }

        }catch (Exception ex){

        }
        return null;
    }

    class SingleClickPayload{
        Setting setting;
        int port;
        class Setting{
            int power;
        }
    }
    class MultiClickPayload{
        Long time;
        int event;
        String description;
    }*/

}