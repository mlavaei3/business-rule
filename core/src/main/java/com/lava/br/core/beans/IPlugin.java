package com.lava.br.core.beans;

import com.lava.br.core.services.PluginService;
import org.apache.camel.CamelContext;

import java.util.List;

public interface IPlugin {

	IPluginInfo getInfo();

	void setInfo(IPluginInfo info);

	void consume(IPlugin plugin);

	void unConsume(IPlugin plugin);

	void produce(IPlugin plugin);

	void register(IPlugin plugin);

	void unRegister(IPlugin plugin);

	List<IPlugin> getPluginsRegisterd();

	IPlugin getInstance() throws InstantiationException, IllegalAccessException;

	String getUniqueIdentity(IPlugin plugin);
}
