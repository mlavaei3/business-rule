package com.lava.br.core.beans;


import com.lava.br.core.enums.TypeField;

import java.io.Serializable;
import java.util.List;

public interface IField extends Serializable {

	int getOrder();

	void setOrder(int order);

	Boolean getVisibility();

	void setVisibility(Boolean visibility);

	String getLabel();

	void setLabel(String label);

	String getName();

	void setName(String name);

	Class<?> getFieldClass();

	void setFieldClass(Class<?> fieldClass);

	Object getValue();

	void setValue(Object value);
	
	TypeField getType();
	
	void setType(TypeField type);

	String getHelpText();

	void setHelpText(String helpText);

	void  setReadonly(Boolean readonly);

	Boolean  getReadonly();

	void  setUnique(Boolean unique);

	Boolean  getUnique();

	List<String> getRefTriggers();

	void  setRefTriggers(List<String> refTriggers);

	List<String> getRefActions();

	void  setRefActions(List<String> refActions);

	List<IFieldValidator> getFieldValidators();

	void  setFieldValidators(List<IFieldValidator> fieldValidators);

}