package com.lava.br.plugins.switchNiligo.services;

import org.springframework.stereotype.Service;

@Service("pluginRegisteredRepositoryService")
public class PluginRegisteredRepositoryService extends com.lava.br.core.repositories.PluginRegisteredRepositoryService {
}
