package com.lava.br.plugins.switchNiligo.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.switchNiligo.services.SwitchNiligoService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("switchNiligo")
@Pluggable(nameOfPlugin = "switchNiligo")
public class SwitchNiligo extends BasePluginWithCamel {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);
        this.getInfo().setIsProducer(true);

        // configs
        /**
         * account
         */
        PartyField partyField = PartyField
                .builder()
                .name("account")
                .label("account")
                .type(TypeField.PartySelectList)
                .visibility(true)
                .order(1)
                .build();
        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        partyField.setPartyUrl(tokenUri + "/oauth/authorize?response_type=code&scopes=read&client_id="+userPass.split(":")[0]+"&redirect_uri=" + UrlUtils.urlEncode(redirectUri));
        this.getInfo().getConfigVariables().add(partyField);
        /**
         * switchNiligoList
         */
        LazySelectField switchNiligoListField = LazySelectField.builder()
                .fieldClass(LazySelectField.class)
                .name("switchNiligoList")
                .label("switchNiligoList")
                .type(TypeField.LazySelectList)
                .visibility(true)
                .order(2)
                .build();

        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReset=FieldValidator.builder()
                .type(ValidatorType.DeptReset)
                .expression("account")
                .build();
        fieldValidators.add(fvReset);
        switchNiligoListField.setFieldValidators(fieldValidators);
        switchNiligoListField.setSelectFields(this::getSwitchNiligos);
        switchNiligoListField.setRequestUrl("?query=${account}");
        this.getInfo().getConfigVariables().add(switchNiligoListField);

        /**
         * side
         */
        SimpleSelectField side = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("side")
                .label("side")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(3)
                .build();
        Map<Object, String> map = new HashMap<>();
        map.put("SWITCH_LEFT", "SWITCH LEFT");
        map.put("SWITCH_RIGHT", "SWITCH RIGHT");

        side.setSelectFields(map);
        this.getInfo().getConfigVariables().add(side);

        /**
         * eventNumber
         */
        SimpleSelectField eventNumber = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("eventNumber")
                .label("eventNumber")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(3)
                .build();
        map = new LinkedHashMap<>();
        map.put("Long1", "1 Vibration");
        map.put("Long2", "2 Vibration");
        map.put("Long3", "3 Vibration");
        map.put("Long4", "4 Vibration");
        map.put("Long5", "5 Vibration");
        map.put("Long6", "6 Vibration");
        map.put("Long7", "7 Vibration");
        map.put("Long8", "8 Vibration");
        map.put("Long9", "9 Vibration");
        map.put("Long10", "10 Vibration");
        map.put("Double", "Double Clicked");
        map.put("Third", "Triple Clicked");
        map.put("Forth", "Quadruple Clicked");
        map.put("Fifth", "Five Times Clicked");
        map.put("Sixth", "Six Times Clicked");
        map.put("Seventh", "Seven Times Clicked");
        map.put("On", "Power On");
        map.put("Off", "Power Off");

        eventNumber.setSelectFields(map);
        this.getInfo().getConfigVariables().add(eventNumber);

    }

    @Value("${lava.plugins.switchNiligo.api_uri}")
    private String apiUri;

    @Value("${lava.plugins.switchNiligo.m2m_uri}")
    private String m2mUuri;

    @Value("${lava.plugins.switchNiligo.auth2.redirect_uri}")
    private String redirectUri;

    @Value("${lava.plugins.switchNiligo.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.switchNiligo.auth2.user_pass}")
    private String userPass;

    public Map<String, String> getAccounts(Map<String, Object> objectMap) {

        SwitchNiligoService switchNiligoService = ApplicationContextProvider.getApplicationContext().getBean("switchNiligoService", SwitchNiligoService.class);
        Map<String, String> accounts = switchNiligoService.getAllValueForKey(objectMap.get("userId").toString(), DbStoreType.ACCOUNTING,"account", "token");
        accounts.forEach((k, v) -> {
            accounts.put(k, k);
        });
        return accounts;
    }

    public Map<String, String> getSwitchNiligos(Map<String, Object> objectMap) {
        Map<String, String> map = new HashMap<>();
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            SwitchNiligoService switchNiligoService = (SwitchNiligoService) applicationContext.getBean("switchNiligoService");
            String token = switchNiligoService.getValueForKey(objectMap.get("userId").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    objectMap.get("query").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);
            HttpEntity<String> request1 = new HttpEntity<>(headers1);

            String resourceUrl = apiUri + "/api/thing/list?type=switch";
            ResponseEntity<ThingDto[]> rsp1 = restTemplate.exchange(resourceUrl, HttpMethod.GET, request1, ThingDto[].class);

            for (ThingDto thingDto : rsp1.getBody()) {
                map.put(thingDto.getUuid(), thingDto.getName());
            }
            return map;
        } catch (IOException e) {
            return map;
        }
    }

//    public List<String> registerSwitchNiligo = new ArrayList<>();
//
//    public List<String> getRegisterSwitchNiligo() {
//        return registerSwitchNiligo;
//    }

    @Override
    public void consume(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            SwitchNiligoService switchNiligoService = (SwitchNiligoService) applicationContext.getBean("switchNiligoService");
            String token = switchNiligoService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);


            String actionType = plugin.getInfo().getValue(PluginInfo.CONFIG, "producerAction").toString();
            String resourceUrl = m2mUuri + "/m2m/integration/user/add";
            Map<String, String> map = new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid", plugin.getInfo().getValue(PluginInfo.CONFIG, "switchNiligoList").toString());
            map.put("trigger", plugin.getInfo().getValue(PluginInfo.CONFIG, "side").toString());
            map.put("switchTouchType", plugin.getInfo().getValue(PluginInfo.CONFIG, "eventNumber").toString());

            HttpEntity<Map<String, String>> request1 = new HttpEntity<>(map, headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            if (rsp1.getStatusCodeValue() == 200 || rsp1.getStatusCodeValue() == 201) {
//                registerSwitchNiligo.add(map.get("uuid")+ map.get("trigger")+
//                        (map.get("trigger").equalsIgnoreCase("SWITCH_MULTIPLE_CLICK")?map.get("switchVibrationCount").trim():""));
            }
            System.out.println(rsp1.getBody());
        } catch(
                IOException e)

        {
            e.printStackTrace();
        }
    }

    @Override
    public void unConsume(IPlugin plugin) {
        try {
            final Map<String,String> map=new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid", plugin.getInfo().getValue(PluginInfo.CONFIG, "switchNiligoList").toString());
            map.put("trigger", plugin.getInfo().getValue(PluginInfo.CONFIG, "side").toString());
            map.put("switchVibrationCount", plugin.getInfo().getValue(PluginInfo.CONFIG, "eventNumber").toString());

            //            registerSwitchNiligo.removeIf(u-> u.equalsIgnoreCase(map.get("uuid")+
//                    map.get("trigger")+
//                    (map.get("trigger").equalsIgnoreCase("SWITCH_MULTIPLE_CLICK")?map.get("switchVibrationCount").trim():""))
//            );

            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            SwitchNiligoService switchNiligoService = (SwitchNiligoService) applicationContext.getBean("switchNiligoService");
            String token = switchNiligoService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);

            String resourceUrl = m2mUuri + "/m2m/integration/user/delete";

            HttpEntity<Map<String, String>> request1 = new HttpEntity<>(map, headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
/*            if (rsp1.getStatusCodeValue() == 200 || rsp1.getStatusCodeValue() == 201) {
                registerSwitchNiligo.add(map.get("account")+map.get("switchNiligoList"));
            }*/
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void produce(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            SwitchNiligoService  prismService= (SwitchNiligoService) applicationContext.getBean("switchNiligoService");
            String token= prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.OUT,"userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG,"account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);


            String actionType=plugin.getInfo().getValue(PluginInfo.CONFIG,"producerAction").toString();
            String resourceUrl=m2mUuri+"/m2m/switch/event/";
            Map<String,String> map=new HashMap<>();
            map.put("uuid",plugin.getInfo().getValue(PluginInfo.CONFIG,"switchNiligoList").toString());
            map.put("direction",plugin.getInfo().getValue(PluginInfo.CONFIG,"side").toString());
            map.put("type",plugin.getInfo().getValue(PluginInfo.CONFIG,"eventNumber").toString());


            HttpEntity<Map<String,String>> request1=new HttpEntity<>(map,headers1);
            System.out.println(request1.getBody());
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            System.out.println(rsp1.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        String side=plugin.getInfo().getValue(PluginInfo.CONFIG, "side").toString().trim();
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString().trim()
                        +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "switchNiligoList").toString().trim()+
                        "-"+side+
                         //((side.equalsIgnoreCase("SWITCH_LEFT")||(side.equalsIgnoreCase("SWITCH_RIGHT"))?
                        "-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "eventNumber").toString().trim();

    }
}