package com.lava.br.plugins.switchNiligo.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.switchNiligo.impl.SwitchNiligo;
import com.lava.br.plugins.switchNiligo.restclients.SwitchNiligoRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SwitchNiligoService extends PluginService {


    @Autowired
    private SwitchNiligo switchNiligo;

    @Autowired
    private SwitchNiligoRestTemplateClient switchNiligoRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return switchNiligo;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return switchNiligoRestTemplateClient;
    }


  /*  public void addAccountWithToken(String userId,String account,String token){
        try {
            DbStore dbStoreClient=(DbStore)dbStore.getInstance();
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"userid","mehdi");
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object","[{\"account\":\"mlavaei3\",\"token\":\"token1\"},{\"account\":\"mlavaei4\",\"token\":\"token4\"}]");
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"name","gmail");
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"dbStoreType",DbStoreType.ACCOUNTING);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"action","save");
            //dbStore.produce(dbStoreClient);

            DbStore dbStoreClient2=(DbStore)dbStore.getInstance();
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"userid","mehdi");
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"name","gmail");
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"dbStoreType",DbStoreType.ACCOUNTING);
            dbStoreClient2.getInfo().setValue(PluginInfo.OUT,"action","load");
            dbStore.produce(dbStoreClient2);
            String res=(String)dbStoreClient2.getInfo().getValue(PluginInfo.OUT,"result");
            final JSONArray obj = new JSONArray(res);
            final JSONObject geodata = obj.getJSONObject(0);
            res=geodata.getString("account");
            Map<String,String> mp=new HashMap<>();
            mp.put("account","acc1");
            mp.put("token","tt1");
            obj.put(2,mp);
            dbStoreClient.getInfo().setValue(PluginInfo.OUT,"object",obj.toString());
            dbStore.produce(dbStoreClient);


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
*/
}
