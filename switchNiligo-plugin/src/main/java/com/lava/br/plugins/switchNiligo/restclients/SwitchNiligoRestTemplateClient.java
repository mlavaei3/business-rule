package com.lava.br.plugins.switchNiligo.restclients;

import com.lava.br.core.restclients.PluginRestTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

//import org.springframework.security.oauth2.client.OAuth2RestTemplate;

@Component
public class SwitchNiligoRestTemplateClient extends PluginRestTemplateClient {
    private static final Logger logger = LoggerFactory.getLogger(SwitchNiligoRestTemplateClient.class);
}
