package com.lava.br.plugins.iris.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.iris.impl.Iris;
import com.lava.br.plugins.iris.restclients.IrisRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IrisService extends PluginService {


    @Autowired
    private Iris iris;

    @Autowired
    private IrisRestTemplateClient irisRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return iris;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return irisRestTemplateClient;
    }



}
