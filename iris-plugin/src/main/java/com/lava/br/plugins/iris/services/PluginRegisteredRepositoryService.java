package com.lava.br.plugins.iris.services;

import org.springframework.stereotype.Service;

@Service("pluginRegisteredRepositoryService")
public class PluginRegisteredRepositoryService extends com.lava.br.core.repositories.PluginRegisteredRepositoryService {
}
