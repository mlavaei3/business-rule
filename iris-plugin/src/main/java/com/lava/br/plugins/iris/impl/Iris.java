package com.lava.br.plugins.iris.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.iris.services.IrisService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("iris")
@Pluggable(nameOfPlugin = "iris")
public class Iris extends BasePlugin {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);

        // configs
        this.removeTrigger("default");
        this.addTrigger("MotionTrigger","Motion Trigger");
        this.addTrigger("NoMotionTrigger","No Motion Detected Trigger");
        this.addTrigger("LuxMeterTrigger","Lux Meter Trigger");

        /**
         * account
         */
        PartyField partyField=PartyField
                .builder()
                .name("account")
                .label("account")
                .type(TypeField.PartySelectList)
                .visibility(true)
                .order(1)
                .build();
        List<String> listTrigger=new ArrayList<>();
        listTrigger.add("MotionTrigger");
        listTrigger.add("NoMotionTrigger");
        listTrigger.add("LuxMeterTrigger");
        partyField.setRefTriggers(listTrigger);

        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        partyField.setPartyUrl(tokenUri+"/oauth/authorize?response_type=code&scopes=read&client_id="+userPass.split(":")[0]+"&redirect_uri="+ UrlUtils.urlEncode(redirectUri));
        this.getInfo().getConfigVariables().add(partyField);
        /**
         * irisList
         */
        LazySelectField irisListField=LazySelectField.builder()
                .fieldClass(LazySelectField.class)
                .name("irisList")
                .label("irisList")
                .type(TypeField.LazySelectList)
                .visibility(true)
                .order(2)
                .build();

        irisListField.setSelectFields(this::getIriss);
        irisListField.setRequestUrl("?query=${account}");

        listTrigger=new ArrayList<>();
        listTrigger.add("MotionTrigger");
        listTrigger.add("NoMotionTrigger");
        listTrigger.add("LuxMeterTrigger");
        irisListField.setRefTriggers(listTrigger);

        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReset=FieldValidator.builder()
                .type(ValidatorType.DeptReset)
                .expression("account")
                .build();
        fieldValidators.add(fvReset);


        irisListField.setFieldValidators(fieldValidators);

        this.getInfo().getConfigVariables().add(irisListField);


        // inVar
        Field field=new Field();
        field.setName("light");
        field.setLabel("light");
        field.setType(TypeField.Number);
        field.setFieldClass(java.lang.Long.class);
        listTrigger=new ArrayList<>();
        listTrigger.add("LuxMeterTrigger");
        field.setRefTriggers(listTrigger);
        field.setOrder(3);
        this.getInfo().getInVariables().add(field);//Field.builder().name("from").fieldClass( String.class).label("from").order(1).build());


    }

    @Value("${lava.plugins.iris.api_uri}")
    private String apiUri;

    @Value("${lava.plugins.iris.m2m_uri}")
    private String m2mUuri;

    @Value("${lava.plugins.iris.auth2.redirect_uri}")
    private String redirectUri;

    @Value("${lava.plugins.iris.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.iris.auth2.user_pass}")
    private String userPass;

    public Map<String, String> getAccounts(Map<String, Object> objectMap){

        IrisService irisService= ApplicationContextProvider.getApplicationContext().getBean("irisService",IrisService.class);
        Map<String,String> accounts=irisService.getAllValueForKey(objectMap.get("userId").toString(),DbStoreType.ACCOUNTING,"account","token");
        accounts.forEach((k,v)->{accounts.put(k,k);});
        return accounts;
    }

    public Map<String, String> getIriss(Map<String, Object> objectMap){
        Map<String,String> map=new HashMap<>();
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            IrisService  irisService= (IrisService) applicationContext.getBean("irisService");
            String token= irisService.getValueForKey(objectMap.get("userId").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    objectMap.get("query").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);
            HttpEntity<String> request1=new HttpEntity<>(headers1);

            String resourceUrl=apiUri+"/api/thing/list?type=iris";
            ResponseEntity<ThingDto[]> rsp1 = restTemplate.exchange(resourceUrl, HttpMethod.GET, request1, ThingDto[].class);

            for (ThingDto thingDto: rsp1.getBody()){
                map.put(thingDto.getUuid(),thingDto.getName());
            }
            return map;
        } catch (IOException e) {
            return map;
        }
    }

//    public List<String> registerIris=new ArrayList<>();
//
//    public List<String> getRegisterIris() {
//        return registerIris;
//    }


    @Override
    public void consume(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            IrisService  prismService= (IrisService) applicationContext.getBean("irisService");
            String token= prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG,"userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG,"account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);


            String actionType=plugin.getInfo().getValue(PluginInfo.CONFIG,"producerAction").toString();
            String resourceUrl=m2mUuri+"/m2m/integration/user/add";
            Map<String,String> map=new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid",plugin.getInfo().getValue(PluginInfo.CONFIG,"irisList").toString());


            String trigger="";
            if(plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("MotionTrigger"))
                trigger="IRIS_MOTION";
            else if(plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("NoMotionTrigger"))
                trigger="IRIS_NO_MOTION";
            else if(plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("LuxMeterTrigger"))
                trigger="IRIS_LUXMETER";

            map.put("trigger", trigger);

            HttpEntity<Map<String,String>> request1=new HttpEntity<>(map,headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            if (rsp1.getStatusCodeValue()==200 || rsp1.getStatusCodeValue()==201){
//                if(registerIris.contains(map.get("uuid"))==false){
//                    registerIris.add(map.get("uuid"));
//                }
            }

            System.out.println(rsp1.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void unConsume(IPlugin plugin) {
        try {
            final Map<String, String> map = new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid", plugin.getInfo().getValue(PluginInfo.CONFIG, "irisList").toString());
            String trigger="";
            if(plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("MotionTrigger"))
                trigger="IRIS_MOTION";
            else if(plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("NoMotionTrigger"))
                trigger="IRIS_NO_MOTION";
            else if(plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("LuxMeterTrigger"))
                trigger="IRIS_LUXMETER";
            map.put("trigger",trigger);
            //registerIris.removeIf(u -> u.equalsIgnoreCase(map.get("irisList")));

            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            IrisService prismService = (IrisService) applicationContext.getBean("irisService");
            String token = prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);


            String actionType = plugin.getInfo().getValue(PluginInfo.CONFIG, "producerAction").toString();
            String resourceUrl = m2mUuri + "/m2m/integration/user/delete";
            HttpEntity<Map<String, String>> request1 = new HttpEntity<>(map, headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            if (rsp1.getStatusCodeValue() == 200 || rsp1.getStatusCodeValue() == 201) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void produce(IPlugin plugin) {

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "irisList").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().trim();
   }
}