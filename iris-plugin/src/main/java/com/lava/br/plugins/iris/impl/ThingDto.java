package com.lava.br.plugins.iris.impl;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ThingDto {

    private long id;
    private String name;
    private String uuid;
    private String password;
    private String type;
    private String owner;
    private boolean state;

}