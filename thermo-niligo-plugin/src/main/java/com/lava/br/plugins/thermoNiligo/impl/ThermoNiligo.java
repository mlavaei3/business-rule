package com.lava.br.plugins.thermoNiligo.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.thermoNiligo.services.ThermoNiligoService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("thermoNiligo")
@Pluggable(nameOfPlugin = "thermoNiligo")
public class ThermoNiligo extends BasePlugin {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);

        // configs
        this.removeTrigger("default");
        this.addTrigger("ThermoTrigger","Thermometer Trigger");
        this.addTrigger("HumidityTrigger","Humidity Trigger");

        /**
         * account
         */
        PartyField partyField=PartyField
                .builder()
                .name("account")
                .label("account")
                .type(TypeField.PartySelectList)
                .visibility(true)
                .order(1)
                .build();
        List<String> listTrigger=new ArrayList<>();
        listTrigger.add("ThermoTrigger");
        listTrigger.add("HumidityTrigger");
        partyField.setRefTriggers(listTrigger);

        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        partyField.setPartyUrl(tokenUri+"/oauth/authorize?response_type=code&scopes=read&client_id="+userPass.split(":")[0]+"&redirect_uri="+ UrlUtils.urlEncode(redirectUri));
        this.getInfo().getConfigVariables().add(partyField);
        /**
         * thermoNiligoList
         */
        LazySelectField thermoNiligoListField=LazySelectField.builder()
                .fieldClass(LazySelectField.class)
                .name("thermoNiligoList")
                .label("thermoNiligoList")
                .type(TypeField.LazySelectList)
                .visibility(true)
                .order(2)
                .build();

        thermoNiligoListField.setSelectFields(this::getThermoNiligos);
        thermoNiligoListField.setRequestUrl("?query=${account}");

        listTrigger=new ArrayList<>();
        listTrigger.add("ThermoTrigger");
        listTrigger.add("HumidityTrigger");
        thermoNiligoListField.setRefTriggers(listTrigger);

        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReset=FieldValidator.builder()
                .type(ValidatorType.DeptReset)
                .expression("account")
                .build();
        fieldValidators.add(fvReset);


        thermoNiligoListField.setFieldValidators(fieldValidators);

        this.getInfo().getConfigVariables().add(thermoNiligoListField);


        // inVar
        Field field=new Field();
        field.setName("temperature");
        field.setLabel("Temperature");
        field.setType(TypeField.Number);
        field.setFieldClass(Long.class);
        listTrigger=new ArrayList<>();
        listTrigger.add("ThermoTrigger");
        field.setRefTriggers(listTrigger);
        field.setOrder(3);
        this.getInfo().getInVariables().add(field);//Field.builder().name("from").fieldClass( String.class).label("from").order(1).build());

        field=new Field();
        field.setName("humidity");
        field.setLabel("humidity");
        field.setType(TypeField.Number);
        field.setFieldClass(Long.class);
        listTrigger=new ArrayList<>();
        listTrigger.add("HumidityTrigger");
        field.setRefTriggers(listTrigger);
        field.setOrder(4);
        this.getInfo().getInVariables().add(field);//Field.builder().name("from").fieldClass( String.class).label("from").order(1).build());

    }

    @Value("${lava.plugins.thermoNiligo.api_uri}")
    private String apiUri;

    @Value("${lava.plugins.thermoNiligo.m2m_uri}")
    private String m2mUuri;

    @Value("${lava.plugins.thermoNiligo.auth2.redirect_uri}")
    private String redirectUri;

    @Value("${lava.plugins.thermoNiligo.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.thermoNiligo.auth2.user_pass}")
    private String userPass;

    public Map<String, String> getAccounts(Map<String, Object> objectMap){

        ThermoNiligoService thermoNiligoService= ApplicationContextProvider.getApplicationContext().getBean("thermoNiligoService",ThermoNiligoService.class);
        Map<String,String> accounts=thermoNiligoService.getAllValueForKey(objectMap.get("userId").toString(),DbStoreType.ACCOUNTING,"account","token");
        accounts.forEach((k,v)->{accounts.put(k,k);});
        return accounts;
    }

    public Map<String, String> getThermoNiligos(Map<String, Object> objectMap){
        Map<String,String> map=new HashMap<>();
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            ThermoNiligoService  thermoNiligoService= (ThermoNiligoService) applicationContext.getBean("thermoNiligoService");
            String token= thermoNiligoService.getValueForKey(objectMap.get("userId").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    objectMap.get("query").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);
            HttpEntity<String> request1=new HttpEntity<>(headers1);

            String resourceUrl=apiUri+"/api/thing/list?type=thermo";
            ResponseEntity<ThingDto[]> rsp1 = restTemplate.exchange(resourceUrl, HttpMethod.GET, request1, ThingDto[].class);

            for (ThingDto thingDto: rsp1.getBody()){
                map.put(thingDto.getUuid(),thingDto.getName());
            }
            return map;
        } catch (IOException e) {
            return map;
        }
    }

//    public List<String> registerThermoNiligo=new ArrayList<>();
//
//    public List<String> getRegisterThermoNiligo() {
//        return registerThermoNiligo;
//    }


    @Override
    public void consume(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            ThermoNiligoService  prismService= (ThermoNiligoService) applicationContext.getBean("thermoNiligoService");
            String token= prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG,"userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG,"account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);


            String actionType=plugin.getInfo().getValue(PluginInfo.CONFIG,"producerAction").toString();
            String resourceUrl=m2mUuri+"/m2m/integration/user/add";
            Map<String,String> map=new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid",plugin.getInfo().getValue(PluginInfo.CONFIG,"thermoNiligoList").toString());
            map.put("trigger", plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("ThermoTrigger")?"SENSOR_TEMPERATURE":"SENSOR_HUMIDITY");

            HttpEntity<Map<String,String>> request1=new HttpEntity<>(map,headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            System.out.println(rsp1.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void unConsume(IPlugin plugin) {
        try {
            final Map<String, String> map = new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid", plugin.getInfo().getValue(PluginInfo.CONFIG, "thermoNiligoList").toString());
            map.put("trigger",plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().equalsIgnoreCase("ThermoTrigger")?"SENSOR_TEMPERATURE":"SENSOR_HUMIDITY");
            //registerThermoNiligo.removeIf(u -> u.equalsIgnoreCase(map.get("thermoNiligoList")));

            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            ThermoNiligoService prismService = (ThermoNiligoService) applicationContext.getBean("thermoNiligoService");
            String token = prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);


            String actionType = plugin.getInfo().getValue(PluginInfo.CONFIG, "producerAction").toString();
            String resourceUrl = m2mUuri + "/m2m/integration/user/delete";
            HttpEntity<Map<String, String>> request1 = new HttpEntity<>(map, headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            if (rsp1.getStatusCodeValue() == 200 || rsp1.getStatusCodeValue() == 201) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void produce(IPlugin plugin) {

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "thermoNiligoList").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "consumerTrigger").toString().trim();
   }
}