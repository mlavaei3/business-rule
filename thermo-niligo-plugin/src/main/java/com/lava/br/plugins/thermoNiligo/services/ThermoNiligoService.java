package com.lava.br.plugins.thermoNiligo.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.thermoNiligo.impl.ThermoNiligo;
import com.lava.br.plugins.thermoNiligo.restclients.ThermoNiligoRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ThermoNiligoService extends PluginService {


    @Autowired
    private ThermoNiligo thermoNiligo;

    @Autowired
    private ThermoNiligoRestTemplateClient thermoNiligoRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return thermoNiligo;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return thermoNiligoRestTemplateClient;
    }



}
