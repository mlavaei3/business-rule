package com.lava.br.plugins.javascript.impl;

/**
 * Created by Aria on 05/06/2018.
 */

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IField;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.scripting.Scripting;
import com.lava.br.core.plugins.scripting.ScriptingManager;
import com.lava.br.rulessvr.services.BusinessRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.*;
import java.util.HashMap;
import java.util.Map;

@Service("javascript")
@Pluggable(nameOfPlugin = "javascript")
public class JavaScript extends Scripting {
    @Autowired
    BusinessRuleService businessRuleService;

    @Override
    public void produce(IPlugin iPlugin) {
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        Bindings bnd = engine.createBindings();

        PluginInfoClient  pluginInfoClient=(PluginInfoClient) iPlugin.getInfo().getValue(PluginInfo.OUT, "input");
        Map<String,Object> inputVars=new HashMap<>();
        pluginInfoClient.getInVariables().stream().forEach(field->{
            inputVars.put(field.getName(),field.getValue());
        });

        BasePlugin iPluginConsumer=new BasePlugin();
        PluginMapper.pluginInfoCloneTo((PluginInfoClient) iPlugin.getInfo().getValue(PluginInfo.OUT, "input"),iPluginConsumer);


        bnd.put("manager", new ScriptingManager(iPlugin.getInfo().getValue(PluginInfo.OUT,"userid").toString()
                ,businessRuleService,iPluginConsumer));

        //bnd.put("manager.consumer_vars", inputVars);

        engine.setBindings(bnd, ScriptContext.ENGINE_SCOPE);
        try {
            Object result = engine.eval((String) iPlugin.getInfo().getValue("OUT", "script"));
            this.getInfo().setValue(PluginInfo.IN, "result",result);
        } catch (ScriptException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
