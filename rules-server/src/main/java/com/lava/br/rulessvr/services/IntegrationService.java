package com.lava.br.rulessvr.services;


import com.lava.br.core.clients.BusinessRuleClient;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.repositories.bean.BusinessRule;
import com.lava.br.core.repositories.bean.PluginRegistered;
import org.springframework.messaging.Message;

public interface IntegrationService {
    Message<PluginInfoClient> saveBusinessRuleIntegration(Message<BusinessRuleClient> businessRuleClient);
    Message<PluginInfoClient> changeStatusBusinessRuleIntegration(Message<BusinessRuleClient> businessRuleClient);
    //Message<Boolean> startBusinessRuleIntegration( Message<PluginInfoClient>  businessRuleClient);
}
