package com.lava.br.rulessvr.drools;

/**
 * Created by Aria on 02/05/2018.
 */


import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.beans.IField;
import com.lava.br.core.beans.IFieldAssigner;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.beans.IRuleProducer;
import com.lava.br.core.clients.BusinessRuleClient;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.enums.BusinessRuleStatus;
import com.lava.br.core.enums.CommunicationType;
import com.lava.br.core.mappers.BusinessRuleMapper;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.BusinessRuleRepositoryService;
import com.lava.br.core.repositories.bean.BusinessRule;
import com.lava.br.core.repositories.bean.Field;
import com.lava.br.core.repositories.bean.FieldAssigner;
///import com.lava.br.core.server.messages.RequestForProducer;
import com.lava.br.core.repositories.bean.PluginRegistered;
import com.lava.br.plugins.javascript.impl.JavaScript;
import com.lava.br.rulessvr.services.BusinessRuleService;
import com.lava.br.rulessvr.utils.BusinessRuleUtils;
import com.lava.br.rulessvr.utils.TemplateGroovy;

import org.bson.BSON;
import org.kie.api.runtime.Channel;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*@Component*/
public class EipChannel implements Channel {
    ApplicationContext applicationContext = null;

    BusinessRuleRepositoryService businessRuleRepositoryService;
    BusinessRuleService businessRuleService;
    //RequestForProducer.StreamProduceGateway streamGateway=null;
    public void send(Object object) {
        try {
            applicationContext = ApplicationContextProvider.getApplicationContext();
            businessRuleService= applicationContext.getBean(BusinessRuleService.class);
            businessRuleRepositoryService = applicationContext.getBean(BusinessRuleRepositoryService.class);
            String brId = ((IPlugin) object).getInfo().getBusinessRuleId();
            BusinessRule br = businessRuleRepositoryService.getBasicRepository().findById(brId).get();

            // check br is start
            if (!br.getStatus().equals(BusinessRuleStatus.START))
                return;

            //check if config consumer equal plugin
            // find unique plugin send from consumer
            PluginInfoClient objectPluginInfoClient=PluginMapper.toPluginInfoClient((IPlugin) object);
            PluginRegistered objectRegistered=businessRuleService.getTemplateRegisterPlugin(MessageBuilder.withPayload(objectPluginInfoClient).setHeader("communicationType",CommunicationType.Rest).build());
            // find unique plugin in busines rule as consumer
            Message<PluginInfoClient> pluginInfoClientMessage =businessRuleService.transformToPluginInfoClient(MessageBuilder.withPayload(br).setHeader("username",br.getUsername()).build());
            PluginRegistered brPluginRegistered=businessRuleService.getTemplateRegisterPlugin(MessageBuilder.withPayload(pluginInfoClientMessage.getPayload()).setHeader("communicationType",CommunicationType.Rest).build());
            //check two unique
            if(!objectRegistered.getPluginUniqueIdentity().equalsIgnoreCase(brPluginRegistered.getPluginUniqueIdentity()))
                return;

            Map<String, Object> model = new HashMap<String, Object>();
            for (IField field : ((IPlugin) object).getInfo().getInVariables()) {
                model.put(field.getName(), field.getValue());
            }
            for (int i = 0; i <br.getConsumerFilters().size(); i++) {
                model.put("p"+String.valueOf(i), "$p"+String.valueOf(i));
            }

            TemplateGroovy tg = new TemplateGroovy(model);

            for (IRuleProducer rr : br.getRuleProducers()) {
                PluginInfoClient child = new PluginInfoClient();
                child.setName(rr.getPluginType());

                if (rr.getPluginOutputVariables() != null)
                    for (FieldAssigner fa : rr.getPluginOutputVariables()) {
                        if (fa.getFieldName().equals("input") == false &&
                                fa.getFieldName().equals("userid") == false) {
                            Field field = new Field();
                            field.setName(fa.getFieldName());
                            field.setLabel("");
                            if(fa.getValue() instanceof String){
                                String template = (String) fa.getValue();
                                String transformedTemplate = tg.start(template);
                                field.setValue(transformedTemplate);
                            }
                            else{
                                field.setValue(fa.getValue());
                            }

                            child.getOutVariables().add(field);
                        }
                    }

                if (rr.getPluginConfigVariables() != null)
                    for (FieldAssigner fa : rr.getPluginConfigVariables()) {
                        Field field = new Field();
                        field.setName(fa.getFieldName());
                        field.setLabel("");
                        field.setValue(fa.getValue());
                        child.getConfigVariables().add(field);
                    }

                Field fieldInput = new Field();
                fieldInput.setName("input");
                fieldInput.setLabel("input");
                fieldInput.setValue(PluginMapper.toPluginInfoClient((IPlugin) object));
                child.getOutVariables().add(fieldInput);

                Field fieldUsernameInput = new Field();
                fieldUsernameInput.setName("userid");
                fieldUsernameInput.setLabel("userid");
                fieldUsernameInput.setValue(br.getUsername());

                child.getOutVariables().add(fieldUsernameInput);


                if (child.getName().equals("javascript")) {
                    JavaScript javaScriptP= applicationContext.getBean(JavaScript.class);
                    JavaScript javaScript= (JavaScript) javaScriptP.getInstance();

                    PluginMapper.pluginInfoCopyTo(child,javaScript);
                    javaScriptP.produce(javaScript);
                } else{
                    child.setBusinessRuleId(brId);
                    businessRuleService.sendToProducer(child);
                }
                child = null;
            }
        }catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

    }
}