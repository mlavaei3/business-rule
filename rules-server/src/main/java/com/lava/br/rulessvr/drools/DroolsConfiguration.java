package com.lava.br.rulessvr.drools;

/**
 * Created by Aria on 02/05/2018.
 */

import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.beans.IFieldFilter;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.Operator;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.repositories.BusinessRuleRepositoryService;
import com.lava.br.core.repositories.bean.BusinessRule;
import org.apache.commons.io.FileUtils;
import org.appformer.maven.integration.MavenRepository;
import org.drools.compiler.compiler.DroolsParserException;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.core.spi.KnowledgeHelper;
import org.drools.template.ObjectDataCompiler;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.ReleaseId;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.kie.scanner.KieMavenRepository.getKieMavenRepository;

//import static org.kie.scanner.KieMavenRepository.getKieMavenRepository;
//import static  org.appformer.maven.integration.MavenRepository.getMavenRepository;

@Configuration
public class DroolsConfiguration {
    private static final String drlFile = "Sample.drl";

    // @Bean
    public static void updateKieContainer(String artifactId, String versionId)throws Exception{

        BusinessRuleRepositoryService businessRuleService = null;
        ApplicationContext applicationContext = null;

        applicationContext = ApplicationContextProvider.getApplicationContext();
        businessRuleService = applicationContext.getBean(BusinessRuleRepositoryService.class);

        KieServices kieServices = KieServices.Factory.get();
        ReleaseId releaseId = kieServices.newReleaseId("com.lava", artifactId, versionId);

        getKieMavenRepository().removeLocalArtifact(releaseId);

        List<BusinessRule> brs = businessRuleService.getBasicRepository().findByArtifactId(artifactId);
        List<Rule> rules = new ArrayList<Rule>();
        for (BusinessRule br : brs) {
            rules.add(createRule(br));
        }
        createAndInstallKieJar(releaseId, rules, "PluginWithChannel");
    }

    public static KieContainer kieContainer(String artifactId, String versionId) throws Exception {
        KieServices kieServices = KieServices.Factory.get();
        ReleaseId releaseId = kieServices.newReleaseId("com.lava", artifactId, versionId);

        if (getKieMavenRepository().resolveArtifact(releaseId) == null) {
            updateKieContainer(artifactId,versionId);
        }

        //KieContainer kieContainer = kieServices.newKieContainer(releaseId);
        return kieServices.newKieContainer(releaseId);
    }

    protected static String getPom(ReleaseId releaseId, ReleaseId... dependencies) {
        String pom = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                + "         xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd\">\n"
                + "  <modelVersion>4.0.0</modelVersion>\n" + "\n" + "  <groupId>" + releaseId.getGroupId()
                + "</groupId>\n" + "  <artifactId>" + releaseId.getArtifactId() + "</artifactId>\n" + "  <version>"
                + releaseId.getVersion() + "</version>\n" + "\n";
        if (dependencies != null && dependencies.length > 0) {
            pom += "<dependencies>\n";
            for (ReleaseId dep : dependencies) {
                pom += "<dependency>\n";
                pom += "  <groupId>" + dep.getGroupId() + "</groupId>\n";
                pom += "  <artifactId>" + dep.getArtifactId() + "</artifactId>\n";
                pom += "  <version>" + dep.getVersion() + "</version>\n";
                pom += "</dependency>\n";
            }
            pom += "</dependencies>\n";
        }
        pom += "</project>";
        return pom;
    }

    private static void createAndInstallKieJar(ReleaseId releaseId, List<Rule> rules, String templateName)
            throws DroolsParserException, IOException, ClassNotFoundException {
        KieServices kieServices = KieServices.Factory.get();

        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.generateAndWritePomXML(releaseId);

        // kieFileSystem.write(ResourceFactory.newClassPathResource("rules/" +
        // drlFile));

        // List<Rule> rules = new ArrayList<Rule>();
        // Load each business rule

        List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>(rules.size());

        for (Rule rule : rules) {
            maps.add(rule.asMap());
        }

        ObjectDataCompiler compiler = new ObjectDataCompiler();
        // Compiles the list of rules using the template to create a readable Drools
        // Rules Language

        String drl = compiler.compile(maps, "/rules/templates/" + templateName + ".drl");
        kieFileSystem.write("src/main/resources/rules/" + templateName + ".drl", drl);

        KieModuleModel kModuleModel = kieServices.newKieModuleModel();
        KieBaseModel baseModel = kModuleModel.newKieBaseModel("defaultKBase").addPackage("rules").setDefault(true);
        baseModel.newKieSessionModel("defaultKSession").setDefault(true);

		/*
		 * kModuleModel.newKieBaseModel("kiemodulemodel") .addInclude("kbase1")
		 * .addInclude("kbase2") .newKieSessionModel("ksession6");
		 */

        kieFileSystem.writeKModuleXML(kModuleModel.toXML());
        // kieFileSystem.writePomXML(getPom(kieServices.newReleaseId("com.lava", "foo",
        // "1.0")));
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        kieBuilder.buildAll();
        if (kieBuilder.getResults().hasMessages(org.kie.api.builder.Message.Level.ERROR)) {
            // kieBuilder.getResults().getMessages().get(0)
            throw new DroolsParserException();
        }
        KieModule kieModule = kieBuilder.getKieModule();
        InternalKieModule internalKieModule = (InternalKieModule) kieServices.getRepository()
                .getKieModule(kieModule.getReleaseId());

        // KieMavenRepository repositories = getKieMavenRepository();
        String pomText = getPom(internalKieModule.getReleaseId());
        File pomFile = new File(System.getProperty("java.io.tmpdir"),
                MavenRepository.toFileName(internalKieModule.getReleaseId(), null) + ".pom");
        try {
            FileOutputStream fos = new FileOutputStream(pomFile);
            fos.write(pomText.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        getKieMavenRepository().installArtifact(internalKieModule.getReleaseId(), internalKieModule.getBytes(), FileUtils.readFileToByteArray(pomFile));
    }

    public static void debug(final KnowledgeHelper helper) {
        System.out.println("Triggered rule: " + helper.getRule().getName());
        if (helper.getMatch() != null && helper.getMatch().getObjects() != null) {
            for (Object object : helper.getMatch().getObjects()) {
                System.out.println("Data object: " + object);
            }
        }
    }

    public static Boolean compare(Object object1,Operator operator,Object object2) {
        try{
            if (operator.equals(Operator.CONTAINS))
            {
                return object1.toString().toUpperCase().contains(object2.toString().toUpperCase());
            }
            else if (operator.equals(Operator.GREATER_THAN))
            {
                return Double.parseDouble(object1.toString().trim()) > Double.parseDouble(object2.toString().trim());
            }
            else if (operator.equals(Operator.GREATER_THAN_OR_EQUAL_TO))
            {
                return Double.parseDouble(object1.toString().trim()) >= Double.parseDouble(object2.toString().trim());
            }

            else if (operator.equals(Operator.LESS_THAN))
            {
                return Double.parseDouble(object1.toString().trim()) < Double.parseDouble(object2.toString().trim());
            }
            else if (operator.equals(Operator.LESS_THAN_OR_EQUAL_TO))
            {
                return Double.parseDouble(object1.toString().trim()) <= Double.parseDouble(object2.toString().trim());
            }
        }catch (Exception ex){
            System.out.println("Compare Drools Rule Error: "+ex.getMessage());
            return false;
        }
        return false;
    }

    private static Rule createRule(BusinessRule br) {
        String businessRuleId=br.getId();
        String className=br.getConsumerType();
        List<IFieldFilter> filters=br.getConsumerFilters();
        // First of all, we create a rule giving it a friendly name
        Rule rule = new Rule("Mehdi Lavaei Is Creator");
        // Here we need to say what kind of object will be processed
        rule.setDataObject(BasePlugin.class.getName());
        rule.setPluginName(br.getConsumerType());
        rule.setBusinessRuleId(businessRuleId);
        // As expected, a rule needs condition to exists. So, let's create it...
        Condition condition = null;
        List<Condition> conditions = new ArrayList<Condition>();
        for (IFieldFilter ff : filters) {
            condition = new Condition();
            // What data, or property, will be checked

            condition.setProperty("getInfo().getValue(\"IN\",\""+ff.getFieldName()+"\")");//.toString()
            // What kind of check to do
            condition.setOperator(ff.getOperator());
            // What is the value to check
            condition.setValue(ff.getValue());

            // Next, is needed to set rule's condition
            conditions.add(condition);

        }
        rule.setConditions(conditions);

        rule.setConditionsExpression(br.getConsumerFiltersExpression());
        // Finally, this is what will be done when ours condition is satisfied
		/*
		 * Action action=new Action("Subject","body"); rule.setAction(action);
		 */

        return rule;
    }

    public static void fireRules(List<String> artifactIds,String versionId,IPlugin plugin){
        for (String artifactId:artifactIds ) {
            try {
                KieSession kieSession = DroolsConfiguration.kieContainer(artifactId, versionId).newKieSession("defaultKSession");
                kieSession.registerChannel("eip-channel", new EipChannel());
                kieSession.insert(plugin);
                kieSession.fireAllRules();
                kieSession.dispose();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
