package com.lava.br.rulessvr.utils;

import groovy.lang.Writable;
import groovy.text.TemplateEngine;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Aria on 08/05/2018.
 */
public class TemplateGroovy {
    Map binding;
    public TemplateGroovy(Map bnd) {
        binding=bnd;
    }
    public String start(String text) throws IOException, ClassNotFoundException {
        //def text = 'Dear "$firstname $lastname",\nSo nice to meet you in <% print city %>.\nSee you in ${month},\n${signed}'
        //def binding = ["firstname":"Sam", "lastname":"Pullara", "city":"San Francisco", "month":"December", "signed":"Groovy-Dev"]

        TemplateEngine engine = new groovy.text.SimpleTemplateEngine();
        Writable template = engine.createTemplate(text).make(binding);

        return template.toString();
    }
}
