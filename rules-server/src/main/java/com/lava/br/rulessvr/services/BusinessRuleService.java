package com.lava.br.rulessvr.services;

import com.lava.br.core.beans.IBusinessRule;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.beans.IScriptService;
import com.lava.br.core.clients.BusinessRuleClient;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.enums.BusinessRuleStatus;
import com.lava.br.core.enums.BusinessRuleTemplateType;
import com.lava.br.core.enums.CommunicationType;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.mappers.BusinessRuleMapper;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.plugins.dbstore.impl.DbStore;
import com.lava.br.core.plugins.utils.dbstore.CRUDKeyValue;
import com.lava.br.core.repositories.BusinessRuleRepositoryService;
import com.lava.br.core.repositories.DbStoreObjectRepositoryService;
import com.lava.br.core.repositories.bean.*;
//for stream
/*import com.lava.br.core.streams.server.RequestForConsumer;
import com.lava.br.core.streams.server.RequestForProducer;*/
import com.lava.br.rulessvr.controllers.BusinessRuleController;
import com.lava.br.rulessvr.drools.DroolsConfiguration;
import com.lava.br.rulessvr.repositories.PluginDefineRepositoryService;
import com.lava.br.rulessvr.repositories.PluginRegisteredRepositoryService;
import com.lava.br.rulessvr.restclients.RulesRestTemplateClient;
import com.lava.br.rulessvr.utils.BusinessRuleUtils;

import java.security.Principal;
import java.util.*;

//for stream
//import com.lava.br.rulessvr.streams.ServerRequestToUnconsume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.integration.annotation.Router;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.context.ExpressionCapable;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import static com.lava.br.rulessvr.drools.DroolsConfiguration.fireRules;

@Service
public class BusinessRuleService implements IScriptService {

    @Autowired
    private BusinessRuleRepositoryService businessRuleRepositoryService;

    @Autowired
    private RulesRestTemplateClient rulesRestTemplateClient;

    @Autowired
    PluginRegisteredRepositoryService pluginRegisteredRepositoryService;

    @Autowired
    PluginDefineRepositoryService pluginDefineRepositoryService;

    @Autowired
    DbStore dbStore;
    //for stream
    //@Autowired
    //RequestForConsumer.StreamConsumeGateway streamConsumeGateway;

    //for stream
    //@Autowired
    //RequestForProducer.StreamProduceGateway streamProduceGateway;

    //@Autowired
    //ServerRequestToUnconsume serverRequestToUnconsume;

    @Async
    public void runFireRules(IPlugin plugin){
        System.out.println("Thread runFireRules Running");
        // get all BR from pluginRegisterd
        fireRules(getArtifactIds(plugin),"1.0",plugin);
    }

    @Async
    public void sendToProducer(PluginInfoClient pluginInfoClient){

        System.out.println("Thread sendToProducer Running");
        PluginDefine pluginDefine= pluginDefineRepositoryService.getBasicRepository().findFirstByName(pluginInfoClient.getName());
        if (pluginDefine.getCommunicationType()==CommunicationType.Rest)
            rulesRestTemplateClient.producePlugin(pluginInfoClient);
        //for stream
        /*else
            streamProduceGateway.process(MessageBuilder.withPayload(pluginInfoClient)
                    .setHeader("type",pluginInfoClient.getName())
                    .setHeader("action","produce")
                    .build()
            ).getPayload();*/

    }

    public List<String> getArtifactIds(IPlugin basePlugin) {
        PluginInfoClient pluginInfoClient=PluginMapper.toPluginInfoClient(basePlugin);
        PluginRegistered pluginRegistered=getTemplateRegisterPlugin(MessageBuilder.withPayload(pluginInfoClient).setHeader("communicationType",CommunicationType.Rest).build());

        List<PluginRegistered>  pluginRegistereds=pluginRegisteredRepositoryService.getBasicRepository().findByConsumerTypeAndPluginUniqueIdentity(basePlugin.getInfo().getName(),pluginRegistered.getPluginUniqueIdentity());
        List<String> lstArtifactIds = new ArrayList();

        for(PluginRegistered pr:pluginRegistereds){
            Optional<BusinessRule> businessRuleOptional= businessRuleRepositoryService.getBasicRepository().findById(pr.getBusinessRuleId());
            if(businessRuleOptional!=null)
            {
                lstArtifactIds.add(businessRuleOptional.get().getArtifactId());
            }
        }

        /*String triggerName=basePlugin.getInfo().getValue(PluginInfo.CONFIG,"consumerTrigger").toString();
        List<FieldAssigner> fieldAssigners=new ArrayList<FieldAssigner>();
        for (Field field:basePlugin.getInfo().getConfigVariables())
        {
            if (field.getRefTriggers().contains(triggerName) && field.getUnique())
                fieldAssigners.add(new FieldAssigner(field.getName(),field.getValue().toString()));
        }

        //BusinessRuleRepositoryService businessRuleService = (BusinessRuleRepositoryService) ApplicationContextProvider.getApplicationContext().getBean("businessRuleRepositoryService");
        List<BusinessRule> brs = null;
        if(fieldAssigners.size() == 1) {
            brs = businessRuleRepositoryService.findByConsumerTypeAndConsumerConfigFieldName(basePlugin.getInfo().getName(),
                    ((FieldAssigner)fieldAssigners.get(0)).getFieldName(), basePlugin.getInfo().getValue("CONFIG", ((FieldAssigner)fieldAssigners.get(0)).getFieldName()).toString());
        } else {
            brs = businessRuleRepositoryService.findByConsumerTypeAndFieldAssigners(basePlugin.getInfo().getName(), fieldAssigners);
        }


        Iterator var5 = brs.iterator();

        while(var5.hasNext()) {
            BusinessRule br = (BusinessRule)var5.next();
            if(!lstArtifactIds.contains(br.getArtifactId())) {
                lstArtifactIds.add(br.getArtifactId());
            }
        }*/

        return lstArtifactIds;
    }

    public Integer registerPluginNumber(Message<PluginInfoClient> message) {
        PluginRegistered pluginRegistered=getTemplateRegisterPlugin(message);
        List<PluginRegistered> lstPluginsRegistered=pluginRegisteredRepositoryService.getBasicRepository()
                .findByConsumerTypeAndPluginUniqueIdentity(pluginRegistered.getConsumerType(),pluginRegistered.getPluginUniqueIdentity());

        return lstPluginsRegistered.size();
        /*if(lstPluginsRegistered.size()==0){
            pluginRegistered=rulesRestTemplateClient.registerPlugin(pluginInfoClient);
        }else{
            pluginRegistered.setBusinessRuleId(pluginInfoClient.getBusinessRuleId());
            pluginRegistered.setInstanceId(lstPluginsRegistered.get(0).getInstanceId());
        }
        pluginRegisteredRepositoryService.getBasicRepository().insert(pluginRegistered);

        return pluginRegistered.toString();*/
    }

    public PluginRegistered addPluginRegistered(Message<PluginInfoClient> message) {
        if (message.getHeaders().get("action").toString()=="insert" ||
                message.getHeaders().get("action").toString()=="update"){
            return getTemplateRegisterPlugin(message);
        }else{
            return pluginRegisteredRepositoryService
                    .getBasicRepository()
                    .findFirstByBusinessRuleId(message.getPayload().getBusinessRuleId());
        }
    }

    @ServiceActivator
    //@Async
    public Message<PluginInfoClient> updateStatusForBusinessRule(Message<PluginInfoClient> message) {
        BusinessRule businessRule=get(message.getPayload().getBusinessRuleId());
        businessRule.setStatus(BusinessRuleStatus.valueOf(message.getHeaders().get("action").toString().toUpperCase()));
        businessRuleRepositoryService.getBasicRepository().save(businessRule);
        return message;
    }

    @ServiceActivator
    //@Async
    public Message<PluginInfoClient> registerPlugin(Message<PluginInfoClient> message) {
        //Thread thread = new Thread(() -> {
        //ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();

        //streamProduceGateway=  applicationContext.getBean(RequestForProducer.StreamProduceGateway.class);
        PluginRegistered pluginRegistered=getTemplateRegisterPlugin(message);
        List<PluginRegistered> lstPluginsRegistered=pluginRegisteredRepositoryService.getBasicRepository()
                .findByConsumerTypeAndPluginUniqueIdentity(pluginRegistered.getConsumerType(),pluginRegistered.getPluginUniqueIdentity());

        if(lstPluginsRegistered.size()==0) {
            System.out.println("Thread sendToProducer Running");
            if (message.getHeaders().get("communicationType") == CommunicationType.Rest)
                pluginRegistered = rulesRestTemplateClient.registerPlugin(message.getPayload());
            //for stream
            /*if (message.getHeaders().get("communicationType") == CommunicationType.Stream)
                pluginRegistered = streamConsumeGateway.process(MessageBuilder.withPayload(
                        message.getPayload())
                        .setHeader("type", message.getPayload().getName())
                        .setHeader("action", "consume")
                        .build())
                        .getPayload();*/
        }else{
            pluginRegistered.setBusinessRuleId(message.getPayload().getBusinessRuleId());
            pluginRegistered.setInstanceId(lstPluginsRegistered.get(0).getInstanceId());
        }
        pluginRegisteredRepositoryService.getBasicRepository().insert(pluginRegistered);
        //});
        //thread.start();
        //thread=null;
        return message;
    }

    @ServiceActivator
    public Message<PluginInfoClient> registerPluginToDb(Message<PluginInfoClient> message) {
        PluginRegistered pluginRegistered =getTemplateRegisterPlugin(message);//(PluginRegistered)message.getHeaders().get("pluginRegistered");

        List<PluginRegistered> lstPluginsRegistered=pluginRegisteredRepositoryService.getBasicRepository()
                .findByConsumerTypeAndPluginUniqueIdentity(pluginRegistered.getConsumerType(),pluginRegistered.getPluginUniqueIdentity());

        pluginRegistered.setBusinessRuleId(message.getPayload().getBusinessRuleId());
        pluginRegistered.setInstanceId(lstPluginsRegistered.get(0).getInstanceId());
        pluginRegisteredRepositoryService.getBasicRepository().insert(pluginRegistered);

        return message;
    }

    @ServiceActivator
    public Message<PluginInfoClient> removePluginFromDb(Message<PluginInfoClient> message) {
        //PluginRegistered pluginRegistered =(PluginRegistered)message.getHeaders().get("pluginRegistered"); //pluginRegistered=getTemplateRegisterPlugin(message.getPayload());
        List<PluginRegistered> lstPluginRegistered=pluginRegisteredRepositoryService
                .getBasicRepository()
                .findByBusinessRuleId(message.getPayload().getBusinessRuleId());

        for(PluginRegistered pg:lstPluginRegistered) {
            pluginRegisteredRepositoryService.getBasicRepository().delete(pg);
        }
        return message;
    }

    public PluginRegistered getTemplateRegisterPlugin(Message<PluginInfoClient> message) {
        PluginRegistered pluginRegistered=null;
        if (message.getHeaders().get("communicationType")==CommunicationType.Rest)
            pluginRegistered=rulesRestTemplateClient.getTemplateRegisterPlugin(message.getPayload());
        //for stream
        /*else
            pluginRegistered=streamConsumeGateway.process(MessageBuilder.withPayload(
                    message.getPayload())
                    .setHeader("type",message.getPayload().getName())
                    .setHeader("action","template")
                    .build())
                    .getPayload();*/


        return pluginRegistered;
    }

    @ServiceActivator
    //@Async
    public Message unRegisterPlugin(Message<PluginInfoClient> message) {
        //serverRequestToUnconsume.sendMessageToUnConsume(message.getPayload());
        try{
            PluginRegistered pluginRegistered = pluginRegisteredRepositoryService.getBasicRepository().findFirstByBusinessRuleId(message.getPayload().getBusinessRuleId());
            List<PluginRegistered> lstPluginsRegistered = pluginRegisteredRepositoryService.getBasicRepository()
                    .findByConsumerTypeAndPluginUniqueIdentityAndInstanceId(
                            pluginRegistered.getConsumerType(),
                            pluginRegistered.getPluginUniqueIdentity(),
                            pluginRegistered.getInstanceId());

            Boolean flag = false;
            // Before this step plugin removed from Database
            if (lstPluginsRegistered.size() == 1) {
                if (message.getHeaders().get("communicationType") == CommunicationType.Rest)
                    flag = rulesRestTemplateClient.unRegisterPlugin(message.getPayload(), lstPluginsRegistered.get(0));
                // if (message.getHeaders().get("communicationType") == CommunicationType.Stream)
                //serverRequestToUnconsume.sendMessageToUnConsume((PluginInfoClient)message.getHeaders().get("oldPlugin"));

       /* PluginRegistered pluginRegistered = (PluginRegistered)message.getHeaders().get("pluginRegistered"); //pluginRegistered=getTemplateRegisterPlugin(message.getPayload());

        if (message.getHeaders().get("communicationType")==CommunicationType.Rest)
            rulesRestTemplateClient.unRegisterPlugin(((PluginInfoClient)message),pluginRegistered);
        else
            serverRequestToUnconsume.sendMessageToUnConsume((PluginInfoClient)message.getHeaders().get("oldPlugin"));*/
                //}
                //pluginRegisteredRepositoryService.getBasicRepository().delete(pluginRegistered.getId());

            }
        }
        catch(Exception ex){
            String sg=ex.getMessage();
        }
        return message;
    }

    public void unRegisterAllConsumer(String brId) {

    }

    //@Cacheable(value = "getPlugins",key = "#page+'-'+#size+'-'+ #orderBy") //key = "#result.name", unless="#result==null"
    public Page<PluginDefine> getPlugins(int page,int size,String orderBy){
        return pluginDefineRepositoryService.getPluginsDefineByPagination(page,size,orderBy);
    }

    //@Cacheable(value = "getBusinessRuleAbbrClient",key = "#userId+'-'+#page+'-'+#size+'-'+ #orderBy") //key = "#result.name", unless="#result==null"
    public Page<BusinessRule> getBusinessRuleAbbrClient(String userId,int page, int size, String orderBy){
        return businessRuleRepositoryService.getBusinessRuleAbbrClientByPagination(userId,page,size,orderBy);
    }

    public Page<BusinessRule> getTemplateBusinessRuleAbbrClient(String type,int page, int size, String orderBy){
        return businessRuleRepositoryService.getTemplateBusinessRuleAbbrClientByPagination(type,page,size,orderBy);
    }

    @ServiceActivator
    @Cacheable(value = "getPluginInfoClient",key = "#message.payload") //key = "#result.name", unless="#result==null"
    public PluginInfoClient getPluginInfoClient(Message<String> message){
        PluginInfoClient pluginInfoClient=null;
        if (message.getHeaders().get("communicationType") ==CommunicationType.Rest)
            pluginInfoClient=rulesRestTemplateClient.getPluginInfoClient(message.getPayload());
        //for stream
        /*if (message.getHeaders().get("communicationType") ==CommunicationType.Stream)
            pluginInfoClient=streamProduceGateway.process(MessageBuilder
                    .withPayload(message.getPayload())
                    .setHeader("type",message.getPayload())
                    .setHeader("action","pluginInfo")
                    .build()
            ).getPayload();*/

        return pluginInfoClient;
    }

    public void runBusinessRules() {
        try {
            List<BusinessRule> brs = businessRuleRepositoryService.getBasicRepository().findAll();
            for (BusinessRule br : brs) {
                // consumersService.sendMessageConsumer(br);
                PluginInfoClient pic = new PluginInfoClient();
                pic.setBusinessRuleId(br.getId());
                pic.setName(br.getConsumerType());

                Field field=new Field();
                field.setName("userid");
                field.setLabel("userid");
                field.setValue(br.getUsername());

                pic.getConfigVariables().add(field);//Field.builder().name("userid").value(br.getUsername()).build());
                for (FieldAssigner fa : br.getConsumerConfig()) {
                    field=new Field();
                    field.setName(fa.getFieldName());
                    field.setValue(fa.getValue());
                    pic.getConfigVariables().add(field);//Field.builder().name(fa.getFieldName()).value(fa.getValue()).build());
                }
                PluginDefine pluginDefine= pluginDefineRepositoryService.getBasicRepository().findFirstByName(br.getConsumerType());
                registerPlugin(MessageBuilder.withPayload(pic).setHeader("communicationType",pluginDefine.getCommunicationType()).build());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @ServiceActivator
    public Message updateKieMaven(Message<BusinessRule> message){
        try {
            String artifactId=message.getHeaders().get("artifactId").toString();
            String version=message.getHeaders().get("version").toString();
            DroolsConfiguration.updateKieContainer(artifactId, version);
            return message;
        } catch (Exception e) {
            return null;
        }
    }

    @Router
    public String choiceAction(Message<PluginInfoClient> message){
        String destinationChannel = null;
        switch (message.getHeaders().get("action").toString())
        {
            case "insert":
                destinationChannel="insert";
                break;
            case "update":
                destinationChannel="update";
                break;
            case "start":
                destinationChannel="start";
                break;
            case "stop":
                destinationChannel="stop";
                break;
        }
        return destinationChannel;
    }

    @Transformer
    public BusinessRule transformToBusinessRule(Message<BusinessRuleClient> message){
        BusinessRule newBr =null;
        newBr =  BusinessRuleMapper.toBusinessRuleClient( message.getPayload());
        newBr.setUsername(message.getHeaders().get("username").toString());
        newBr.setArtifactId(message.getHeaders().get("username").toString());
        newBr.setVersionId("1.0");
        return newBr;
    }

    @Transformer
    public Message<PluginInfoClient> transformToPluginInfoClient(Message<BusinessRule> message){
        PluginInfoClient pic = new PluginInfoClient();
        pic.setBusinessRuleId(message.getPayload().getId());
        pic.setName(message.getPayload().getConsumerType());

        Field field=null;
        for (FieldAssigner fa : message.getPayload().getConsumerConfig()) {
            field=new Field();
            field.setName(fa.getFieldName());
            field.setValue(fa.getValue());
            pic.getConfigVariables().add(field);// Field.builder().name(fa.getFieldName()).value(fa.getValue()).build());
        }
        field=new Field();
        field.setName("userid");
        field.setValue(message.getHeaders().get("username").toString());

        pic.getConfigVariables().add(field);//Field.builder().name("userid").value(message.getHeaders().get("username").toString()).build());

        Message<PluginInfoClient> newMessage=MessageBuilder.createMessage(pic,message.getHeaders());
        return newMessage;
    }

    @ServiceActivator
    public Message insertToDb(Message<BusinessRule>  message) {
        try {
            businessRuleRepositoryService.getBasicRepository().insert((BusinessRule)message.getHeaders().get("newBr"));
            return message;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ServiceActivator
    public Message updateToDb(Message<BusinessRule> message) {
        try {
            businessRuleRepositoryService.getBasicRepository().save((BusinessRule)message.getHeaders().get("newBr"));
            return message;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Message addPluginHeaders(Message<BusinessRule> message) {
        Map<String,Object> messageHeaders = null;
        try {
            if (message.getHeaders().get("action").toString().equals("insert") ||
                    message.getHeaders().get("action").toString().equals("update")) {
                BusinessRule newBr = message.getPayload();//  BusinessRuleMapper.toBusinessRuleClient( businessRuleClient );
                messageHeaders = new HashMap<>();
                messageHeaders.put("newBr", newBr);
                IBusinessRule oldBr = null;
                if (message.getHeaders().get("action").toString().equals("update") &&
                        message.getPayload().getId() != null)
                    oldBr = businessRuleRepositoryService.getBasicRepository().findById(message.getPayload().getId()).get();
                messageHeaders.put("oldBr", oldBr);
                // if config's consumer change4
                if (oldBr != null) {
                    if (BusinessRuleUtils.configsEquals(oldBr, newBr) == false) {
                        messageHeaders.put("isConfigChanged", true);
                        // unregister old plugin
                        PluginInfoClient pic = new PluginInfoClient();
                        pic.setBusinessRuleId(oldBr.getId());
                        pic.setName(oldBr.getConsumerType());
                        Field field=new Field();
                        for (FieldAssigner fa : oldBr.getConsumerConfig()) {
                            field=new Field();
                            field.setName(fa.getFieldName());
                            field.setValue(fa.getValue());
                            pic.getConfigVariables().add(field);// Field.builder().name(fa.getFieldName()).value(fa.getValue()).build());
                        }
                        messageHeaders.put("oldPlugin", pic);
                        // register new plugin
                        pic = new PluginInfoClient();
                        pic.setBusinessRuleId(newBr.getId());
                        pic.setName(newBr.getConsumerType());
                        for (FieldAssigner fa : newBr.getConsumerConfig()) {
                            field=new Field();
                            field.setName(fa.getFieldName());
                            field.setValue(fa.getValue());
                            pic.getConfigVariables().add(field);//Field.builder().name(fa.getFieldName()).value(fa.getValue()).build());
                        }
                        messageHeaders.put("newPlugin", pic);
                        //registerPlugin(pic);
                    }
                    // update if filter's consumer is change
                    if (BusinessRuleUtils.filtersEquals(oldBr, newBr) == false) {
                        messageHeaders.put("isFilterChanged", true);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MessageBuilder.fromMessage(message).copyHeaders(messageHeaders).build();
    }

    public PluginDefine getPluginDefine (String pluginName){
        PluginDefine pluginDefine= pluginDefineRepositoryService.getBasicRepository().findFirstByName(pluginName);
        if (pluginDefine!=null)
            return pluginDefine;
        else
            return null;
    }

    public void delete(@PathVariable("id") String id, Principal principal) {
        BusinessRule br = businessRuleRepositoryService.getBasicRepository().findById(id).get();

        // unregister old plugin
        PluginInfoClient pic = new PluginInfoClient();
        pic.setBusinessRuleId(br.getId());
        pic.setName(br.getConsumerType());
        Field field=null;
        for (FieldAssigner fa : br.getConsumerConfig()) {
            field=new Field();
            field.setName(fa.getFieldName());
            field.setValue(fa.getValue());
            pic.getConfigVariables().add(field);// Field.builder().name(fa.getFieldName()).value(fa.getValue()).build());
        }

        unRegisterPlugin(MessageBuilder.withPayload(pic)
                .setHeader("communicationType",getPluginDefine(pic.getName()).getCommunicationType())
                .build());

        updateKieMaven(MessageBuilder.withPayload(br)
                .setHeader("artifactId",br.getArtifactId())
                .setHeader("version",br.getVersionId())
                .build());


        businessRuleRepositoryService.getBasicRepository().deleteById(id);
    }

    public BusinessRule get(String id){

        return businessRuleRepositoryService.getBasicRepository().findById(id).get();
    }

    public Map<String,List<String>> getAccounting(String userid) {
        List<PluginDefine> pluginDefines = pluginDefineRepositoryService.getBasicRepository().findByHasAccounting(true);
        Map<String, List<String>> result = new HashMap<>();

        for (PluginDefine pd : pluginDefines) {
            List<String> accounts = new ArrayList<>();
            Map<String, String> accountsMap = CRUDKeyValue.getAllValueForKey(dbStore, pd.getName(), userid, DbStoreType.ACCOUNTING, "account", "token");
            accountsMap.forEach((k, v) -> {
                accounts.add(k);
            });
            if(accounts.size()>0) {
                result.put(pd.getName(), accounts);
            }
        }
        return result;
    }

    @Autowired
    BusinessRuleController businessRuleController;

    public void removeAccount(String userid,String pluginName,String account, Principal principal) {
        CRUDKeyValue.removeKeyWithValue(dbStore,pluginName,userid,DbStoreType.ACCOUNTING,"account",account);
        List<FieldAssigner> fieldAssigners=new ArrayList<>();
        fieldAssigners.add(new FieldAssigner("account",account));
        List<BusinessRule> businessRules= businessRuleRepositoryService.findByUsernameAndConsumerTypeAndFieldAssigners(userid,pluginName,fieldAssigners);
        for(BusinessRule businessRule:businessRules){
            businessRuleController.stop(businessRule.getId(),principal);
        }
    }

    @Override
    public void exec(IPlugin plugin) {
        PluginInfoClient pluginInfoClient= PluginMapper.toPluginInfoClient(plugin);
        sendToProducer(pluginInfoClient);
    }

    /*   public BusinessRule update(BusinessRuleClient businessRuleClient, Principal principal) {
           try {
               BusinessRule newBr =  BusinessRuleMapper.toBusinessRuleClient( businessRuleClient );
               IBusinessRule oldBr = businessRuleRepositoryService.getBasicRepository().findOne(businessRuleClient.getId());

               // if config's consumer change4
               if (oldBr != null) {
                   if (BusinessRuleUtils.configsEquals(oldBr, newBr) == false) {

                       // unregister old plugin
                       PluginInfoClient pic = new PluginInfoClient();
                       pic.setBusinessRuleId(oldBr.getId());
                       pic.setName(oldBr.getConsumerType());
                       for (FieldAssigner fa : oldBr.getConsumerConfig()) {
                           pic.getConfigVariables().add( Field.builder().name(fa.getFieldName()).value(fa.getValue()).build());
                       }
                       unRegisterPlugin(pic);

                       // register new plugin
                       pic = new PluginInfoClient();
                       pic.setBusinessRuleId(newBr.getId());
                       pic.setName(newBr.getConsumerType());
                       for (FieldAssigner fa : newBr.getConsumerConfig()) {
                           pic.getConfigVariables().add( Field.builder().name(fa.getFieldName()).value(fa.getValue()).build());
                       }

                       //registerPlugin(pic);
                   }
                   // update if filter's consumer is change
                   if (BusinessRuleUtils.filtersEquals(oldBr, newBr) == false) {
                       DroolsConfiguration.updateKieContainer(principal.getName(), "1.0");
                   }
                   // save in repositories with change producers
                   newBr = businessRuleRepositoryService.getBasicRepository().save(newBr);

                   return newBr;
               }
           } catch (InstantiationException e) {
               e.printStackTrace();
           } catch (IllegalAccessException e) {
               e.printStackTrace();
           } catch (Exception e) {
               e.printStackTrace();
           }
           return null;
       }
   */

}
