package com.lava.br.rulessvr.controllers;

/**
 * Created by Aria on 02/05/2018.
 */

import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.clients.BusinessRuleClient;
import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.enums.BusinessRuleStatus;
import com.lava.br.core.enums.CommunicationType;
import com.lava.br.core.exceptions.BaseException;
import com.lava.br.core.mappers.BusinessRuleMapper;
import com.lava.br.core.mappers.PluginMapper;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.javascript.impl.JavaScript;
import com.lava.br.rulessvr.services.BusinessRuleService;
import com.lava.br.rulessvr.services.IntegrationService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.Map;

/**
 * Created by Aria on 16/03/2018.
 */
//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping({"v1"})
public class BusinessRuleController {

    private String version = "1.0";

    @Autowired
    private BusinessRuleService businessRuleService;

    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping(path = {"/test2"})
    public String test2(){
        return "rrrr";
    }
    @Autowired
    Environment environment;

    private BusinessRuleClient returnNullIfTemplate(BusinessRuleClient businessRuleClient){
        if (businessRuleClient.getStatus()==BusinessRuleStatus.TEMPLATE)
            return null;
        else
           return  businessRuleClient;
    }

    @PostMapping(path = {"/getValue"})
    public String getValue(@RequestBody String key){
        switch (key){
            case "port":
                return environment.getProperty("local.server.port");
            case "socket":
                this.template.convertAndSend("/topic/hi","{\"message\": \"Hello Socket!\"}");
                break;
        }
        return "";

    }

    @RequestMapping(value="/fireRules",method = RequestMethod.POST)
    public String fireRules(@RequestBody PluginInfoClient pluginInfoClient)  {
        BasePlugin basePlugin=new BasePlugin();
        PluginMapper.pluginInfoCloneTo(pluginInfoClient,basePlugin);
        businessRuleService.runFireRules(basePlugin);
        return "okkk";
    }

    @GetMapping(path = {"/run"})
    public void runBusinessRules(Principal principal) throws NoSuchMethodException {
        Authentication authentication=SecurityContextHolder.getContext().getAuthentication();
        businessRuleService.runBusinessRules();
    }

    @GetMapping(path = {"/list/{page}/{size}/{orderBy}"})
    public Page<BusinessRule> findAll(@PathVariable("page") int page,
                                      @PathVariable("size") int size,
                                      @PathVariable("orderBy") String orderBy,
                                      Principal principal) throws NoSuchMethodException {

        return businessRuleService.getBusinessRuleAbbrClient(principal.getName(),page,size,orderBy);
    }

    @GetMapping(path = {"/templateList/{type}/{page}/{size}/{orderBy}"})
    public Page<BusinessRule> templateListFindAll(@PathVariable("type") String type,
                                      @PathVariable("page") int page,
                                      @PathVariable("size") int size,
                                      @PathVariable("orderBy") String orderBy,
                                      Principal principal) throws NoSuchMethodException {

        return businessRuleService.getTemplateBusinessRuleAbbrClient(type,page,size,orderBy);
    }

    @PostMapping(path = {"/save"})
    public BusinessRuleClient save(@RequestBody BusinessRuleClient businessRuleClient, Principal principal){
        if (returnNullIfTemplate(businessRuleClient)==null)return null;
        Message<BusinessRuleClient> message=MessageBuilder.withPayload(businessRuleClient)
                .setHeader("username",principal.getName())
                .setHeader("action","insert")
                .setHeader("artifactId",principal.getName())
                .setHeader("version",version)
                .setHeader("isFilterChanged",true)
                .setHeader("isConfigChanged",false)
                .setHeader("oldPlugin",null)
                .setHeader("newPlugin",null)
                .setHeader("numberRegisteration",0)
                .setHeader("pluginRegistered",null)
                .setHeader("communicationType",businessRuleService.getPluginDefine(businessRuleClient.getConsumerType()).getCommunicationType())

                .build();

        IntegrationService helloService =
                ApplicationContextProvider.getApplicationContext().getBean("helloGateway", IntegrationService.class);
        Message pluginRegistered=helloService.saveBusinessRuleIntegration(message);
        if (businessRuleClient.getStatus().equals(BusinessRuleStatus.START)){
            Message message2=MessageBuilder.fromMessage(message).setHeader("action","start").build();
            Message pluginRegistered2=helloService.changeStatusBusinessRuleIntegration(message2);

        }

        return businessRuleClient;
    }

    @PostMapping(path = {"/start"})
    public Boolean start(@RequestBody String businessRuleId, Principal principal) {
        BusinessRule businessRule=businessRuleService.get(businessRuleId);
        BusinessRuleClient businessRuleClient=BusinessRuleMapper.toBusinessRuleClient(businessRule);

        if (returnNullIfTemplate(businessRuleClient)==null)return null;

        Message<BusinessRuleClient> message=MessageBuilder
                .withPayload(businessRuleClient)
                .setHeader("username",principal.getName())
                .setHeader("action","start")
                .setHeader("numberRegisteration",0)
                .setHeader("pluginRegistered",new PluginRegistered())
                .setHeader("communicationType",businessRuleService.getPluginDefine(businessRule.getConsumerType()).getCommunicationType())
                .build();

        IntegrationService helloService =
                ApplicationContextProvider.getApplicationContext().getBean("helloGateway", IntegrationService.class);

        Message<PluginInfoClient> result=helloService.changeStatusBusinessRuleIntegration(message);
        return true;//result.getPayload();
    }

    @PostMapping(path = {"/stop"})
    public Boolean stop(@RequestBody String businessRuleId, Principal principal) {

        BusinessRule businessRule=businessRuleService.get(businessRuleId);
        BusinessRuleClient businessRuleClient=BusinessRuleMapper.toBusinessRuleClient(businessRule);

        if (returnNullIfTemplate(businessRuleClient)==null)return null;

        Message<BusinessRuleClient> message=MessageBuilder
                .withPayload(businessRuleClient)
                .setHeader("username",principal.getName())
                .setHeader("businessRuleId",businessRuleId)
                .setHeader("action","stop")
                .setHeader("numberRegisteration",0)
                .setHeader("pluginRegistered",new PluginRegistered())
                .setHeader("communicationType",businessRuleService.getPluginDefine(businessRule.getConsumerType()).getCommunicationType())
                .build();

        IntegrationService helloService =
                ApplicationContextProvider.getApplicationContext().getBean("helloGateway", IntegrationService.class);

        Message<PluginInfoClient> result=helloService.changeStatusBusinessRuleIntegration(message);
        return true;//result.getPayload();

    }

    @GetMapping(path = {"/get/{id}"})
    public BusinessRule findOne(@PathVariable("id") String id, Principal principal){
        return businessRuleService.get(id);
    }

    @PostMapping(path = {"/logout"})
    public Boolean logout(HttpServletRequest request){
        try{
            request.logout();
        }catch(Exception ex){
            return false;
        }
        return true;
    }

    @GetMapping(path = {"/getInstance/{type}"})
    public BusinessRule getInstance(@PathVariable("type") String type, Principal principal){
      /*  BusinessRule br=new BusinessRule();
        br.setId(null);
        br.setUsername("mehdi");//principal.getName()
        br.setConsumerType(type);
        br.setArtifactId("mehdi");//principal.getName()
        br.setName("example1");
        List<IFieldAssigner> fas=new ArrayList<IFieldAssigner>();
        IPlugin plugin= pluginManager.get(type);

        for(Field fi:plugin.getInfo().getConfigVariables()){
            IFieldAssigner fa =new FieldAssigner();
            fa.setFieldName(fi.getName());
            fa.setValue("");
            fas.add(fa);
        }
        br.setConsumerConfig(fas);

        List<IFieldFilter> ffs=new ArrayList<IFieldFilter>();
        for(Field fi:plugin.getInfo().getInVariables()){
            IFieldFilter ff =new FieldFilter();
            ff.setOperator(Operator.CONTAINS);
            ff.setFieldName(fi.getName());
            ff.setValue("");
            ffs.add(ff);
        }
        br.setConsumerFilters(ffs);
        br.setConsumerFiltersExpression("()");
        List<IRuleProducer> rps=new ArrayList<IRuleProducer>();
        IRuleProducer rp=new RuleProducer();
        rp.setId((new ObjectId()).toString());
        rp.setPriority(1);
        rp.setPluginType("javascript");
        fas=new ArrayList<IFieldAssigner>();
        
        // for javascript plugin
        IFieldAssigner fa =new FieldAssigner();
        fa.setFieldName("script");
        fa.setValue("");
        fas.add(fa);
        //
        fa =new FieldAssigner();
        fa.setFieldName("input");
        fa.setValue("");
        fas.add(fa);
        //
        fa =new FieldAssigner();
        fa.setFieldName("result");
        fa.setValue("");
        fas.add(fa);

        rp.setPluginOutputVariables(fas);
        rps.add(rp);
        br.setRuleProducers(rps);

        return br;*/
        return null;
    }

    @PutMapping(path = {"/update"})
    public BusinessRuleClient update(@RequestBody BusinessRuleClient businessRuleClient, Principal principal){
        BusinessRule businessRule=businessRuleService.get(businessRuleClient.getId());
        BusinessRuleClient businessRuleClientTest=BusinessRuleMapper.toBusinessRuleClient(businessRule);
        if (returnNullIfTemplate(businessRuleClientTest)==null)return null;

        Message<BusinessRuleClient> message=MessageBuilder.withPayload(businessRuleClient)
                .setHeader("username",principal.getName())
                .setHeader("action","update")
                .setHeader("artifactId",principal.getName())
                .setHeader("version",version)
                .setHeader("isFilterChange",false)
                .setHeader("isConfigChange",false)
                .setHeader("oldPlugin",null)
                .setHeader("newPlugin",null)
                .setHeader("numberRegisteration",0)
                .setHeader("pluginRegistered",null)
                .setHeader("communicationType",businessRuleService.getPluginDefine(businessRuleClient.getConsumerType()).getCommunicationType())

                .build();

        IntegrationService helloService =
                ApplicationContextProvider.getApplicationContext().getBean("helloGateway", IntegrationService.class);
        Message pluginRegistered=helloService.saveBusinessRuleIntegration(message);
        return businessRuleClient;
    }

    @DeleteMapping(path ={"/delete/{id}"})
    public void delete(@PathVariable("id") String id,Principal principal) {
        BusinessRule businessRule=businessRuleService.get(id);
        BusinessRuleClient businessRuleClientTest=BusinessRuleMapper.toBusinessRuleClient(businessRule);
        if (returnNullIfTemplate(businessRuleClientTest)!=null) {
            businessRuleService.delete(id, principal);
        }
    }

    @GetMapping(path = {"/getAllProperty/{type}"})
    public List<Field> getAllProperty(@PathVariable("type") String type) {
        /*IPlugin plugin=pluginManager.get(type);
        return plugin.getInfo().getInVariables();*/
        return null;
    }

    @GetMapping(path = {"/getPlugins/{page}/{size}/{orderBy}"})
    public Page<PluginDefine> getPlugins(@PathVariable("page") int page,
                                         @PathVariable("size") int size,
                                         @PathVariable("orderBy") String orderBy) throws NoSuchMethodException {
        return businessRuleService.getPlugins(page,size,orderBy);
    }
    @Autowired
    JavaScript javaScript;

    @RequestMapping(value="/getPluginInfoClient",method = RequestMethod.GET
            ,produces = "application/json")
    public PluginInfoClient getPluginInfoClient() throws BaseException {
        try {
            // return PluginMapper.toPluginInfoClient(getPluginService().getPlugin());
            /*PluginInfoClient pluginInfoClient=businessRuleService.getPluginInfoClient(
                    MessageBuilder
                            .withPayload(pluginName)
                            .setHeader("communicationType",businessRuleService.getPluginDefine(pluginName).getCommunicationType())
                            .build());
            return pluginInfoClient;*/
            PluginInfoClient pluginInfoClient=PluginMapper.toPluginInfoClient(javaScript);
            return pluginInfoClient;
        } catch (Exception e) {
            throw new BaseException();
        }
    }


    @RequestMapping(value="/sendToSocket",method = RequestMethod.POST)
    public ResponseEntity callbackFromParty(@RequestBody MessageSocket messageSocket)
    {
        this.template.convertAndSend("/secured/user/queue/specific-user"+"-"+
                        messageSocket.getSessionId(),
                "{\"objectId\":\""+messageSocket.getObjectId() +
                        "\", \"message\":"+messageSocket.getMessage()+"}");
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value="/getAccounting",method = RequestMethod.GET
            ,produces = "application/json")
    public Map<String,List<String>> getAccounting(Principal principal){
        return businessRuleService.getAccounting(principal.getName());
    }

    @DeleteMapping(path ={"/removeAccount/{pluginName}/{account}"})
    public void delete(@PathVariable("pluginName") String pluginName,
                       @PathVariable("account") String account,
                       Principal principal) {
        businessRuleService.removeAccount(principal.getName(),pluginName,account,principal);

    }

    /*@Autowired
    Plugins plugins;

    @GetMapping(path = {"/getAllConsumer"})
    public List<PluginInfo> findAllConsumer() {
        return plugins.getPluginList();
    }*/
}

