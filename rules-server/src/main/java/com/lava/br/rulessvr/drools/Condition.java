package com.lava.br.rulessvr.drools;

/**
 * Created by Aria on 02/05/2018.
 */

import com.lava.br.core.enums.Operator;
import org.springframework.data.annotation.TypeAlias;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Representation of condition to apply a business rule.
 *
 * @author Gabriel Stelmach <gabriels@fourway.com.br>
 * @see Rule
 */
@TypeAlias("condition")
public class Condition
{
    /**
     * Object property to be evaluated.
     */
    private String property;
    /**
     * Value to be evaluated.
     */
    private Object value;
    /**
     * Operator used to compare the data.
     */
    private Operator operator;

    /**
     * Type of available operator.
     */
  /*public static enum Operator
  {
    /**
    /**
     * Create a new empty condition.
     */
    public Condition()
    {
    }

    /**
     * Create a complete condition.
     *
     * @param property Data property to be evaluated.
     * @param operator Operator used to compare the data.
     * @param value Value to be evaluated.
     */
    public Condition(String property, Operator operator, Object value)
    {
        this.property = property;
        this.operator = operator;
        this.value = value;
    }

    /**
     * Convert the condition to textual expression.
     *
     * @return The expression of this condition in dialect.
     * @throws IllegalArgumentException Indicates the use of invalid pair of value and condition.
     */
    public String buildExpression() throws IllegalArgumentException
    {
        StringBuilder drl = new StringBuilder();

        if (value instanceof String)
        {
        drl.append("compare(").append(property).append(",").append("Operator.").append(operator.name()).append(",\"").append(value).append("\")");
        }
        return  drl.toString();

       /* if (value instanceof String)
        {
            drl.append(expressionForStringValue());
        }
        else if (value instanceof Number)
        {
            drl.append(expressionForNumberValue());
        }
        else if (value instanceof Date)
        {
            drl.append(expressionForDateValue());
        }
        else
        {
            throw new IllegalArgumentException("The class " + value.getClass().getSimpleName() + " of value is not acceptable.");
        }

        return drl.toString();*/
    }

    /**
     * Convert the condition for <b>String</b> value in expression.
     *
     * @return Expression in dialect.
     * @throws IllegalArgumentException Indicates the use of invalid pair of value and condition.
     */
    private String expressionForStringValue() throws IllegalArgumentException
    {
        StringBuilder drl = new StringBuilder();

        if (operator.isComparable(String.class))
        {
            if (operator.equals(Operator.CONTAINS))
            {
                drl.append(property).append(".toString().toUpperCase().contains(\"").append(((String)value).toUpperCase()).append("\")");
            }
            else
            {
                drl.append(property).append(" ").append(operator.getOperation()).append(" ").append("\"").append(value).append("\"");
            }
        }
        else
        {
            throw new IllegalArgumentException("Is not possible to use the operator " + operator.getDescription() + " to a " + value.getClass().getSimpleName() + " object.");
        }

        return drl.toString();
    }

    /**
     * Convert the condition for <b>Integer</b>, <b>Double</b> or <b>Float</b> value in expression.
     *
     * @return Expression in dialect.
     * @throws IllegalArgumentException Indicates the use of invalid pair of value and condition.
     */
    private String expressionForNumberValue() throws IllegalArgumentException
    {
        StringBuilder drl = new StringBuilder();

        if ((operator.isComparable(Short.class)) || (operator.isComparable(Integer.class)) || (operator.isComparable(Long.class))
                || (operator.isComparable(Double.class)) || (operator.isComparable(Float.class)))
        {
            drl.append(property).append(" ").append(operator.getOperation()).append(" ").append(value);
        }
        else
        {
            throw new IllegalArgumentException("Is not possible to use the operator " + operator.getDescription() + " to a " + value.getClass().getSimpleName() + " object.");
        }

        return drl.toString();
    }

    /**
     * Convert the condition for <b>Date</b> value in expression.
     *
     * @return Expression in dialect.
     * @throws IllegalArgumentException Indicates the use of invalid pair of value and condition.
     */
    private String expressionForDateValue() throws IllegalArgumentException
    {
        StringBuilder drl = new StringBuilder();

        if (operator.isComparable(Date.class))
        {
            drl.append(property).append(" ").append(operator.getOperation()).append(" (new SimpleDateFormat(\"dd/MM/yyyy HH:mm:ss\")).parse(\"" + (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format((Date)value) + "\")");
        }
        else
        {
            throw new IllegalArgumentException("Is not possible to use the operator " + operator.getDescription() + " to a " + value.getClass().getSimpleName() + " object.");
        }

        return drl.toString();
    }

    public String getProperty()
    {
        return property;
    }

    public Object getValue()
    {
        return value;
    }

    public Operator getOperator()
    {
        return operator;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    public void setOperator(Operator operator)
    {
        this.operator = operator;
    }
}

