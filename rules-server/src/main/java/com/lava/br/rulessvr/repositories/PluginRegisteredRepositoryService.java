package com.lava.br.rulessvr.repositories;

import org.springframework.stereotype.Service;

@Service("pluginRegisteredRepositoryService")
public class PluginRegisteredRepositoryService extends com.lava.br.core.repositories.PluginRegisteredRepositoryService {
}
