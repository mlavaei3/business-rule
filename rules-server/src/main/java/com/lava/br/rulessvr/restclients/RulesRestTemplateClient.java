package com.lava.br.rulessvr.restclients;

import com.lava.br.core.clients.PluginInfoClient;
import com.lava.br.core.repositories.bean.PluginRegistered;
import com.lava.br.core.restTemplates.KeycloakClientCredentialsRestTemplate;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class RulesRestTemplateClient {

    @Autowired
    @Qualifier("keycloakClientCredentialsRestTemplate")
    KeycloakClientCredentialsRestTemplate keycloakClientCredentialsRestTemplate;

    private static final Logger logger = LoggerFactory.getLogger(RulesRestTemplateClient.class);

    public PluginInfoClient getPluginInfoClient(String pluginName){
        logger.debug("GET PluginInfo From Plugin Service: {}", pluginName);
        PluginInfoClient pluginInfoClient=null;
        try {
            HttpEntity<String> requestEntity = new HttpEntity<>(pluginName);

            ResponseEntity<PluginInfoClient> restExchange =
                    keycloakClientCredentialsRestTemplate.exchange(
                            "http://zuulservice/api/" + pluginName + "/v1/getPluginInfoClient",
                            HttpMethod.GET,
                            null,PluginInfoClient.class);

            /*Save the record from cache*/
            pluginInfoClient = restExchange.getBody();//Boolean.valueOf(
        }catch (HttpServerErrorException ex){
            System.out.println(ex.getMessage());
        }
        return pluginInfoClient;
    }

    public PluginRegistered registerPlugin(PluginInfoClient pluginInfoClient) throws RestClientException {
        logger.debug("In Register Plugin Info: {}", pluginInfoClient);
        //List<ServiceInstance>  gmailplugin=discoveryClient.getInstances("gmailplugin");
        HttpEntity<PluginInfoClient> requestEntity = new HttpEntity<PluginInfoClient>(pluginInfoClient);

        /*ResponseEntity<String> restExchange2 =
                keycloakClientCredentialsRestTemplate.exchange(
                        "http://zuulservice/api/"+pluginInfoClient.getName()+"/v1/test/rr",
                        HttpMethod.GET,
                        null, String.class);

        *//*Save the record from cache*//*
        String pluginRegistereda = restExchange2.getBody();*/

        ResponseEntity<PluginRegistered> restExchange =
                keycloakClientCredentialsRestTemplate.exchange(
                        "http://zuulservice/api/"+pluginInfoClient.getName()+"/v1/registerPlugin",
                        HttpMethod.POST,
                        requestEntity, PluginRegistered.class);

        /*Save the record from cache*/
        PluginRegistered pluginRegistered = restExchange.getBody();
        return pluginRegistered;
    }

    public PluginRegistered getTemplateRegisterPlugin(PluginInfoClient pluginInfoClient) {
        logger.debug("In Get Template RegisterPlugin Info: {}", pluginInfoClient);
        HttpEntity<PluginInfoClient> requestEntity = new HttpEntity<PluginInfoClient>(pluginInfoClient);
        ResponseEntity<PluginRegistered> restExchange =
                keycloakClientCredentialsRestTemplate.exchange(
                        "http://zuulservice/api/"+pluginInfoClient.getName()+"/v1/getTemplateRegisterPlugin",
                        HttpMethod.POST,
                        requestEntity, PluginRegistered.class);

        /*Save the record from cache*/
        PluginRegistered pluginRegistered = restExchange.getBody();
        return pluginRegistered;
    }


    @Autowired
    @Qualifier("eurekaClient")
    EurekaClient eurekaClient;

    @Autowired
    @Qualifier("keycloakClientCredentialsRestTemplateNoBalance")
    KeycloakClientCredentialsRestTemplate keycloakClientCredentialsRestTemplateNoBalance;

    public Boolean unRegisterPlugin(PluginInfoClient pluginInfoClient,PluginRegistered pluginRegistered) {
        logger.debug("In unRegisterPlugin Plugin Info: {}", pluginInfoClient);

        List<InstanceInfo> lstInstances= eurekaClient.getInstancesById(pluginRegistered.getInstanceId());
        InstanceInfo instanceInfo= lstInstances.stream().filter(p->p.getInstanceId().equalsIgnoreCase(pluginRegistered.getInstanceId())).findFirst().get();

        HttpEntity<PluginInfoClient> requestEntity = new HttpEntity<>(pluginInfoClient);
        ResponseEntity<Boolean> restExchange =
                keycloakClientCredentialsRestTemplateNoBalance.exchange(
                        instanceInfo.getHomePageUrl()+"/v1/unRegisterPlugin",
                        HttpMethod.POST,
                        requestEntity, Boolean.class);

        /*Save the record from cache*/
        return restExchange.getBody();
    }

    public Boolean producePlugin(PluginInfoClient pluginInfoClient) {
        logger.debug("In send Plugin To Producer: {}", pluginInfoClient);
        Boolean org=null;
        try {
            //HttpHeaders headers = new HttpHeaders();
            //headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            //headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<PluginInfoClient> requestEntity = new HttpEntity<>(pluginInfoClient);

            ResponseEntity<Boolean> restExchange =
                    keycloakClientCredentialsRestTemplate.exchange(
                            "http://zuulservice/api/" + pluginInfoClient.getName() + "/v1/producePlugin",
                            HttpMethod.POST,
                            requestEntity,Boolean.class);

            /*Save the record from cache*/
            org = restExchange.getBody();//Boolean.valueOf(
        }catch (HttpServerErrorException ex){
            System.out.println(ex.getMessage());
        }
        return org;
    }
}
