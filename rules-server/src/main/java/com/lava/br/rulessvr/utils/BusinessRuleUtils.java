package com.lava.br.rulessvr.utils;

import com.lava.br.core.beans.IBusinessRule;
import com.lava.br.core.beans.IFieldFilter;
import com.lava.br.core.repositories.bean.FieldAssigner;

/**
 * Created by Aria on 09/04/2018.
 */
public  class BusinessRuleUtils {
    public static boolean configsEquals(IBusinessRule br1, IBusinessRule br2){
        if(br1.getConsumerConfig().size()!=br2.getConsumerConfig().size())
            return false;

        for (FieldAssigner faBr1:br1.getConsumerConfig()){
            boolean findField=false;
            for (FieldAssigner faBr2:br2.getConsumerConfig()){
                if (faBr1.getFieldName().equals(faBr2.getFieldName())){
                    findField=true;
                    if(!faBr1.getValue().equals(faBr2.getValue())){
                        return false;
                    }
                }
            }
            if (findField==false) return false;
        }

        for (FieldAssigner faBr2:br2.getConsumerConfig()){
            boolean findField=false;
            for (FieldAssigner faBr1:br1.getConsumerConfig()){
                if (faBr2.getFieldName().equals(faBr1.getFieldName())){
                    findField=true;
                    if(!faBr2.getValue().equals(faBr1.getValue())){
                        return false;
                    }
                }
            }
            if (findField==false) return false;
        }

        return true;
    }

    public static boolean filtersEquals(IBusinessRule br1, IBusinessRule br2){
        if(br1.getConsumerFilters().size()!=br2.getConsumerFilters().size())
            return false;

        for (IFieldFilter ffBr1:br1.getConsumerFilters()){
            boolean findField=false;
            for (IFieldFilter ffBr2:br2.getConsumerFilters()){
                if (ffBr1.getFieldName().equals(ffBr2.getFieldName())){
                    findField=true;
                    if(!ffBr1.getValue().equals(ffBr2.getValue())){
                        return false;
                    }
                    if(!ffBr1.getOperator().equals(ffBr2.getOperator())){
                        return false;
                    }
                }
            }
            if (findField==false) return false;
        }

        for (IFieldFilter ffBr2:br2.getConsumerFilters()){
            boolean findField=false;
            for (IFieldFilter ffBr1:br1.getConsumerFilters()){
                if (ffBr2.getFieldName().equals(ffBr1.getFieldName())){
                    findField=true;
                    if(!ffBr2.getValue().equals(ffBr1.getValue())){
                        return false;
                    }
                    if(!ffBr2.getOperator().equals(ffBr1.getOperator())){
                        return false;
                    }
                }
            }
            if (findField==false) return false;
        }

        return true;
    }
}
