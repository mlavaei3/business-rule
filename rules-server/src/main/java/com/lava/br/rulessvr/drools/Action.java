package com.lava.br.rulessvr.drools;

/**
 * Created by Aria on 02/05/2018.
 */

public class Action {
    private String lProperty;
    private String rProperty;


    public Action(String lProperty, String rProperty) {
        this.lProperty = lProperty;
        this.rProperty = rProperty;
    }

    public String getlProperty() {
        return lProperty;
    }

    public void setlProperty(String lProperty) {
        this.lProperty = lProperty;
    }

    public String getrProperty() {
        return rProperty;
    }

    public void setrProperty(String rProperty) {
        this.rProperty = rProperty;
    }

}