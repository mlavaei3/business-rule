package com.lava.br.plugins.rest.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.Field;
import jdk.nashorn.internal.objects.NativeArray;
import org.apache.camel.*;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.message.MessageContentsList;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("rest")
@Pluggable(nameOfPlugin = "rest")
public class Rest extends BasePluginWithCamel {
    @PostConstruct
    public void init() {
       /* // configs
        this.getInfo().getOutVariables().add(new Field("host", java.lang.String.class, "host", true,null,new String[]{"default"},1));
        this.getInfo().getOutVariables().add(new Field("restClientId", java.lang.String.class, "restClientId", 2));
        this.getInfo().getOutVariables().add(new Field("operationName", java.lang.String.class, "operationName", 3));

        // inVar
        this.getInfo().getInVariables().add(new Field("result", Map.class, "result",null,new String[]{"default"},1));

        // outVar
        this.getInfo().getOutVariables().add(new Field("paramsList", List.class, "paramsList",1));
*/
    }

    @Override
    public void consume(IPlugin plugin) {
    }

    public void produce(IPlugin plugin) {
        Endpoint endpoint = getCamelContext()
                .getEndpoint("cxfrs:bean:"+plugin.getInfo().getValue(PluginInfo.CONFIG,"restClientId"));
        Exchange exchange = endpoint.createExchange();
        exchange.setPattern(ExchangePattern.InOut);

        Message inMessage = exchange.getIn();
        // set the operation name
        inMessage.setHeader(CxfConstants.OPERATION_NAME,  plugin.getInfo().getValue(PluginInfo.CONFIG,"operationName"));
        // using the proxy client API
        inMessage.setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);

        //creating the request
        MessageContentsList req = new MessageContentsList();

        //req.add("hi");
        //req.add("222");
        req.addAll((ArrayList)plugin.getInfo().getValue(PluginInfo.OUT,"paramsList"));

        inMessage.setBody(req);
        runTemplateProducer(endpoint,exchange);


        try {
            ProducerTemplate template=getCamelContext().createProducerTemplate();
            Map response = template.requestBody("direct:marsha2json", exchange.getOut().getBody(String.class),Map.class);

            plugin.getInfo().setValue(PluginInfo.IN,"result",response.get("entity"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
        }
    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "username")!=null?
                plugin.getInfo().getValue(PluginInfo.CONFIG, "username").toString().trim()
                :null;
    }
}