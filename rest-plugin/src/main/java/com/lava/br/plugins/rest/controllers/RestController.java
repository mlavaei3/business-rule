package com.lava.br.plugins.rest.controllers;


import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.rest.services.RestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.web.bind.annotation.RestController
public class RestController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(RestController.class);

    @Autowired
    private RestService restService;

    @Override
    public PluginService getPluginService(){
         return restService;
    }

}
