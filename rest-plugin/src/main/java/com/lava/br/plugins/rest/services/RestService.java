package com.lava.br.plugins.rest.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.rest.impl.Rest;
import com.lava.br.plugins.rest.restclients.RestRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestService extends PluginService {
    @Autowired
    private Rest rest;

    @Autowired
    private RestRestTemplateClient restRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return rest;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return restRestTemplateClient;
    }
}
