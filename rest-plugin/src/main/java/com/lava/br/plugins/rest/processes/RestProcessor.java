package com.lava.br.plugins.rest.processes;

import com.lava.br.core.processes.BaseProcessor;
import com.lava.br.plugins.rest.impl.Rest;
import org.apache.camel.Exchange;

public class RestProcessor extends BaseProcessor {
    public RestProcessor(Rest rest) {
        super(rest);
    }

    @Override
    public void process(Exchange exchange) throws Exception {
               sendMessageToServer();
    }


}
