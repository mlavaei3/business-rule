package com.lava.br.plugins.gpiotNiligo.services;

import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.restclients.PluginRestTemplateClient;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.gpiotNiligo.impl.GpiotNiligo;
import com.lava.br.plugins.gpiotNiligo.restclients.GpiotNiligoRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GpiotNiligoService extends PluginService {


    @Autowired
    private GpiotNiligo gpiotNiligo;

    @Autowired
    private GpiotNiligoRestTemplateClient gpiotNiligoRestTemplateClient;

    @Override
    public IPlugin getPlugin() {
        return gpiotNiligo;
    }

    @Override
    public PluginRestTemplateClient getPluginRestTemplateClient() {
        return gpiotNiligoRestTemplateClient;
    }



}
