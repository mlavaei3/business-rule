package com.lava.br.plugins.gpiotNiligo.impl;

/**
 * Created by Aria on 04/05/2018.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.ApplicationContextProvider;
import com.lava.br.core.annotations.Pluggable;
import com.lava.br.core.beans.IFieldValidator;
import com.lava.br.core.beans.IPlugin;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.TypeField;
import com.lava.br.core.enums.ValidatorType;
import com.lava.br.core.plugins.BasePlugin;
import com.lava.br.core.plugins.BasePluginWithCamel;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.*;
import com.lava.br.plugins.gpiotNiligo.services.GpiotNiligoService;
import org.apache.cxf.common.util.UrlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("gpiotNiligo")
@Pluggable(nameOfPlugin = "gpiotNiligo")
public class GpiotNiligo extends BasePlugin {
    @PostConstruct
    public void init() {
        this.getInfo().setIsConsumer(true);

        // configs
        /**
         * account
         */
        PartyField partyField=PartyField
                .builder()
                .name("account")
                .label("account")
                .type(TypeField.PartySelectList)
                .visibility(true)
                .order(1)
                .build();
        partyField.setSelectFields(this::getAccounts);
        partyField.setRequestUrl("");
        partyField.setPartyUrl(tokenUri+"/oauth/authorize?response_type=code&scopes=read&client_id="+userPass.split(":")[0]+"&redirect_uri="+ UrlUtils.urlEncode(redirectUri));
        this.getInfo().getConfigVariables().add(partyField);
        /**
         * gpiotNiligoList
         */
        LazySelectField gpiotNiligoListField=LazySelectField.builder()
                .fieldClass(LazySelectField.class)
                .name("gpiotNiligoList")
                .label("gpiotNiligoList")
                .type(TypeField.LazySelectList)
                .visibility(true)
                .order(2)
                .build();

        gpiotNiligoListField.setSelectFields(this::getGpiotNiligos);
        gpiotNiligoListField.setRequestUrl("?query=${account}");

        List<IFieldValidator> fieldValidators=new ArrayList<>();
        FieldValidator fvReset=FieldValidator.builder()
                .type(ValidatorType.DeptReset)
                .expression("account")
                .build();
        fieldValidators.add(fvReset);


        gpiotNiligoListField.setFieldValidators(fieldValidators);

        this.getInfo().getConfigVariables().add(gpiotNiligoListField);


        /**
         * port
         */
        SimpleSelectField port = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("port")
                .label("port")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(3)
                .build();
        Map<Object, String> map = new HashMap<>();
        map.put("0", "0");
        map.put("1", "1");

        port.setSelectFields(map);
        this.getInfo().getConfigVariables().add(port);

        /**
         * operation
         */
        SimpleSelectField operation = SimpleSelectField.builder()
                .fieldClass(SimpleSelectField.class)
                .name("operation")
                .label("operation")
                .type(TypeField.SimpleSelectList)
                .visibility(true)
                .order(3)
                .build();
        map = new LinkedHashMap<>();
        map.put("GPIOT_EDGE_DOWN", "EDGE_DOWN");
        map.put("GPIOT_EDGE_UP", "EDGE_UP");
        map.put("GPIOT_LEVEL_DOWN", "LEVEL_DOWN");
        map.put("GPIOT_LEVEL_UP", "LEVEL_UP");

        operation.setSelectFields(map);
        this.getInfo().getConfigVariables().add(operation);

    }

    @Value("${lava.plugins.gpiotNiligo.api_uri}")
    private String apiUri;

    @Value("${lava.plugins.gpiotNiligo.m2m_uri}")
    private String m2mUuri;

    @Value("${lava.plugins.gpiotNiligo.auth2.redirect_uri}")
    private String redirectUri;

    @Value("${lava.plugins.gpiotNiligo.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.gpiotNiligo.auth2.user_pass}")
    private String userPass;

    public Map<String, String> getAccounts(Map<String, Object> objectMap){

        GpiotNiligoService gpiotNiligoService= ApplicationContextProvider.getApplicationContext().getBean("gpiotNiligoService",GpiotNiligoService.class);
        Map<String,String> accounts=gpiotNiligoService.getAllValueForKey(objectMap.get("userId").toString(),DbStoreType.ACCOUNTING,"account","token");
        accounts.forEach((k,v)->{accounts.put(k,k);});
        return accounts;
    }

    public Map<String, String> getGpiotNiligos(Map<String, Object> objectMap){
        Map<String,String> map=new HashMap<>();
        try {
            ApplicationContext applicationContext = null;
            applicationContext = ApplicationContextProvider.getApplicationContext();
            GpiotNiligoService  gpiotNiligoService= (GpiotNiligoService) applicationContext.getBean("gpiotNiligoService");
            String token= gpiotNiligoService.getValueForKey(objectMap.get("userId").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    objectMap.get("query").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;

            node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);
            HttpEntity<String> request1=new HttpEntity<>(headers1);

            String resourceUrl=apiUri+"/api/thing/list?type=gpiot";
            ResponseEntity<ThingDto[]> rsp1 = restTemplate.exchange(resourceUrl, HttpMethod.GET, request1, ThingDto[].class);

            for (ThingDto thingDto: rsp1.getBody()){
                map.put(thingDto.getUuid(),thingDto.getName());
            }
            return map;
        } catch (IOException e) {
            return map;
        }
    }

//    public List<String> registerGpiotNiligo=new ArrayList<>();
//
//    public List<String> getRegisterGpiotNiligo() {
//        return registerGpiotNiligo;
//    }


    @Override
    public void consume(IPlugin plugin) {
        try {
            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            GpiotNiligoService  prismService= (GpiotNiligoService) applicationContext.getBean("gpiotNiligoService");
            String token= prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG,"userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG,"account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate=new RestTemplate();
            HttpHeaders headers1=new HttpHeaders();
            headers1.add("Authorization","Bearer "+accessToken);


            String actionType=plugin.getInfo().getValue(PluginInfo.CONFIG,"producerAction").toString();
            String resourceUrl=m2mUuri+"/m2m/integration/user/add";
            Map<String,String> map=new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid",plugin.getInfo().getValue(PluginInfo.CONFIG,"gpiotNiligoList").toString());
            map.put("trigger", plugin.getInfo().getValue(PluginInfo.CONFIG, "operation").toString());
            map.put("port", plugin.getInfo().getValue(PluginInfo.CONFIG, "port").toString());

            HttpEntity<Map<String,String>> request1=new HttpEntity<>(map,headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            if (rsp1.getStatusCodeValue()==200 || rsp1.getStatusCodeValue()==201){

            }

            System.out.println(rsp1.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void unConsume(IPlugin plugin) {
        try {
            final Map<String, String> map = new HashMap<>();
            map.put("integrationUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString());
            map.put("m2mUsername", plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString());
            map.put("uuid", plugin.getInfo().getValue(PluginInfo.CONFIG, "gpiotNiligoList").toString());
            map.put("trigger", plugin.getInfo().getValue(PluginInfo.CONFIG, "operation").toString());
            map.put("port", plugin.getInfo().getValue(PluginInfo.CONFIG, "port").toString());

            ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
            GpiotNiligoService prismService = (GpiotNiligoService) applicationContext.getBean("gpiotNiligoService");
            String token = prismService.getValueForKey(plugin.getInfo().getValue(PluginInfo.CONFIG, "userid").toString(),
                    DbStoreType.ACCOUNTING,
                    "account",
                    plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString(),
                    "token");

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(token);

            String accessToken = node.path("accessToken").asText();
            //String refreshToken = node.path("refreshToken").asText();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers1 = new HttpHeaders();
            headers1.add("Authorization", "Bearer " + accessToken);


            String actionType = plugin.getInfo().getValue(PluginInfo.CONFIG, "producerAction").toString();
            String resourceUrl = m2mUuri + "/m2m/integration/user/delete";
            HttpEntity<Map<String, String>> request1 = new HttpEntity<>(map, headers1);
            ResponseEntity<String> rsp1 = restTemplate.postForEntity(resourceUrl, request1, String.class);
            if (rsp1.getStatusCodeValue() == 200 || rsp1.getStatusCodeValue() == 201) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void produce(IPlugin plugin) {

    }

    @Override
    public String getUniqueIdentity(IPlugin plugin) {
        return plugin.getInfo().getValue(PluginInfo.CONFIG, "account").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "gpiotNiligoList").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "port").toString().trim()
                +"-"+plugin.getInfo().getValue(PluginInfo.CONFIG, "operation").toString().trim();
   }
}