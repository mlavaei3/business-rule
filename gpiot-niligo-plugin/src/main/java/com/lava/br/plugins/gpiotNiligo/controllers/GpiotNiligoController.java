package com.lava.br.plugins.gpiotNiligo.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lava.br.core.controllers.PluginController;
import com.lava.br.core.enums.DbStoreType;
import com.lava.br.core.enums.NiligoTriggerType;
import com.lava.br.core.plugins.utils.niligo.Endpoints;
import com.lava.br.core.plugins.utils.niligo.M2MPayload;
import com.lava.br.core.plugins.PluginInfo;
import com.lava.br.core.repositories.bean.PluginRegistered;
import com.lava.br.core.services.PluginService;
import com.lava.br.plugins.gpiotNiligo.impl.GpiotNiligo;
import com.lava.br.plugins.gpiotNiligo.services.GpiotNiligoService;
import com.lava.br.plugins.gpiotNiligo.services.PluginRegisteredRepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class GpiotNiligoController extends PluginController {

    private static final Logger logger = LoggerFactory.getLogger(GpiotNiligoController.class);

    @Value("${lava.plugins.gpiotNiligo.auth2.user_pass}")
    private String userPassForToken;

    @Value("${lava.plugins.gpiotNiligo.auth2.token_uri}")
    private String tokenUri;

    @Value("${lava.plugins.gpiotNiligo.auth2.redirect_uri}")
    private String redirectUri;

    @Autowired
    private GpiotNiligoService gpiotNiligoService;

    @Override
    public PluginService getPluginService(){
        return gpiotNiligoService;
    }

    private Endpoints endpoints;

    private Endpoints getEndpoints(){
        if (endpoints==null) {
            endpoints = new Endpoints(userPassForToken, tokenUri, redirectUri, getPluginService());
        }
        return endpoints;
    }

    @RequestMapping(value="testDb/{brId}",method = RequestMethod.GET)
    public String testDb(@PathVariable("brId") String brId) {
        gpiotNiligoService.addKeyWithValue("mehdi",DbStoreType.ACCOUNTING,"account","acc1","token","ttt22");
        gpiotNiligoService.addKeyWithValue("mehdi",DbStoreType.ACCOUNTING,"account","acc2","token","ttt2222");

        Map<String,String> maps= gpiotNiligoService.getAllValueForKey("mehdi",DbStoreType.ACCOUNTING,"account","token");
        String gg= gpiotNiligoService.getValueForKey("mehdi",DbStoreType.ACCOUNTING,"account","acc1","token");
        return "ok";
    }


    @RequestMapping(value="/testSocket",method = RequestMethod.GET)
    public void testSocket(@RequestParam(value ="sessionId",required = true) String sessionId,
                           @RequestParam(value ="objectId",required = true) String objectId ,
                           @RequestParam(value ="email",required = true) String email) {

        gpiotNiligoService.sendToSocket(sessionId,objectId,"{\""+email+ "\":\""+email+"\"}");

    }

    @Autowired
    PluginRegisteredRepositoryService pluginRegisteredRepositoryService;

    @RequestMapping(value="/callbackFromGpiotNiligo",method = RequestMethod.POST)
    public ResponseEntity callbackFromGpiotNiligo(@RequestBody M2MPayload object) {
        try {
            if(object.getM2mUsers()==null || object.getM2mUsers().size()==0){
                return null;
            }
            GpiotNiligo gpiotNiligo=(GpiotNiligo)  getPluginService().getNewInstancePlugin();
            gpiotNiligo.getInfo().setValue(PluginInfo.CONFIG,"account",object.getM2mUsers().get(0));// "ghasematsadeghi@gmail.com");
            gpiotNiligo.getInfo().setValue(PluginInfo.CONFIG,"gpiotNiligoList",object.getUuid());
            gpiotNiligo.getInfo().setValue(PluginInfo.CONFIG,"port",object.getPortFromRelay());
            if(object.getTriggerType()==NiligoTriggerType.GPIOT_EDGE_DOWN) {
                gpiotNiligo.getInfo().setValue(PluginInfo.CONFIG,"operation","GPIOT_EDGE_DOWN");
            }else if(object.getTriggerType()==NiligoTriggerType.GPIOT_EDGE_UP){
                gpiotNiligo.getInfo().setValue(PluginInfo.CONFIG,"operation","GPIOT_EDGE_UP");
            }else if(object.getTriggerType()==NiligoTriggerType.GPIOT_LEVEL_DOWN){
                gpiotNiligo.getInfo().setValue(PluginInfo.CONFIG,"operation","GPIOT_LEVEL_DOWN");
            }else if(object.getTriggerType()==NiligoTriggerType.GPIOT_LEVEL_UP){
                gpiotNiligo.getInfo().setValue(PluginInfo.CONFIG,"operation","GPIOT_LEVEL_UP");
            }

            String unique=getPluginService().getPlugin().getUniqueIdentity(gpiotNiligo);
            List<PluginRegistered> lstPluginsRegistered=pluginRegisteredRepositoryService.getBasicRepository()
                    .findByConsumerTypeAndPluginUniqueIdentity(getPluginService().getPlugin().getInfo().getName(),unique);

            if(lstPluginsRegistered.size()>0){
                getPluginService().sendMessageToServer(gpiotNiligo);
            }


        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value="/callbackFromParty",method = RequestMethod.GET)
    public ResponseEntity callbackFromParty(@RequestParam(value ="state",required = true) String state,
                                            @RequestParam(value ="code",required = true) String code)  {

        return getEndpoints().callbackFromParty(state,code);
        /*String sessionId=state.split(" ")[0];
        String objectId=state.split(" ")[1];
        String userid=state.split(" ")[2];

        //String userPass = "ghasem:sadeghi";
        String basicEncoded = Base64Utils.encodeToString(userPassForToken.getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + basicEncoded);
        HttpEntity<String> request = new HttpEntity<>(headers);

        String url = tokenUri+"/oauth/token?";
        url += "code=" + code;
        url += "&grant_type=authorization_code";
        url += "&redirect_uri="+redirectUri;

        try {
            RestTemplate restTemplate=new RestTemplate();
            ResponseEntity<String> rsp = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

            //(2)extract the Access Token From the recieved JSON response
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(rsp.getBody());
            String accessToken = node.path("access_token").asText();

            getPluginService().addKeyWithValue(userid,
                    "account",
                    node.path("username").asText(),"token",
                    String.format("{\"accessToken\":\"%s\"}",accessToken));

            gpiotNiligoService.sendToSocket(sessionId,objectId,"{\""+node.path("username").asText()+ "\":\""+node.path("username").asText()+"\"}");


        } catch (HttpClientErrorException e) {
            System.out.println("\n1:"+e.getStatusCode());
            System.out.println("\n2:"+e.getResponseBodyAsString());
            //model.addAttribute("message", e.getResponseBodyAsString());
            //return "error";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("<script language='javascript'>parentwin = window.self;\n" +
                "parentwin.opener = window.self;\n" +
                "parentwin.close(); </script>");
*/
    }

    @RequestMapping(value="/getGpiotNiligos",method = RequestMethod.GET)
    public ResponseEntity getGpiotNiligos(Principal principal, @RequestParam(value ="account",required = true) String account) {
        Map<String, Object> objectMap=new HashMap<>();
        objectMap.put("userId",principal.getName());
        objectMap.put("query",account);
        Map<String, String> gpiotNiligoMap=((GpiotNiligo)gpiotNiligoService.getPlugin()).getGpiotNiligos(objectMap);
        return ResponseEntity.ok(gpiotNiligoMap);
    }

}
